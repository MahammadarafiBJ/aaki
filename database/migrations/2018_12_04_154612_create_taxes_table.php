<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxes', function (Blueprint $table) {
            $table->bigIncrements('tax_id');
            $table->string('tax_name',50);
            $table->double('tax_rate',15,2);
            $table->string('cgst_name',50);
            $table->double('cgst_rate',15,2);
            $table->string('sgst_name',50);
            $table->double('sgst_rate',15,2);
            $table->string('igst_name',50);
            $table->double('igst_rate',15,2);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('taxes')->insert(
            [
                [
                    'tax_name' => '0.0%',
                    'tax_rate' => 0.0,
                    'cgst_name' => '0.0%',
                    'cgst_rate' => 0.0,
                    'sgst_name' => '0.0%',
                    'sgst_rate' => 0.0,
                    'igst_name' => '0.0%',
                    'igst_rate' => 0.0,
                    'created_by' => 'mdbstech',
                ],
                [
                    'tax_name' => '5.0%',
                    'tax_rate' => 5.0,
                    'cgst_name' => '2.5%',
                    'cgst_rate' => 2.5,
                    'sgst_name' => '2.5%',
                    'sgst_rate' => 2.5,
                    'igst_name' => '5.0%',
                    'igst_rate' => 5.0,
                    'created_by' => 'mdbstech',
                ],
                [
                    'tax_name' => '12.0%',
                    'tax_rate' => 12.0,
                    'cgst_name' => '6.0%',
                    'cgst_rate' => 6.0,
                    'sgst_name' => '6.0%',
                    'sgst_rate' => 6.0,
                    'igst_name' => '12.0%',
                    'igst_rate' => 12.0,
                    'created_by' => 'mdbstech',
                ],
                [
                    'tax_name' => '18.0%',
                    'tax_rate' => 18.0,
                    'cgst_name' => '9.0%',
                    'cgst_rate' => 9.0,
                    'sgst_name' => '9.0%',
                    'sgst_rate' => 9.0,
                    'igst_name' => '18.0%',
                    'igst_rate' => 18.0,
                    'created_by' => 'mdbstech',
                ],
                [
                    'tax_name' => '28.0%',
                    'tax_rate' => 28.0,
                    'cgst_name' => '14.0%',
                    'cgst_rate' => 14.0,
                    'sgst_name' => '14.0%',
                    'sgst_rate' => 14.0,
                    'igst_name' => '28.0%',
                    'igst_rate' => 28.0,
                    'created_by' => 'mdbstech',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxes');
    }
}
