<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use PDF;

use App\Category;
use App\Organization;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_category_code(Request $request)
    {
        $id = Category::max('category_id');
        $category_code = $id + 1;
        $length = strlen($category_code);
        $n = 3 - $length;
        for ($i=0; $i < $n; $i++) 
        { 
            $category_code = '0'.$category_code;
        }
        return $category_code;
    }

    public function display(Request $request)
    {
        return Category::
        	where('category_code', 'like', '%'.$request->search.'%')
        	->orWhere('category_name', 'like', '%'.$request->search.'%')
        	->paginate(5);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'category_code' => 'required|max:50',
            'category_name' => 'required|max:50',
        ]);
        Category::create([
            'category_code' => $request->category_code,
            'category_name' => $request->category_name,
            'created_by'    => Auth::User()->username,         
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'category_code' => 'required|max:50',
            'category_name' => 'required|max:50',
        ]);
        Category::where('category_id',$request->category_id)->update([
            'category_code' => $request->category_code,
            'category_name' => $request->category_name,
            'updated_by'    => Auth::User()->username,         
        ]);
    }

    public function destroy(Category $category)
    {
    	return Category::where('category_id',$category->category_id)->delete();
    }

    public function get_categories()
    {
        return Category::get();
    }

    // public function report(Request $request)
    // {
    //     $org = Organization::first();
    //     $categories = Category::get();

    //     $category_invoice_products = Category::whereHas('CategoryInvoiceProducts',function($query) use($request){
    //         $query->whereHas('Invoice',function($que) use($request){
    //             $que->whereBetween('invoice_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))));
    //         });
    //     })->get();
    //     //Category based invoice products
    //     if($request->category_id!=null){
    //         $category_invoice_products = $category_invoice_products->filter(function($val)use($request){
    //             return $val->category_id == $request->category_id;
    //         })->values();
    //     }
    //     if (count($category_invoice_products)>0) {
    //         //all invoice products 
    //         $category_invoice_products->load(['CategoryInvoiceProducts'=>function($query) use($request){
    //         $query->whereHas('Invoice',function($que) use($request){
    //             $que->whereBetween('invoice_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))));
    //             });
    //         }]);
    //         //grouping products datewise
    //         $category_invoice_products->each(function($category,$key)use($request){
    //             $category->filtered_invoice_products = $category->CategoryInvoiceProducts->unique('product_id')->values();

    //             $category->filtered_invoice_products->load(['CategoryInvoice'=>function($query) use($request){
    //                 $query->whereHas('Invoice',function($que) use($request){
    //                     $que->whereBetween('invoice_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))));
    //                     });
    //                 }]);
    //         });
    //     } 
    //     if($request->display_type=='display'){

    //         return compact('category_invoice_products','categories');

    //     }else if($request->display_type=='pdf'){
            
    //         PDF::loadView('product.category_report', compact('org','request','category_invoice_products'), [], [
    //             'margin_top' => 22
    //         ])->stream('category-report.pdf');
            
    //     }else{
    //         return view('product.category_report', compact('org','request','category_invoice_products'));
    //     }      
        
    // }
}
