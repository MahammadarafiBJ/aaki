<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\PaymentTerm;

class PaymentTermController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return PaymentTerm::
        	where('payment_term', 'like', '%'.$request->search.'%')
        	->orWhere('no_of_days', 'like', '%'.$request->search.'%')
        	->paginate(5);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'payment_term' => 'required|max:50',
            'no_of_days' => 'required|max:50|numeric',
        ]);
        PaymentTerm::create([
            'payment_term' => $request->payment_term,
            'no_of_days' => $request->no_of_days,
            'created_by' => Auth::User()->username,         
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'payment_term' => 'required|max:50',
            'no_of_days' => 'required|max:50|numeric',
        ]);
        return PaymentTerm::where('payment_term_id',$request->payment_term_id)->update([
            'payment_term' => $request->payment_term,
            'no_of_days' => $request->no_of_days,
            'updated_by' => Auth::User()->username,         
        ]);
    }

    public function destroy(PaymentTerm $payment_term)
    {
    	return PaymentTerm::where('payment_term_id',$payment_term->payment_term_id)->delete();
    }

    public function get_payment_terms()
    {
        return PaymentTerm::get();
    }

    public function change_date(Request $request)
    {
        $payment_term = PaymentTerm::where('payment_term_id',$request->payment_term_id)->first();
        $day = date('Y-m-d', strtotime('+'.$payment_term['no_of_days'].' day', strtotime($request->bill_date)));
        return $day;
    }
}
