<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Preference extends Model
{
    use SoftDeletes;

    // protected $fillable = [
    //     'bill_prefix', 'bill_no_length','bill_pdf','transfer_prefix','transfer_no_length','transfer_pdf','invoice_prefix','invoice_no_length','invoice_pdf','receipt_prefix','receipt_no_length','receipt_pdf','voucher_prefix','voucher_no_length','voucher_pdf','payment_prefix','payment_no_length','payment_pdf','fiscal_year','discount_on', 'allow_negative_stock','discount', 'discount1', 'card_amount', 'card_discount', 'printer_type','printer_ip','created_by', 'updated_by',
    // ];
    protected $guarded = [];

    protected $primaryKey = 'preference_id';

    protected $dates = ['deleted_at'];

    public function Branch()
    {
        return $this->hasOne('App\Branch','branch_id','branch_id')->withTrashed();
    }
    
    //used the following functions in settings controller for db to db migration
    // public function fetchAccount($account_id)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    // 	$acc = $db_ext->table('accounts')->where('account_id',$account_id)->first();
    //     if(is_null($acc)){
    //         $acc = $db_ext->table('accounts')->where('account_id',1)->first();
    //     }
    //     $account = Account::where('account_code',$acc->account_code)->first();
    // 	return $account;
    // }
    // public function fetchBill($bill_id)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    // 	$bil = $db_ext->table('bills')->where('bill_id',$bill_id)->first();
    //     if(is_null($bil)){
    //         $bil = $db_ext->table('bills')->where('bill_id',1)->first();
    //     }
    //     $bill = Bill::where('bill_no',$bil->bill_no)->first();
    // 	return $bill;
    // }
    // public function fetchCategory($category_id)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    // 	$cat = $db_ext->table('categories')->where('category_id',$category_id)->first();
    //     if(is_null($cat)){
    //         $cat = $db_ext->table('categories')->where('category_id',1)->first();
    //     }
    //     $category = Category::where('category_code',$cat->category_code)->first();
    // 	return $category;
    // }
    // public function fetchContact($contact_id)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    // 	$con = $db_ext->table('contacts')->where('contact_id',$contact_id)->first();
    //     if(is_null($con)){
    //         $con = $db_ext->table('contacts')->where('contact_id',1)->first();
    //     }
    //     // $old_count = $db_ext->table('contacts')->where('mobile_no',$con->mobile_no)->count();
    //     // $new_count = Contact::where('mobile_no',$con->mobile_no)->count();
    //     // if(($old_count>1)&&($new_count==1)){
    //     //     $contact = Contact::where('mobile_no',$con->mobile_no)->first();    
    //     // }else{
    //         $contact = Contact::where('contact_code',$con->contact_code)->first();    
    //     // }
    //     return $contact;
    // }
    // public function fetchInvoice($invoice_id)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    // 	$rec = $db_ext->table('receipt_invoices')->where('invoice_id',$invoice_id)->first();
    //     if(is_null($rec)){
    //         $rec = $db_ext->table('receipt_invoices')->where('invoice_id',1)->first();
    //     }
    // 	$inv = $db_ext->table('invoices')->where('invoice_id',$rec->invoice_id)->first();
    //     $invoice = Invoice::where('invoice_no',$inv->invoice_no)->first();
    //     return $invoice;
    // }
    // public function fetchPlace($place_id)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    // 	$plc = $db_ext->table('places')->where('place_id',$place_id)->first();
    //     if(is_null($plc)){
    //         $plc = $db_ext->table('places')->where('place_id',1)->first();
    //     }
    //     $place = Place::where('place_code',$plc->place_code)->first();
    // 	return $place;
    // }
    // public function fetchProduct($product_id)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    // 	$pro = $db_ext->table('products')->where('product_id',$product_id)->first();
    //     if(is_null($pro)){
    //         $pro = $db_ext->table('products')->where('product_id',1)->first();
    //     }
    //     $product = Product::where('product_code',$pro->product_code)->first();
    // 	return $product;
    // }
    // public function fetchPurchase($purchase_id)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    // 	$purc = $db_ext->table('purchases')->where('purchase_id',$purchase_id)->first();
    //     if(is_null($purc)){
    //         $purc = $db_ext->table('purchases')->where('purchase_id',1)->first();
    //     }
    //     $purchase = Bill::where('bill_no',$purc->bill_no)->first();
    // 	return $purchase;
    // }
    // public function fetchTax($tax_id)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    // 	$old_tax = $db_ext->table('taxes')->where('tax_id',$tax_id)->first();
    //     if(is_null($old_tax)){
    //         $old_tax = $db_ext->table('taxes')->where('tax_id',1)->first();
    //     }
    //     $tax = Tax::where('tax_rate',$old_tax->tax_rate)->first();
    // 	return $tax;
    // }
    // public function fetchTerm($terms)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    //     $count = $db_ext->table('terms')->where('term',$terms)->count();
    // 	if(($terms != null)&&($count>0)){
    // 		$ter = $db_ext->table('terms')->where('term',$terms)->first();
    //     	$term = Term::where('term',$ter->term)->first();	
    //     	return $term;
    // 	}else{
    // 		return null;
    // 	}
    // }
    // public function fetchWarehouse($warehouse_id)
    // {
    // 	$db_ext = \DB::connection('mysql_external');
    // 	$ware = $db_ext->table('warehouses')->where('warehouse_id',$warehouse_id)->first();
    //     $warehouse = Warehouse::where('warehouse_name',$ware->warehouse_name)->first();
    // 	return $warehouse;
    // }
    // public function amountCalc($sales_exc,$qty)
    // {
    // 	if($sales_exc == null){
    // 		$sales_exc = 0;
    // 	}
    //     $total = number_format(round((float)($sales_exc*$qty),2),2,'.','');
    // 	return $total;
    // }
    // public function taxAmountCalc($amount,$tax)
    // {
    // 	$total = number_format(round((float)(($amount*$tax)/100),2),2,'.','');
    // 	return $total;
    // }
    // public function discountAmountCalc($amount,$discount,$discount_type)
    // {
    // 	switch($discount_type){
    // 		case "%":
    // 				$total = number_format(round((float)(($amount*$discount)/100),2),2,'.','');
    // 				return $total;
    // 		break;
    // 		case "₹":
    // 				$total = number_format(round((float)($discount),2),2,'.','');
    // 				return $total;
    // 		break;
    // 	}
    // }

    // public function frontEndStatus($data,$main_table,$sub_sub_table){
    //     $model_name = '\\App\\'.$main_table;
    //     $model = new $model_name;
    //     $key = ($model)->getKeyName();
    //     $delete_status = $model->max($key) == $data->$key;
    //     $sub_table = $main_table.'Products';
    //     $edit_status = $data->$sub_table->every(function($value,$key) use($sub_sub_table){
    //                                         $inc = 0;
    //                                         foreach ($sub_sub_table as $sub_value) {
    //                                             $value->load($sub_value);
    //                                             $inc +=  count($value->$sub_value);
    //                                         }
    //                                         return $inc == 0;
    //                                     });
    //     return [
    //         'delete' => $delete_status&&$edit_status,
    //         'edit'   => $edit_status,
    //     ];
       
    // }
}
