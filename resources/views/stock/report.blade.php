@php
    if($request->display_type=='excel')
    {
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=stock-report.xls");
    }
@endphp
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/report.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <table border="1">
        <thead>
            <tr>
                <th @if($request->stock_type == 'Main Stock') colspan="9" @else colspan="8" @endif class="text-center">STOCK REPORT 
                    @if($request->branch_id!='')
                       ( Branch : {{ $auth_branch->branch_name }} )
                    @endif
                </th>
            </tr>
            <tr>
                <td @if($request->stock_type == 'Main Stock') colspan="9" @else colspan="8" @endif>
                    <div>
                        <strong>{{ $org->org_name }}</strong>
                    </div>
                    <div>{{ $org->address }}</div>
                    <div>Email: {{ $org->email }}</div>
                    <div>Phone: {{ $org->mobile_no }}</div>
                    <div>PAN: {{ $org->pan_no }}</div>
                    <div>GSTIN: {{ $org->gstin_no }}</div>
                </td>
            </tr>
            <tr>
                <th class="text-center">#</th>
                <th >Product</th>
                <th >Category</th>
                <th class="text-center">Opening Stock</th>
                @if(Auth::User()->user_role == 'Super Admin' && $request->stock_type == 'Main Stock' )
                <th class="text-center">Purchase Stock</th>
                @endif
                <th class="text-center">Transfer Stock</th>
                <th class="text-center">Transfer Return Stock</th>
                <th class="text-center">Sales Stock</th>
                <th class="text-center">Closing Stock</th>
            </tr>
        </thead>
        <tbody>
        @foreach($result as $key => $product)
        @if($product['opening_stock']!=0 || $product['transfer_stock']!=0 || $product['sales_stock']!=0 || $product['closing_stock']!=0  )
            <tr>
                <td class="text-center">{{ $key+1 }}</td>
                <td>{{ $product['product_name'] }}</td>
                <td>{{ $product['category_name'] }}</td>
                <td class="text-center">{{ $product['opening_stock'] }}</td>
                @if(Auth::User()->user_role == 'Super Admin' && $request->stock_type == 'Main Stock' )
                <td class="text-center">{{ $product['purchase_stock'] }}</td>
                @endif
                <td class="text-center">{{ $product['transfer_stock'] }}</td>
                <td class="text-center">{{ $product['transfer_return_stock'] }}</td>
                <td class="text-center">{{ $product['sales_stock'] }}</td>
                <td class="text-center">{{ $product['closing_stock'] }}</td>
            </tr>
        @endif
        @endforeach
        </tbody>
    </table>
</body>
</html>