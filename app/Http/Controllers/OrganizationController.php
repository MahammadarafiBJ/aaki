<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;
use Auth;

class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_organization()
    {
        return Organization::first();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'org_code'  => 'required|max:50',
            'org_name'  => 'required|max:50',
            'email'     => 'sometimes|nullable|email|max:50',
            'mobile_no' => 'sometimes|nullable|digits:10|regex:/[0-9]{10}/|numeric',
            'phone_no'  => 'sometimes|nullable|digits:11|regex:/[0-9]{10}/|numeric',
            'website'   => 'sometimes|nullable|url|max:50',
            'address'   => 'max:255',
            'pan_no'    => 'sometimes|nullable|max:10|min:10',
            'gstin_no'  => 'sometimes|nullable|max:15|min:15',
        ]);

        Organization::where('org_id',$request->org_id)->update([
            'org_code'  => $request->org_code,
            'org_name'  => $request->org_name,
            'email'     => $request->email,
            'mobile_no' => $request->mobile_no,
            'phone_no'  => $request->phone_no,
            'website'   => $request->website,
            'address'   => $request->address,
            'pan_no'    => $request->pan_no,
            'gstin_no'  => $request->gstin_no,
            'updated_by'=> Auth::User()->username,         
        ]);

        $data = $request->logo;
        if(strpos($data, 'base64') !== false) 
        {
            $image_parts = explode(";base64,", $request->logo);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = date('Ymdhis').".".$image_type_aux[1];
            $path = public_path().'/storage/organization/' .$png_url;
            file_put_contents($path, $image_base64);
            Organization::where('org_id',$request->org_id)->update([
                'logo'  => $png_url,
            ]);
        }
        return Organization::where('org_id',$request->org_id)->first();
    }
}
