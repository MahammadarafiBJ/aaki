<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use PDF;
use App\Income;
use App\Account;
use App\Contact;
use App\Term;
use App\Preference;
use App\Organization;

class IncomeController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_receipt_no(Request $request)
    {
        $preference = Preference::first();
        $id = Income::where('fiscal_year',$preference->fiscal_year)->max('income_id');
        $id = $id + 1;
        $n = $preference->receipt_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $receipt_no = $preference->receipt_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $receipt_no;
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'receipt_no'        => 'required|max:50',
            'receipt_date'      => 'required|date',
            'account_id'        => 'required|numeric',
            'contact_id'        => 'required|numeric',
            'payment_mode'      => 'required',
            'reference_no'      => 'max:50',
            'reference_date'    => 'sometimes|nullable|date',
            'amount'            =>'required|numeric',
            'term_id'           => 'sometimes|nullable|numeric',
            'terms'             => 'max:2550',
            'note'              => 'max:2550',
        ]);	

        $preference = Preference::first();

        $income = Income::create([
            'fiscal_year'       => $preference->fiscal_year,
            'receipt_no'        => $request->receipt_no,
            'receipt_date'      => date("Y-m-d", strtotime($request->receipt_date)),
            'account_id'        => $request->account_id,
            'contact_id'        => $request->contact_id,
            'payment_mode'      => $request->payment_mode,
            'reference_no'      => $request->reference_no,
            'reference_date'    => $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'amount'            => $request->amount,
            'term_id'           =>$request->term_id,
            'terms'             =>$request->terms,
            'note'              => $request->note,
            'created_by'        => Auth::User()->username,
        ]);
    }

    public function display(Request $request)
    {
        return Income::
            whereHas('Contact', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhereHas('Account', function($query) use($request){
                $query->where('account_name','like', "%$request->search%");
            })
            ->orWhere('receipt_no', 'like', '%'.$request->search.'%')
            ->orWhere('receipt_date', 'like', '%'.$request->search.'%')
            ->orWhere('amount', 'like', '%'.$request->search.'%')
            ->with('Contact')
            ->with('Account')
            ->orderBy('income_id','DESC')
            ->paginate(10);
    }

    public function view(Income $income)
    {
        return  Income::where('income_id',$income->income_id)
            ->with('Contact')->with('Account')
            ->first();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'receipt_no'        => 'required|max:50',
            'receipt_date'      => 'required|date',
            'account_id'        => 'required|numeric',
            'contact_id'        => 'required|numeric',
            'payment_mode'      => 'required',
            'reference_no'      => 'max:50',
            'reference_date'    => 'sometimes|nullable|date',
            'amount'            =>'required|numeric',
            'term_id'           => 'sometimes|nullable|numeric',
            'terms'             => 'max:2550',
            'note'              => 'max:2550',
        ]); 

        $preference = Preference::first();

        $income = Income::where('income_id',$request->income_id)->update([
            'fiscal_year'       => $preference->fiscal_year,
            'receipt_no'        => $request->receipt_no,
            'receipt_date'      => date("Y-m-d", strtotime($request->receipt_date)),
            'account_id'        => $request->account_id,
            'contact_id'        => $request->contact_id,
            'payment_mode'      => $request->payment_mode,
            'reference_no'      => $request->reference_no,
            'reference_date'    => $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'amount'            => $request->amount,
            'term_id'           =>$request->term_id,
            'terms'             =>$request->terms,
            'note'              => $request->note,
            'updated_by'        => Auth::User()->username,
        ]);
    }

    public function destroy(Income $income)
    {
        Income::where('income_id',$income->income_id)->delete();
    }

    // public function report(Request $request)
    // {
    //     $org = Organization::first();
    //     $incomes = Income::whereBetween('receipt_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))))
    //         ->with(['Contact','Account','Term'])->orderBy('receipt_date','desc')->get();

    //     $contact = Contact::where('contact_id',$request->contact_id)->first();

    //     if($request->contact_id!=null) {
    //             $incomes = $incomes->filter(function($value,$key) use($request){
    //                 return ($value->contact_id == $request->contact_id);
    //             })->values();
    //         }

    //     if($request->display_type=='display')
    //     {
    //         return compact('incomes','contact');
    //     }
    //     else if($request->display_type=='pdf')
    //     {
    //         PDF::loadView('income.report', compact('org','request','incomes','contact'), [], [
    //             'margin_top' => 41.8
    //         ])->stream('income-report.pdf');
    //     }
    //     else
    //     {
    //         return view('income.report', compact('org','request','incomes','contact'));
    //     }
    // }

    public function pdf(Income $income)
    {
        $org = Organization::first();

        PDF::loadView('income.pdf', compact('org','income'), [], [
            'margin_top' => 49 ,
        ])->stream($income->receipt_no.'.pdf');

    }
}
