<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="description" content="STOCK-BOOK" />  
        <meta name="author" content="MDBS Tech Private Limited">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link href="{{ asset('storage/favicon.ico') }}" rel="shortcut icon" type="image/png">
        <link href="{{ asset('vendors/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('vendors/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
        <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('vendors/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
    </head>
    <body class="app header-fixed footer-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
        <div id="app">
            <Myheader :org="{{ $org }}" :preference="{{ $preference }}" :user="{{ Auth::User() }}" :branch="{{ $branch }}" ></Myheader>
            <div class="app-body">
                <Mymenu :org="{{ $org }}" :preference="{{ $preference }}" :user="{{ Auth::User() }}" :branch="{{ $branch }}" ></Mymenu>
                <router-view :org="{{ $org }}" :preference="{{ $preference }}" :user="{{ Auth::User() }}" :branch="{{ $branch }}" ></router-view>
            </div>
            <Myfooter></Myfooter>
        </div>
        
        <script src="{{ asset('vendors/jquery/js/jquery.min.js') }}"></script>
        <script src="{{ asset('vendors/pace-progress/js/pace.min.js') }}"></script>
        <script src="{{ asset('vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
        <script src="{{ asset('vendors/@coreui/coreui/js/coreui.min.js') }}"></script>
    </body>
</html>
