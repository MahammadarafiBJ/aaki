@php
    if($request->display_type=='excel')
    {
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=transfer-report.xls");
    }
@endphp
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/report.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <table border="1">
        <thead>
            <tr>
                <th colspan="12" class="text-center">TRANSFER REPORT</th>
            </tr>
            <tr>
                <td colspan="12">
                    <div>
                        <strong>{{ $org->org_name }}</strong>
                    </div>
                    <div>{{ $org->address }}</div>
                    <div>Email: {{ $org->email }}</div>
                    <div>Phone: {{ $org->mobile_no }}</div>
                    <div>PAN: {{ $org->pan_no }}</div>
                    <div>GSTIN: {{ $org->gstin_no }}</div>
                </td>
            </tr>
        </thead>
        <tbody>
        @if($request->report_type == 'Brief Report')
            <tr>
                <th class="text-center">#</th>
                <th class="text-nowrap">Transfer No</th>
                <th class="text-nowrap">Transfer Date</th>
                <th class="text-nowrap">Branch</th>
                <th class="text-right" colspan="2">Sub Total</th>
                @if($preference->discount_on=='Inc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Discount</th>
                @if($preference->discount_on=='Exc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Total Amount</th>
                <th class="text-right">Round off</th>
                <th class="text-right" colspan="2">Grand Total</th>
            </tr>
            @php
                $sub_total = 0;
                $discount_amount = 0;
                $tax_amount = 0;
                $total_amount = 0;
                $round_off = 0;
                $grand_total = 0;
            @endphp
            @foreach($transfers as $key => $transfer)
            <tr>
                <td class="text-center">
                        {{ $key+1 }}
                    </td>
                    <td>
                        {{ $transfer->transfer_no }}
                    </td>
                    <td>
                        {{ date('d-m-Y',strtotime($transfer->transfer_date)) }}
                    </td>
                    <td>
                        {{ $transfer->Branch->branch_name }}
                    </td>
                    <td class="text-right" colspan="2">
                        {{ number_format($transfer->sub_total,2) }}
                    </td>
                    @if($preference->discount_on=='Inc')
                    <td class="text-right">
                        {{ number_format($transfer->tax_amount,2) }}
                    </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($transfer->discount_amount,2) }}
                    </td>
                    @if($preference->discount_on=='Exc')
                    <td class="text-right">
                        {{ number_format($transfer->tax_amount,2) }}
                    </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($transfer->total_amount,2) }}
                    </td>
                    <td class="text-right">
                        {{ number_format($transfer->round_off,2) }}
                    </td>
                    <td class="text-right" colspan="2">
                        {{ number_format($transfer->grand_total,2) }}
                    </td>
                    @php
                        $sub_total += $transfer->sub_total;
                        $discount_amount += $transfer->discount_amount;
                        $tax_amount += $transfer->tax_amount;
                        $total_amount += $transfer->total_amount;
                        $round_off += $transfer->round_off;
                        $grand_total += $transfer->grand_total;
                    @endphp
            </tr>
            @endforeach
            <tr>
                <th colspan="4" class="text-right">Total</th>
                <th class="text-right" colspan="2">{{ number_format($sub_total,2) }}</th>
                @if($preference->discount_on=='Inc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right" >{{ number_format($discount_amount,2) }}</th>
                @if($preference->discount_on=='Exc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($total_amount,2) }}</th>
                <th class="text-right">{{ number_format($round_off,2) }}</th>
                <th class="text-right" colspan="2">{{ number_format($grand_total,2) }}</th>
            </tr>
        @endif
        @if($request->report_type == 'Detail Report')
            @foreach($transfers as $key=> $transfer)
                <tr>
                    <th colspan="12">{{ $key+1 }}</th>
                </tr>
                <tr>
                    <td colspan="12">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div><b>Transfer No : </b>{{ $transfer->transfer_no }}</div>
                        <div><b>Transfer Date : </b>{{ date('d-m-Y',strtotime($transfer->transfer_date)) }}</div>
                        <div><b>Branch : </b>{{ $transfer->Branch->branch_name }}</div>
                    </td>
                </tr>
                <tr>
                    <th class="text-center">#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th class="text-right">P.Rate(Exc)</th>
                    <th class="text-right">P.Rate(Inc)</th>
                    @if($preference->discount_on=='Inc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right" >Discount</th>
                    @if($preference->discount_on=='Exc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Qty</th>
                    <th class="text-right">Amount</th>
                </tr>
                @foreach($transfer->TransferProducts as $val=> $transfer_product)
                    <tr>
                        <td class="text-center">
                             {{ $val+1 }} 
                        </td>
                        <td>
                            {{ $transfer_product->product_code }}
                        </td>
                        <td>
                            {{ $transfer_product->product_name }}
                        </td>
                        <td class="text-right">
                            {{ number_format($transfer_product->purchase_rate_exc,2) }}
                        </td>
                        <td class="text-right">
                           {{ number_format($transfer_product->purchase_rate_inc,2) }}
                        </td>
                        @if($preference->discount_on=='Inc')
                        <td class="text-right">
                            {{ number_format($transfer_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td class="text-right">
                            {{ number_format($transfer_product->discount_amount,2) }}
                        </td>
                        @if($preference->discount_on=='Exc')
                        <td class="text-right">
                            {{ number_format($transfer_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td class="text-right">
                            {{ number_format($transfer_product->quantity,2) }}
                        </td>
                        <td class="text-right">
                            {{ number_format($transfer_product->amount,2) }}
                        </td>                     
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Terms and Conditions : </strong><br>
                        {{ $transfer->terms }}
                    </td>
                    <th class="text-right" colspan="2">Sub Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($transfer->sub_total,2) }}
                    </th>
                </tr>
                @if($preference->discount_on=='Inc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($transfer->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <th class="text-right" colspan="2">Discount</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($transfer->discount_amount,2) }}
                    </th>
                </tr>
                @if($preference->discount_on=='Exc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($transfer->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Note : </strong><br>
                        {{ $transfer->note  }}
                    </td>
                    <th class="text-right" colspan="2">Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($transfer->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Round off</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($transfer->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Grand Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($transfer->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</body>
</html>