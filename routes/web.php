<?php



Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/{name}',function(){
    return redirect('/');
})->where('name','[A-Za-z]+');

//Dashboard
Route::post('/data/view', 'HomeController@get_dashboard_data');
Route::post('/home/branch_view', 'HomeController@branch_view');

//Organization
Route::post('/organization/get_organization', 'OrganizationController@get_organization');
Route::post('/organization/update', 'OrganizationController@update');

//Preferences
Route::post('/preference/get_preference', 'PreferenceController@get_preference');
Route::post('/preference/update', 'PreferenceController@update');

//Master
Route::post('/master/display', 'MasterController@display');
Route::post('/master/store', 'MasterController@store');
Route::post('/master/destroy/{master}', 'MasterController@destroy');
Route::post('/master/update', 'MasterController@update');
Route::post('/master/get_masters', 'MasterController@get_masters');

//Taxes
Route::post('/tax/display', 'TaxController@display');
Route::post('/tax/store', 'TaxController@store');
Route::post('/tax/update', 'TaxController@update');
Route::post('/tax/destroy/{tax}', 'TaxController@destroy');
Route::post('/tax/get_taxes', 'TaxController@get_taxes');

//Branches
Route::post('/branch/display', 'BranchController@display');
Route::post('/branch/store', 'BranchController@store');
Route::post('/branch/edit/{branch}', 'BranchController@edit');
Route::post('/branch/update', 'BranchController@update');
Route::post('/branch/destroy/{branch}', 'BranchController@destroy');
Route::post('/branch/get_branches', 'BranchController@get_branches');

//Users
Route::post('/user/display', 'UserController@display');
Route::post('/user/store', 'UserController@store');
Route::post('/user/edit/{user}', 'UserController@edit');
Route::post('/user/update', 'UserController@update');
Route::post('/user/destroy/{user}', 'UserController@destroy');
Route::post('/profile/get_users', 'UserController@get_users')->name('profile.get_users');
Route::post('/profile/update', 'UserController@update_profile')->name('profile.update');
Route::post('/change/password', 'UserController@change_password');


//Accounts
Route::post('/account/display', 'AccountController@display');
Route::post('/account/store', 'AccountController@store');
Route::post('/account/destroy/{account}', 'AccountController@destroy');
Route::post('/account/update', 'AccountController@update');
Route::post('/account/get_accounts', 'AccountController@get_accounts');

//Terms
Route::post('/term/display', 'TermController@display');
Route::post('/term/store', 'TermController@store');
Route::post('/term/destroy/{term}', 'TermController@destroy');
Route::post('/term/update', 'TermController@update');
Route::post('/term/get_terms', 'TermController@get_terms');

//Payment Terms
Route::post('/payment_term/display', 'PaymentTermController@display');
Route::post('/payment_term/store', 'PaymentTermController@store');
Route::post('/payment_term/destroy/{payment_term}', 'PaymentTermController@destroy');
Route::post('/payment_term/update', 'PaymentTermController@update');
Route::post('/payment_term/get_payment_terms', 'PaymentTermController@get_payment_terms');
Route::post('/payment_term/change_date', 'PaymentTermController@change_date');

//Place
Route::post('/place/display', 'PlaceController@display');
Route::post('/place/store', 'PlaceController@store');
Route::post('/place/destroy/{place}', 'PlaceController@destroy');
Route::post('/place/update', 'PlaceController@update');
Route::post('/place/get_places', 'PlaceController@get_places');

//Contact
Route::post('/contact/get_contact_code', 'ContactController@get_contact_code');
Route::post('/contact/display', 'ContactController@display');
Route::post('/contact/view/{contact}', 'ContactController@view');
Route::post('/contact/store', 'ContactController@store');
Route::post('/contact/edit/{contact}', 'ContactController@edit');
Route::post('/contact/update', 'ContactController@update');
Route::post('/contact/destroy', 'ContactController@destroy'); 
Route::post('/contact/get_contacts', 'ContactController@get_contacts');
Route::post('/contact/update_card_no', 'ContactController@update_card_no');

// Route::post('/contact/excel_upload', 'ContactController@excel_upload');
// Route::get('/contact/report', 'ContactController@report');
// Route::get('/contact/outstanding_report','ContactController@outstanding_report');
// Route::get('/contact/all_contacts_reports', 'ContactController@all_report');

//Categories
Route::post('/category/display', 'CategoryController@display');
Route::post('/category/get_category_code', 'CategoryController@get_category_code');
Route::post('/category/store', 'CategoryController@store');
Route::post('/category/destroy/{category}', 'CategoryController@destroy');
Route::post('/category/update', 'CategoryController@update');
Route::post('/category/get_categories', 'CategoryController@get_categories');
// Route::get('/category/report', 'CategoryController@report');
// Route::get('/category/pdf/{category}', 'CategoryController@pdf');


//Product
Route::post('/product/get_product_code', 'ProductController@get_product_code');
Route::post('/product/display', 'ProductController@display');
Route::post('/product/store', 'ProductController@store');
Route::post('/product/edit/{product}', 'ProductController@edit');
Route::post('/product/update', 'ProductController@update');
Route::post('/product/destroy/{product}', 'ProductController@destroy');
Route::post('/product/get_products', 'ProductController@get_products');
Route::post('/price/validation', 'ProductController@validation');
Route::get('/product/stock_report', 'ProductController@stock_report');
// Route::post('/product/excel_upload', 'ProductController@excel_upload');
// Route::post('/product/view/{product}', 'ProductController@view');
Route::post('/product/get_barcode', 'ProductController@get_barcode');
Route::post('/product/get_product_list', 'ProductController@get_product_list');


//Expense
Route::post('/expense/get_voucher_no', 'ExpenseController@get_voucher_no');
Route::post('/expense/store', 'ExpenseController@store');
Route::post('/expense/display', 'ExpenseController@display');
Route::post('/expense/view/{expense}', 'ExpenseController@view');
Route::post('/expense/update', 'ExpenseController@update');
Route::post('/expense/destroy/{expense}', 'ExpenseController@destroy');
Route::get('/expense/pdf/{expense}', 'ExpenseController@pdf');
// Route::get('/expense/report', 'ExpenseController@report');

//Income
Route::post('/income/get_receipt_no', 'IncomeController@get_receipt_no');
Route::post('/income/store', 'IncomeController@store');
Route::post('/income/display', 'IncomeController@display');
Route::post('/income/view/{income}', 'IncomeController@view');
Route::post('/income/update', 'IncomeController@update');
Route::post('/income/destroy/{income}', 'IncomeController@destroy');
Route::get('/income/pdf/{income}', 'IncomeController@pdf');
// Route::get('/income/report', 'IncomeController@report');

//Bill
Route::post('/bill/get_bill_no', 'BillController@get_bill_no');
Route::post('/bill/validation', 'BillController@validation');
Route::post('/bill/store', 'BillController@store');
Route::post('/bill/display', 'BillController@display');
Route::post('/bill/view/{bill}', 'BillController@view');
Route::post('/bill/destroy/{bill}', 'BillController@destroy');
Route::post('/bill/update', 'BillController@update');
//Route::post('/bill/get_unpaid_bills', 'BillController@get_unpaid_bills');
// Route::get('/bill/pdf/{bill}', 'BillController@pdf');
// Route::get('/bill/tax_report', 'BillController@tax_report');
// Route::post('/bill/convert_purchase', 'BillController@convert_purchase');
Route::post('/product/add_new_barcode','ProductController@add_new_barcode');
Route::post('/bills/import/excel','BillController@import');
Route::get('/bill/report', 'BillController@report');

//Transfer
Route::post('/transfer/get_transfer_no', 'TransferController@get_transfer_no');
Route::post('/transfer/validation', 'TransferController@validation');
Route::post('/transfer/store', 'TransferController@store');
Route::post('/transfer/display', 'TransferController@display');
Route::post('/transfer/view/{transfer}', 'TransferController@view');
Route::post('/transfer/destroy/{transfer}', 'TransferController@destroy');
Route::post('/transfer/update', 'TransferController@update');
Route::get('/transfer/pdf/{transfer}', 'TransferController@pdf');
Route::get('/transfer/report', 'TransferController@report');

//Invoice
Route::post('/invoice/get_invoice_no', 'InvoiceController@get_invoice_no');
Route::post('/invoice/validation', 'InvoiceController@validation');
Route::post('/invoice/store', 'InvoiceController@store');
Route::post('/invoice/display', 'InvoiceController@display');
Route::post('/invoice/view/{invoice}', 'InvoiceController@view');
Route::post('/invoice/destroy/{invoice}', 'InvoiceController@destroy');
Route::post('/invoice/update', 'InvoiceController@update');
Route::get('/invoice/pdf/{invoice}', 'InvoiceController@pdf');
Route::get('/invoice/report', 'InvoiceController@report');
Route::get('/invoice/print/{invoice}','InvoiceController@print');
Route::post('/invoice/send_message/{invoice}', 'InvoiceController@send_message');
// Route::post('/invoice/get_unpaid_invoices', 'InvoiceController@get_unpaid_invoices');
// Route::get('/invoice/pdf/{invoice}', 'InvoiceController@pdf');
// Route::post('/invoice/convert/delivery', 'InvoiceController@convert_invoice');
// Route::post('/invoice/convert/return', 'InvoiceController@convert_invoice');
// Route::get('/invoice/tax_report', 'InvoiceController@tax_report');

//Transfer
Route::post('/transfer_return/get_transfer_return_no', 'TransferReturnController@get_transfer_return_no');
Route::post('/transfer_return/validation', 'TransferReturnController@validation');
Route::post('/transfer_return/store', 'TransferReturnController@store');
Route::post('/transfer_return/display', 'TransferReturnController@display');
Route::post('/transfer_return/view/{transfer_return}', 'TransferReturnController@view');
Route::post('/transfer_return/destroy/{transfer_return}', 'TransferReturnController@destroy');
Route::post('/transfer_return/update', 'TransferReturnController@update');
Route::get('/transfer_return/pdf/{transfer_return}', 'TransferReturnController@pdf');
Route::get('/transfer_return/report', 'TransferReturnController@report');