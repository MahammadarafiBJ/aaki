<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'branch_code','branch_name','contact_person','email','mobile_no','phone_no','address','logo','created_by','updated_by',
    ];
    protected $primaryKey = 'branch_id';

    protected $dates = ['deleted_at'];

    public function Invoices()
    {
    	return $this->hasMany('App\Invoice','branch_id','branch_id');
    }

    public function Transfers()
    {
    	return $this->hasMany('App\Transfer','branch_id','branch_id');
    }
}
