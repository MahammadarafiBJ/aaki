<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\Imports\ProductImport;
// use Maatwebsite\Excel\Facades\Excel;
// use Validator;

use App\Organization;
use App\Contact;
// use App\Invoice;

// use App\Bill;
// use App\Receipt;
// use App\Payment;
// use App\Income;
// use App\Expense;
use Auth;
use PDF;

class ContactController extends Controller
{
 	public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_contact_code(Request $request)
    {
        $id = Contact::withTrashed()->max('contact_id');
        $contact_code = $id + 1;
        $length = strlen($contact_code);
        $n = 3 - $length;
        for ($i=0; $i < $n; $i++) 
        { 
            $contact_code = '0'.$contact_code;
        }
        return $contact_code;
    }  

    public function display(Request $request)
    {
        return Contact::
            where('contact_code', 'like', '%'.$request->search.'%')
            ->orWhere('contact_type', 'like', '%'.$request->search.'%')
            ->orWhere('contact_name', 'like', '%'.$request->search.'%')
            ->orWhere('mobile_no', 'like', '%'.$request->search.'%')
            ->orWhere('email', 'like', '%'.$request->search.'%')
            ->paginate(10);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'contact_type'          => 'required|max:50',
            'contact_code'          => 'required|max:50',
            'contact_name'          => 'required|max:255',
            'email'                 => 'sometimes|nullable|email|max:50',
            // 'mobile_no'  => 'sometimes|nullable|regex:/[0-9]{10}/',
            'mobile_no'             => 'required|unique:contacts,mobile_no|digits:10|regex:/[0-9]{10}/|numeric',
            'phone_no'              => 'sometimes|nullable|regex:/[0-9]{10}/|digits:11|numeric',
            'pan_no'                => 'sometimes|nullable|max:10|min:10',
            'gstin_no'              => 'sometimes|nullable|max:15|min:15',
            'billing_address'       => 'max:255',
            'shipping_address'      => 'max:255',
            'account_no'            => 'max:50',
            'bank_name'             => 'max:50',
            'branch_name'           => 'max:50',
            'ifsc_code'             => 'max:50',
            'date_of_birth'         => 'sometimes|nullable|date',
            'wedding_anniversary'   => 'sometimes|nullable|date',
            'payment_term_id'       => 'numeric|required',
            'place_id'              => 'numeric|required',
            'card_no'               => 'sometimes|nullable|unique:contacts'
        ]);

        return Contact::create([
            'contact_type'          => $request->contact_type,
            'contact_code'          => $request->contact_code,
            'contact_name'          => $request->contact_name,
            'email'                 => $request->email,
            'mobile_no'             => $request->mobile_no,
            'phone_no'              => $request->phone_no,
            'pan_no'                => $request->pan_no,
            'gstin_no'              => $request->gstin_no,
            'billing_address'       => $request->billing_address,
            'shipping_address'      => $request->shipping_address,
            'account_no'            => $request->account_no,
            'bank_name'             => $request->bank_name,
            'branch_name'           => $request->branch_name,
            'ifsc_code'             => $request->ifsc_code,
            'payment_term_id'       => $request->payment_term_id,
            'place_id'              => $request->place_id,
            'date_of_birth'         => $request->date_of_birth ? date('Y-m-d',strtotime($request->date_of_birth)) : Null,
            'wedding_anniversary'   => $request->wedding_anniversary ? date('Y-m-d',strtotime($request->wedding_anniversary)) : Null,
            'card_no'               => $request->card_no,
            'created_by'            => Auth::User()->username,
        ]);
        return redirect()->back()->with('success-message','Contact '.$contact->contact_name.' is successfully created...!');
    }

    public function edit(Contact $contact)
    {
    	return $contact;
    }
    public function view(Contact $contact)
    {
        switch ($contact->contact_type){
        case "Customer":
            $contact = Contact::where('contact_id',$contact->contact_id)->first();
            break;
        case "Vendor":
            $contact = Contact::where('contact_id',$contact->contact_id)->first();
            break;
        } 
        return $contact;
    }
    public function update_card_no(Request $request)
    {
        $this->validate($request,[
            'card_no' => 'sometimes|nullable|unique:contacts,card_no,'.$request->contact_id.',contact_id',
        ]);

        Contact::where('contact_id',$request->contact_id)->update([
            'card_no' => $request->card_no,
        ]);
        
    }
    public function destroy(Request $request)
    {
        Contact::where('contact_id',$request->contact_id)->delete();
    }
    public function update(Request $request)
    {
    	$contact = Contact::where('contact_id',$request->contact_id)->first();
        $this->validate($request, [
            'contact_type'          => 'required|max:50',
            'contact_code'          => 'required|max:50',
            'contact_name'          => 'required|max:255',
            'email'                 => 'sometimes|nullable|email|max:50',
            'mobile_no'             => 'sometimes|nullable|regex:/[0-9]{10}/|digits:10|numeric',
            'phone_no'              => 'sometimes|nullable|regex:/[0-9]{10}/|digits:11|numeric',
            'pan_no'                => 'sometimes|nullable|max:10|min:10',
            'gstin_no'              => 'sometimes|nullable|max:15|min:15',
            'billing_address'       => 'max:255',
            'shipping_address'      => 'max:255',
            'account_no'            => 'max:50',
            'bank_name'             => 'max:50',
            'branch_name'           => 'max:50',
            'ifsc_code'             => 'max:50',
            'date_of_birth'         => 'sometimes|nullable|date',
            'wedding_anniversary'   => 'sometimes|nullable|date',
            'payment_term_id'       => 'numeric|required',
            'place_id'              => 'numeric|required',
            'card_no'               => 'sometimes|nullable|unique:contacts,card_no,'.$request->contact_id.',contact_id',
        ]);

        Contact::where('contact_id', $contact->contact_id)->update([
            'contact_type'          => $request->contact_type,
            'contact_code'          => $request->contact_code,
            'contact_name'          => $request->contact_name,
            'email'                 => $request->email,
            'mobile_no'             => $request->mobile_no,
            'phone_no'              => $request->phone_no,
            'pan_no'                => $request->pan_no,
            'gstin_no'              => $request->gstin_no,
            'billing_address'       => $request->billing_address,
            'shipping_address'      => $request->shipping_address,
            'account_no'            => $request->account_no,
            'bank_name'             => $request->bank_name,
            'branch_name'           => $request->branch_name,
            'ifsc_code'             => $request->ifsc_code,
            'payment_term_id'       => $request->payment_term_id,
            'place_id'              => $request->place_id,
            'date_of_birth'         => $request->date_of_birth ? date('Y-m-d',strtotime($request->date_of_birth)) : $contact->date_of_birth,
            'wedding_anniversary'   => $request->wedding_anniversary ? date('Y-m-d',strtotime($request->wedding_anniversary)) : $contact->wedding_anniversary,
            'card_no'               => $request->card_no,
            'created_by'            => Auth::User()->username,
        ]);
    }

    public function get_contacts(Request $request)
    {
        if ($request->contact_type == 'Contact') {

            $contact =  Contact::where('contact_name', 'like', '%'.$request->search.'%')
                        ->orwhere('mobile_no', 'like', '%'.$request->search.'%')
                        ->limit(10)
                        ->orderBy('contact_id')
                        ->get();

        }else{
            $contact =  Contact::where('contact_type',$request->contact_type)
            ->where(function($query) use($request){
                $query->where('contact_name', 'like', '%'.$request->search.'%')
                      ->orwhere('mobile_no', 'like', '%'.$request->search.'%');
            })
            ->limit(10)
            ->orderBy('contact_id')
            ->get();
        }
        
        return $contact;

    }
    // public function excel_upload(Request $request)
    // {
    //     if($request->hasFile('contact_excel')){
    //         Validator::make(
    //             [
    //                 'extension' => strtolower($request['contact_excel']->getClientOriginalExtension()),
    //             ],
    //             [
    //                 'extension'      => 'required|in:xlsx,xls,ods',
    //             ]
    //         )->validate();

    //         $excel_contacts =  (new ProductImport)->toCollection(request()->file('contact_excel'));
    //         $contact_array = $excel_contacts->toArray();
    //         $contacts = $contact_array[0];
            
    //         Validator::make($contacts, [
    //             "*.contact_type" => 'required',
    //             "*.contact_code" => 'required',
    //             "*.contact_name"   => 'required',
    //             // "*.place"            => 'required|exists:places,place_name',
    //             // "*.payment_term"     => 'required|exists:payment_terms,payment_term',
    //         ])->validate();
            
    //         foreach($contacts as $contact){
    //             // $payment_term = payment_term::where('payment_term',$product['payment_term'])->first();
    //             // $place = Place::where('place_name',$product['place'])->first();
    //             Contact::create([
    //                 'contact_type' => $contact['contact_type'],
    //                 'contact_code' => $contact['contact_code'],
    //                 'contact_name' => $contact['contact_name'],
    //                 'email'        => $contact['email'],
    //                 'mobile_no'    => $contact['mobile_no'],
    //                 'phone_no'     => $contact['phone_no'],
    //                 // 'pan_no'       => $contact['pan_no'],
    //                 // 'gstin_no'     => $contact['gstin_no'],
    //                 // 'billing_address'  => $contact['billing_address'],
    //                 // 'shipping_address' => $contact['shipping_address'],
    //                 // 'account_no'       => $contact['account_no'],
    //                 // 'bank_name'        => $contact['bank_name'],
    //                 // 'branch_name'      => $contact['branch_name'],
    //                 // 'ifsc_code'        => $contact['ifsc_code'],
    //                 'payment_term_id'  => 1,
    //                 'place_id'         => 1,
    //                 // 'date_of_birth' => $contact['date_of_birth'] ? date('Y-m-d',strtotime($contact['date_of_birth'])) : Null,
    //                 // 'wedding_anniversary' => $contact['wedding_anniversary'] ? date('Y-m-d',strtotime($contact['wedding_anniversary'])) : Null,
    //                 'created_by' => Auth::User()->username,
    //             ]);
    //         }
    //     }
    // }

    // public function report(Request $request)
    // {
    //     $org = Organization::first();

    //     $incomes = Income::whereHas('Contact',function($q) use($request){
    //         $q->where('contact_type',$request->contact_type);
    //     })->whereBetween('receipt_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))))
    //         ->with(['Contact'])->orderBy('receipt_date','desc')->get();

    //     $expenses = Expense::whereHas('Contact',function($q) use($request){
    //         $q->where('contact_type',$request->contact_type);
    //     })->whereBetween('voucher_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))))
    //         ->with(['Contact'])->orderBy('voucher_date','desc')->get();

    //     $openingIncome = Income::whereHas('Contact',function($q) use($request){
    //         $q->where('contact_type',$request->contact_type);
    //     })->whereDate('receipt_date','<',$request->from_date)->sum('amount');            
    //     $openingExpense = Expense::whereHas('Contact',function($q) use($request){
    //         $q->where('contact_type',$request->contact_type);
    //     })->whereDate('voucher_date','<',$request->from_date)->sum('amount');

        
    //     if ($request->contact_type == 'Customer') {
    //         //Customer report
    //         $invoices = Invoice::whereBetween('invoice_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))))
    //         ->with(['Customer','InvoiceProducts'])->orderBy('invoice_date','desc')->get();

    //         $receipts = Receipt::whereBetween('receipt_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))))
    //         ->with(['Customer','ReceiptParticulars'])->orderBy('receipt_date','desc')->get();

    //         //opening amounts
    //         $openingInvoice = Invoice::whereDate('invoice_date','<',$request->from_date)->sum('grand_total');

    //         $openingReceipt = Receipt::whereDate('receipt_date','<',$request->from_date)->sum('total_amount');

    //         $customer  =  Contact::where('contact_id',$request->customer_id)->first();
            
    //         if($request->customer_id!=null) {
    //             //report filter cutomer
    //             $invoices = $invoices->filter(function($value,$key) use($request){
    //                 return ($value->customer_id == $request->customer_id);
    //             })->values();
    //             $receipts = $receipts->filter(function($value,$key) use($request){
    //                 return ($value->customer_id == $request->customer_id);
    //             })->values();

    //             $incomes = $incomes->filter(function($value,$key) use($request){
    //                 return ($value->contact_id == $request->customer_id);
    //             })->values();
    //             $expenses = $expenses->filter(function($value,$key) use($request){
    //                 return ($value->contact_id == $request->customer_id);
    //             })->values();

    //             //opening amount filter cutomer
    //             $openingInvoice = Invoice::where('customer_id',$request->customer_id)
    //             ->whereDate('invoice_date','<',$request->from_date)->sum('grand_total');

    //             $openingReceipt = Receipt::where('customer_id',$request->customer_id)
    //             ->whereDate('receipt_date','<',$request->from_date)->sum('total_amount');

    //             $openingIncome = Income::where('contact_id',$request->customer_id)
    //             ->whereDate('receipt_date','<',$request->from_date)->sum('amount');

    //             $openingExpense = Expense::where('contact_id',$request->customer_id)
    //             ->whereDate('voucher_date','<',$request->from_date)->sum('amount');

    //         }
            
    //         $openingPaymentTotal = $openingInvoice + $openingExpense;
    //         $openingReceiptTotal = $openingReceipt + $openingIncome;

    //         if($request->display_type=='display')
    //         {
    //             return compact('invoices','receipts','incomes','expenses','customer','openingPaymentTotal','openingReceiptTotal');
    //         }
    //         else if($request->display_type=='pdf')
    //         {
    //             PDF::loadView('contact.customer.report', compact('org','request','invoices','receipts','incomes','expenses','customer','openingPaymentTotal','openingReceiptTotal'), [], [
    //                 'margin_top' => 41.8
    //             ])->stream('customer-report.pdf');
    //         }
    //         else
    //         {
    //             return view('contact.customer.report', compact('org','request','invoices','receipts','incomes','expenses','customer','openingPaymentTotal','openingReceiptTotal'));
    //         }

    //     }else{
    //         //vendor report vendor
    //         $bills = Bill::whereBetween('bill_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))))
    //         ->with(['Vendor','BillProducts'])->orderBy('bill_date','desc')->get();

    //         $payments = Payment::whereBetween('voucher_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))))
    //         ->with(['Vendor','PaymentParticulars'])->orderBy('voucher_date','desc')->get();

    //         //opening amounts vendor
    //         $openingBill = Bill::whereDate('bill_date','<',$request->from_date)->sum('grand_total');
    //         $openingPayment = Payment::whereDate('voucher_date','<',$request->from_date)->sum('total_amount');

    //         $vendor  =  Contact::where('contact_id',$request->vendor_id)->first();
            
    //         if($request->vendor_id!=null) {
    //              //report filter
    //             $bills = $bills->filter(function($value,$key) use($request){
    //                 return ($value->vendor_id == $request->vendor_id);
    //             })->values();

    //             $payments = $payments->filter(function($value,$key) use($request){
    //                 return ($value->vendor_id == $request->vendor_id);
    //             })->values();

    //             $incomes = $incomes->filter(function($value,$key) use($request){
    //                 return ($value->contact_id == $request->vendor_id);
    //             })->values();

    //             $expenses = $expenses->filter(function($value,$key) use($request){
    //                 return ($value->contact_id == $request->vendor_id);
    //             })->values();

    //             //opening amount filter vendor
    //             $openingBill = Bill::where('vendor_id',$request->vendor_id)
    //             ->whereDate('bill_date','<',$request->from_date)->sum('grand_total');

    //             $openingPayment = Payment::where('vendor_id',$request->vendor_id)
    //             ->whereDate('voucher_date','<',$request->from_date)->sum('total_amount');
    //             $openingIncome = Income::where('contact_id',$request->vendor_id)
    //             ->whereDate('receipt_date','<',$request->from_date)->sum('amount');

    //             $openingExpense = Expense::where('contact_id',$request->vendor_id)
    //             ->whereDate('voucher_date','<',$request->from_date)->sum('amount');
    //         }

    //         $openingPaymentTotal = $openingBill + $openingExpense;
    //         $openingReceiptTotal = $openingPayment + $openingIncome;

    //         if($request->display_type=='display')
    //         {
    //             return compact('bills','payments','incomes','expenses','vendor','openingPaymentTotal','openingReceiptTotal');
    //         }
    //         else if($request->display_type=='pdf')
    //         {
    //             PDF::loadView('contact.vendor.report', compact('org','request','bills','payments','incomes','expenses','vendor','openingPaymentTotal','openingReceiptTotal'), [], [
    //                 'margin_top' => 41.8
    //             ])->stream('vendor-report.pdf');
    //         }
    //         else
    //         {
    //             return view('contact.vendor.report', compact('org','request','bills','payments','incomes','expenses','vendor','openingPaymentTotal','openingReceiptTotal'));
    //         }

    //     }
    // }
    // public function outstanding_report(Request $request)
    // {
    //     $org = Organization::first();
    //     switch($request->contact_type){
    //         case 'Customer':
    //             $contacts = Contact::where('contact_type',$request->contact_type)
    //                 ->whereHas('CustomerInvoiceReceipt',function($query)use($request){
    //                     $query->whereBetween('invoice_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))));
    //                 })->get();

    //             if($request->customer_id!=null){
    //                 $contacts = $contacts->filter(function($val)use($request){
    //                     return $val->contact_id == $request->customer_id;
    //                 })->values();
    //             }

    //             if(count($contacts)>0){
    //                 $contacts->load(['CustomerInvoiceReceipt'=>function($query)use($request){
    //                     $query->whereBetween('invoice_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))));
    //                 }]);

    //                 $contacts = $contacts->each(function($value){
    //                     $value->invoice_receipts = $value->CustomerInvoiceReceipt->filter(function($val){
    //                         return ((is_null($val->InvoiceReceiptTotal)||($val->InvoiceReceiptTotal->paid_amount<$val->grand_total))&&($val->grand_total>0));
    //                         })->values();
    //                 })->filter(function($value){
    //                     return count($value->invoice_receipts)>0;
    //                 })->values();
    //             }
    //             if($request->display_type=='display'){
    //                 return $contacts;
    //                 // return compact('contacts');
    //             }else if($request->display_type=='pdf'){
    //                 PDF::loadView('contact.customer.outstanding_report', compact('org','request','contacts'), [], [
    //                     'margin_top' => 41.8
    //                 ])->stream('customer-outstanding-report.pdf');
    //             }else{
    //                 return view('contact.customer.outstanding_report', compact('org','request','contacts'));
    //             }
    //         break;
    //         case 'Vendor':
    //             $contacts = Contact::where('contact_type',$request->contact_type)
    //                 ->whereHas('VendorBills',function($query)use($request){
    //                     $query->whereBetween('bill_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))));
    //                 })->get();

    //             if($request->vendor_id!=null){
    //                 $contacts = $contacts->filter(function($val)use($request){
    //                     return $val->contact_id == $request->vendor_id;
    //                 })->values();
    //             }

    //             if(count($contacts)>0){
    //                 $contacts->load(['VendorBillPayment'=>function($query)use($request){
    //                     $query->whereBetween('bill_date',array(date('Y-m-d',strtotime($request->from_date)),date('Y-m-d',strtotime($request->to_date))));
    //                 }]);
    //                 $contacts = $contacts->each(function($value){
    //                     $value->bill_payments = $value->VendorBillPayment->filter(function($val){
    //                         return ((is_null($val->BillPaymentTotal)||($val->BillPaymentTotal->paid_amount<$val->grand_total))&&($val->grand_total>0));
    //                         });
    //                 })->filter(function($value){
    //                     return count($value->bill_payments)>0;
    //                 })->values();
    //             }
    //             if($request->display_type=='display'){
    //                 return $contacts;
    //                 // return compact('contacts');
    //             }else if($request->display_type=='pdf'){
    //                 PDF::loadView('contact.vendor.outstanding_report', compact('org','request','contacts'), [], [
    //                     'margin_top' => 41.8
    //                 ])->stream('vendor-outstanding-report.pdf');
    //             }else{
    //                 return view('contact.vendor.outstanding_report', compact('org','request','contacts'));
    //             }
    //         break;
    //     }
        
    // }

    // public function all_report(Request $request)
    // {
    //     $org = Organization::first();
    //     $contacts = Contact::get();
    //     //dd('grj');
    //     //dd($org);
    //     return view('contact.all.allreport', compact('org','contacts'));
    //     // PDF::loadView('contact.all.all_report', compact('org','request','contacts'), [], [
    //     //                 'margin_top' => 41.8
    //     //             ])->stream('contact-report.pdf');
    // }
}
