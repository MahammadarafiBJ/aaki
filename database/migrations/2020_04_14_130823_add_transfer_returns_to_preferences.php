<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransferReturnsToPreferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preferences', function (Blueprint $table) {
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preferences', function (Blueprint $table) {
            $table->string('transfer_return_pdf',50)->after('branch_id')->default('pdf1');
            $table->string('transfer_return_no_length',50)->after('branch_id')->default(4);
            $table->string('transfer_return_prefix',50)->after('branch_id')->default('TRF-RTN');
        });
    }
}
