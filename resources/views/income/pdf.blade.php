<!DOCTYPE html>
<html>
<head>
    <title>{{ $income->receipt_no }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pdf.css') }}">
</head>
<body>
    <htmlpageheader name="page-header">
        <table class="table-100 ">
            <tr>
                <td align="center" colspan="2"><h3> RECEIPT </h3></td>
            </tr>
            <tr>
                <td align="left" colspan="2"><b>{{ $org->org_name }}</b></td>
            </tr>
            <tr >
                <td align="left" colspan="2" style="border-bottom:1px solid black"><span >{{ $org->address }} , {{$org->email}}</span></td>
            </tr>
            <tr>
                <td >
                    
                </td>
            </tr>
            <tr >
                <td><b>Receipt No : </b> {{ $income->receipt_no }} </td>
                <td align="right"><b>Date : </b> {{ date('d-m-Y',strtotime($income->receipt_date)) }} </td>
            </tr>
            <tr>
                <td align="left" colspan="2"><b>Head Of Account :</b> {{ $income->account->account_name }}</td>
            </tr>
            <tr>
                <td align="left" colspan="2">Paid To : <b> {{ $income->Contact->contact_name }}</b> </td>
            </tr>
        </table>
    </htmlpageheader>
    
    @php
        $grand_total = round( $income->amount);
    @endphp
    <table style="margin-left: -6px !important;" class="table-100">
        <tr>
            <td colspan="3"></td>
        </tr>
        
        <tr>
            <td align="left" colspan="3">[Amount In Words] : &nbsp;&nbsp;RUPEES {{ convert_number_to_words(round($grand_total)) }} ONLY</td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td>Mode of Payment : &nbsp;&nbsp;{{$income->payment_mode}}</td>
            <td>Ref no.{{$income->reference_no}}</td>
            <td>Ref Date : {{date('d-m-Y',strtotime($income->reference_date))}}</td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3" align="left"><b>Rs : &nbsp;&nbsp;&nbsp;&nbsp;{{number_format($grand_total , 2)}}</b></td>
        </tr>
        <tr >
            <td class="table-50" colspan="3">
                <table class="table-100">
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            For {{ $org->org_name }}
                        </td>
                    </tr>   
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            Authorized Signature
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table class="table-100 th-font">
        @if(!empty($income->terms))
            
                <tr>
                    <td><b>Terms &amp; Conditions</b></td>
                </tr>
                <tr>
                    <td>
                        <pre>{{ $income->terms }}</pre>
                    </td>
                </tr>
            
        @endif
        <tr >
            <td style="border-bottom:1px solid black"></td>
        </tr>
    </table>
    <htmlpagefooter name="page-footer">
        <table class="table-100">
            {{-- <tr >
                <td style="border-bottom:1px solid black"></td>
            </tr> --}}
        </table>
    </htmlpagefooter>
</body>
</html>
@php
    function convert_number_to_words($no)
    {   
        $words = array('0'=> 'ZERO' ,'1'=> 'ONE' ,'2'=> 'TWO' ,'3' => 'THREE','4' => 'FOUR','5' => 'FIVE','6' => 'SIX','7' => 'SEVEN','8' => 'EIGHT','9' => 'NINE','10' => 'TEN','11' => 'ELEVEN','12' => 'TWELVE','13' => 'THIRTEEN','14' => 'FOURTEEN','15' => 'FIFTEEN','16' => 'SIXTEEN','17' => 'SEVENTEEN','18' => 'EIGHTEEN','19' => 'NINETEEN','20' => 'TWENTY','30' => 'THIRTY','40' => 'FOURTY','50' => 'FIFTY','60' => 'SIXTY','70' => 'SEVENTY','80' => 'EIGHTY','90' => 'NINTY','100' => 'HUNDRED','1000' => 'THOUSAND','100000' => 'LAKH','10000000' => 'CRORE');
        if($no == 0)
        {
            return '';
        }
        else 
        {
            $novalue='';
            $highno=$no;
            $remainno=0;
            $value=100;
            $value1=1000;       
            while($no>=100)    
            {
                if(($value <= $no) &&($no  < $value1))    
                {
                    $novalue=$words["$value"];
                    $highno = (int)($no/$value);
                    $remainno = $no % $value;
                    break;
                }
                $value= $value1;
                $value1 = $value * 100;
            }       
            if(array_key_exists("$highno",$words))
            {
                return $words["$highno"]." ".$novalue." ".convert_number_to_words($remainno);
            }
            else 
            {
                $unit=$highno%10;
                $ten =(int)($highno/10)*10;            
                return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".convert_number_to_words($remainno);
            }
        }
    }
@endphp 