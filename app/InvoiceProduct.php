<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class InvoiceProduct extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'invoice_id','reference','reference_id','product_id','product_type','product_code','hsn_code','category_id','product_name','description','product_unit','price_id','barcode','purchase_rate_exc','sales_rate_exc','purchase_rate_inc','sales_rate_inc','quantity','amount','discount','discount_type','discount_amount','tax_id','tax_amount','sub_total','created_by','updated_by'
    ];

    protected $primaryKey = 'invoice_product_id';

    protected $dates = ['deleted_at'];

    public function Tax()
    {
    	return $this->hasOne('App\Tax','tax_id','tax_id')->withTrashed();
    }
    public function Invoice()
    {
        return $this->belongsTo('App\Invoice','invoice_id','invoice_id')->with('Customer');
    }
    
    // public function CategoryInvoice()
    // {
    //     return $this->hasOne('App\InvoiceProduct','product_id','product_id')
    //     ->selectRaw('product_id,SUM(quantity) as quantity')//,SUM(sub_total) as total
    //         ->groupBy('product_id');
    // }
    // public function DeliveryProductsCheck()
    // {
    //     return $this->hasMany('App\DeliveryProduct','reference_id','invoice_product_id')->where('reference','App\InvoiceProduct');
    // }
    // public function ReceiptParticularsCheck()
    // {
    //     return $this->hasMany('App\ReceiptParticular','reference_id','invoice_id')->where('reference','App\Invoice');
    // }

    public function TotalAmount($tax_id, $invoice_id)
    {
        $amount = InvoiceProduct::where([['invoice_id',$invoice_id],['tax_id',$tax_id]])->sum('amount');
        return $amount;
    }
}
