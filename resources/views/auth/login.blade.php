<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>LOGIN | {{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('storage/favicon.ico') }}" rel="shortcut icon" type="image/png">
    <link rel="stylesheet" href="{{ asset('css/theme.min.css') }}" id="stylesheetLight">
    <style>body { display: none; }</style>
</head>
<body class="d-flex align-items-center bg-auth border-top border-top-2 border-primary">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-md-5 col-lg-6 col-xl-4 px-lg-6 my-5">
                <center><img src="{{ asset('storage/logo.png') }}" width="200px"></center>
                <br><br>
                <h1 class="display-4 text-center mb-3">Sign in</h1>
                <form method="post" action="{{ route('login') }}" autocomplete="off">
                    @csrf
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" placeholder="Enter your username" name="username" autofocus value="{{ old('username') }}" tabindex="1">
                        @if ($errors->has('username'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label>Password</label>
                            </div>
                            {{-- <div class="col-auto">
                                <a href="{{ route('password.request') }}" class="form-text small text-muted">
                                    Forgot password?
                                </a>
                            </div> --}}
                        </div> 
                        <div class="input-group input-group-merge">
                            <input type="password" class="form-control form-control-appended{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Enter your password" name="password" tabindex="2">
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <span data-feather="eye"></span>
                                </span>
                            </div>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <button method="submit" class="btn btn-lg btn-block btn-primary mb-3">
                        Sign in
                    </button>
                </form>
            </div>
            <div class="col-12 col-md-7 col-lg-6 col-xl-8 d-none d-lg-block">
                <div class="bg-cover vh-100 mt-n1 mr-n3" style="background-image: url(storage/auth.jpg);"></div>
            </div>
        </div>
    </div>
</body>
</html>