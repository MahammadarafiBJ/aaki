<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('account_id');
            $table->string('account_code',50);
            $table->string('account_name',50);
            $table->string('account_no',50)->nullable();
            $table->string('bank_name',50)->nullable();
            $table->string('branch_name',50)->nullable();
            $table->string('ifsc_code',50)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('accounts')->insert(
            [
                [
                    'account_code' => '001',
                    'account_name' => 'Petty Cash',
                    'created_by' => 'mdbstech',
                ]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
