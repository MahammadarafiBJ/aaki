<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('invoice_id');
            $table->string('fiscal_year',50);
            $table->string('invoice_type',50)->default('Invoice');
            $table->string('invoice_no',50);
            $table->date('invoice_date');
            $table->string('reference_no',50)->nullable();
            $table->date('reference_date')->nullable();
            $table->bigInteger('branch_id')->unsigned();
            $table->bigInteger('customer_id')->unsigned();
            $table->text('billing_address')->nullable();
            $table->text('shipping_address')->nullable();
            $table->bigInteger('source_id')->unsigned();
            $table->bigInteger('destination_id')->unsigned();
            $table->bigInteger('payment_term_id')->unsigned();
            $table->date('due_date');
            $table->double('sub_total',15,2);
            $table->double('discount_amount',15,2);
            $table->double('tax_amount',15,2);
            $table->double('total_amount',15,2);
            $table->double('round_off',15,2);
            $table->double('grand_total',15,2);
            $table->double('card_discount_amount',15,2)->default(0.00);
            $table->double('payable_amount',15,2);
            $table->bigInteger('term_id')->unsigned()->nullable();
            $table->longtext('terms')->nullable();
            $table->longtext('note')->nullable();
            $table->string('invoice_status',50)->default('Open');
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('branch_id')->references('branch_id')->on('branches');
            $table->foreign('customer_id')->references('contact_id')->on('contacts');
            $table->foreign('source_id')->references('place_id')->on('places');
            $table->foreign('destination_id')->references('place_id')->on('places');
            $table->foreign('term_id')->references('term_id')->on('terms');
            $table->foreign('payment_term_id')->references('payment_term_id')->on('payment_terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
