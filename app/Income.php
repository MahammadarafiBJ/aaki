<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Contact;

class Income extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','receipt_no','receipt_date','account_id','contact_id','payment_mode','reference_no','reference_date','amount','term_id','terms','note','created_by','updated_by'
    ];

    protected $primaryKey = 'income_id';

    protected $dates = ['deleted_at'];

     public function Contact()
    {
    	return $this->hasOne('App\Contact','contact_id','contact_id')->withTrashed();
    }

    public function Account()
    {
    	return $this->hasOne('App\Account','account_id','account_id')->withTrashed();
    }
    public function Term()
    {
        return $this->hasOne('App\Term','term_id','term_id')->withTrashed();
    }
}
