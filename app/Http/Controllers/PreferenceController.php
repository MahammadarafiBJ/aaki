<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Preference;

class PreferenceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_preference()
    {
        return Preference::first();
    }

    public function update(Request $request)
    {
        $data = $this->validate($request, [
            'bill_prefix'           => 'required|max:50',
            'bill_no_length'        => 'required|max:50',
           	'bill_pdf'              => 'required|max:50',
           	'transfer_prefix'       => 'required|max:50',
            'transfer_no_length'    => 'required|max:50',
            'transfer_pdf'          => 'required|max:50',
            'transfer_return_prefix'    => 'required|max:50',
            'transfer_return_no_length' => 'required|max:50',
            'transfer_return_pdf'       => 'required|max:50',
            'invoice_prefix'        => 'required|max:50',
            'invoice_no_length'     => 'required|max:50',
            'invoice_pdf'           => 'required|max:50',
            'receipt_prefix'        => 'required|max:50',
            'receipt_no_length'     => 'required|max:50',
            'receipt_pdf'           => 'required|max:50',
            'voucher_prefix'        => 'required|max:50',
            'voucher_no_length'     => 'required|max:50',
          	'voucher_pdf'           => 'required|max:50',
            'payment_prefix'        => 'required|max:50',
            'payment_no_length'     => 'required|max:50',
            'payment_pdf'           => 'required|max:50',
            'fiscal_year'           => 'required|max:50',
            'discount_on'           => 'required|max:50',
            'allow_negative_stock'  => 'required|max:50',
            'discount'              => 'required|max:50',
            'discount1'             => 'required|max:50',
            'card_amount'           => 'required|max:50',
            'card_discount'         => 'required|max:50',
            'printer_type'          => 'required|max:50',
            'printer_ip'            => 'required|max:50',
            'branch_id'             => 'required',
        ]);

        return Preference::where('preference_id',$request->preference_id)->update($data);
    }
    
}