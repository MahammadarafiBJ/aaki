<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Organization;
use App\Preference;
use App\Branch;
use App\User;

Use App\Contact;
Use App\Product;
Use App\Price;
Use App\Bill;
Use App\Transfer;
Use App\Invoice;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $org = Organization::first();
        $preference = Preference::first();
        $branch = Branch::where('branch_id',Auth::User()->branch_id)->first();

        return view('home',compact('org','branch','preference'));
    }

    public function get_dashboard_data(Request $request){

        $contacts = Contact::count();
        $products = Product::count();
        
        $date = date('Y-m-d',strtotime($request->date));

        $purchase_bill = Bill::whereDate('created_at',$date)->sum('grand_total');
        $today_sales = Invoice::whereDate('created_at',$date)->get();

        $callback = function($query) use($date){
                        $query->whereDate('created_at',$date);
                    };

        $branches = Branch::with(['Invoices' => $callback ,'Transfers' => $callback])->get();

        $day_sales = $today_sales->sum('grand_total');

        if (Auth::User()->user_role != 'Super Admin') {
            $branches = $branches->filter(function($query){
                return ($query->branch_id == Auth::User()->branch_id);
            })->values();

            $day_sales = $today_sales->where('branch_id',Auth::User()->branch_id)->sum('grand_total');
        }

        return compact('contacts','products','purchase_bill','branches','day_sales');

    }

    public function branch_view(Request $request)
    {
        $branch = User::where('user_id',Auth::User()->user_id)->update([
            'branch_id' => $request['branch_id']
        ]);
        return $branch;
    }
    
}
