<?php

namespace App\Imports;

use App\Bill;
use App\BillProduct;
use App\Product;
use App\Price;
use App\Category;
use App\Preference;
use App\Tax;
use App\PaymentTerm;
use App\Contact;

use Maatwebsite\Excel\Concerns\ToModel;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class BillsImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        $preference = Preference::first();
        $payment_term = PaymentTerm::first();
        $contact = Contact::first();

        $id = Bill::where('fiscal_year',$preference->fiscal_year)->max('bill_id');
        $id = $id + 1;
        $n = $preference->bill_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $bill_no = $preference->bill_prefix.'/'.$id.'/'.$preference->fiscal_year;

        $bill = Bill::create([
            'fiscal_year'       => $preference->fiscal_year,
            'bill_no'           => $bill_no,
            'bill_date'         => date("Y-m-d"),
            'due_date'          => date("Y-m-d"),
            'reference_date'    => date("Y-m-d"),
            'vendor_id'         => $contact->contact_id,
            'billing_address'   => $contact->billing_address,
            'shipping_address'  => $contact->shipping_address,
            'source_id'         => $contact->place_id,
            'payment_term_id'   => $payment_term->payment_term_id,
            'destination_id'    => $contact->place_id,
            'sub_total'         => 0,
            'discount_amount'   => 0,
            'tax_amount'        => 0,
            'total_amount'      => 0,
            'round_off'         => 0,
            'grand_total'       => 0,
            'bill_status'       => 'Open',
        ]);

        $total_sub_total = 0;
        $total_tax_amount= 0;
        $total_amount    = 0;

        foreach ($rows as $key => $row) {

            $category = Category::where('category_name',$row['category_name'])->first();

            if ($category==null) {
                $id = Category::withTrashed()->max('category_id');
                $category_code = $id + 1;
                $length = strlen($category_code);
                $n = 3 - $length;
                for ($i=0; $i < $n; $i++) 
                { 
                    $category_code = '0'.$category_code;
                }

                $category = Category::create([
                    'category_code' => $category_code,
                    'category_name' => $row['category_name'],
                ]);
            }

            $product = Product::where([['category_id',$category->category_id],['product_name',$row['product_name']]])->first();

            if ($product==null) {
                $id = Product::withTrashed()->max('product_id');
                $product_code = $id + 1;
                $length = strlen($product_code);
                $n = 4 - $length;
                for ($i=0; $i < $n; $i++) 
                { 
                    $product_code = '0'.$product_code;
                }
                $product = Product::create([
                    'product_type'  => 'Goods',
                    'product_code'  => $product_code,
                    'hsn_code'      => $row['hsn_code']==''?'NULL':$row['hsn_code'],
                    'category_id'   => $category->category_id,
                    'product_name'  => $row['product_name'],
                    'product_unit'  => 'No',
                    'product_size'  => 2,
                ]);
            }

            $price = Price::where([['product_id',$product->product_id],['barcode',$row['barcode']]])->first();

            $tax = Tax::where('tax_rate',$row['tax'])->first();

            if ($price==null) {
                $purchase_rate_inc = $row['purchase_rate_exc']+(($row['purchase_rate_exc'])*($tax->tax_rate/100)); 
                $sales_rate_inc = $row['sales_rate_exc']+(($row['sales_rate_exc'])*($tax->tax_rate/100)); 
                $price = Price::create([
                    'product_id'        => $product->product_id,
                    'barcode'           => $row['barcode'],
                    'purchase_rate_exc' => $row['purchase_rate_exc'],
                    'sales_rate_exc'    => $row['sales_rate_exc'],
                    'tax_id'            => $tax->tax_id,
                    'purchase_rate_inc' => $purchase_rate_inc,
                    'sales_rate_inc'    => $sales_rate_inc,
                ]);
            }

            $amount     =  $price->purchase_rate_exc*$row['quantity'];
            $tax_amount =  $amount*($tax->tax_rate/100);
            $sub_total  =  ($amount+$tax_amount);

            $bill_up = BillProduct::create([
                'bill_id'           => $bill->bill_id,
                'product_id'        => $product->product_id,
                'product_type'      => $product->product_type,
                'product_code'      => $product->product_code,
                'hsn_code'          => $product->hsn_code,
                'category_id'       => $product->category_id,
                'product_name'      => $product->product_name,
                'description'       => $product->description,
                'product_unit'      => $product->product_unit,
                'price_id'          => $price->price_id,
                'barcode'           => $price->barcode,
                'purchase_rate_exc' => $price->purchase_rate_exc,
                'sales_rate_exc'    => $price->sales_rate_exc,
                'purchase_rate_inc' => $price->purchase_rate_inc,
                'sales_rate_inc'    => $price->sales_rate_inc,
                'quantity'          => $row['quantity'],
                'amount'            => $amount,
                'discount'          => 0,
                'discount_type'     => '%',
                'discount_amount'   => 0,
                'tax_id'            => $tax->tax_id,
                'tax_amount'        => $tax_amount,
                'sub_total'         => $sub_total,
            ]);

            $total_sub_total    += $amount;
            $total_tax_amount   += $tax_amount;
            $total_amount       += $amount+$tax_amount;
        }

        $grand_total = round($total_amount,2);

        Bill::where('bill_id',$bill->bill_id)->update([
            'sub_total'         => $total_sub_total,
            'tax_amount'        => $total_tax_amount,
            'total_amount'      => $total_amount,
            'round_off'         => $grand_total-$total_amount,
            'grand_total'       => $grand_total,
        ]);
    }

}
