<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masters', function (Blueprint $table) {
            $table->bigIncrements('master_id');
            $table->string('master_name',50);
            $table->string('master_value',50);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('masters')->insert(
            [
                [
                    'master_name' => 'User Role',
                    'master_value' => 'Admin',
                    'created_by' => 'mdbstech',
                ],
                [
                    'master_name' => 'User Role',
                    'master_value' => 'User',
                    'created_by' => 'mdbstech',
                ],
                [
                    'master_name' => 'Payment Mode',
                    'master_value' => 'Cash',
                    'created_by' => 'mdbstech',
                ],
                [
                    'master_name' => 'Payment Mode',
                    'master_value' => 'Online',
                    'created_by' => 'mdbstech',
                ],
                [
                    'master_name' => 'Invoice Type',
                    'master_value' => 'Invoice',
                    'created_by' => 'mdbstech',
                ]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masters');
    }
}
