<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use Auth;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return Account::
        	where('account_code', 'like', '%'.$request->search.'%')
        	->orWhere('account_name', 'like', '%'.$request->search.'%')
        	->orWhere('account_no', 'like', '%'.$request->search.'%')
        	->orWhere('bank_name', 'like', '%'.$request->search.'%')
        	->orWhere('branch_name', 'like', '%'.$request->search.'%')
        	->orWhere('ifsc_code', 'like', '%'.$request->search.'%')
        	->paginate(5);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'account_code'  => 'required|max:50',
            'account_name'  => 'required|max:50',
            'account_no'    => 'max:50',
            'bank_name'     => 'max:50',
            'branch_name'   => 'max:50',
            'ifsc_code'     => 'max:50',
        ]);

        Account::create([
            'account_code'  => $request->account_code,
            'account_name'  => $request->account_name,
            'account_no'    => $request->account_no,
            'bank_name'     => $request->bank_name,
            'branch_name'   => $request->branch_name,
            'ifsc_code'     => $request->ifsc_code,
            'created_by'    => Auth::User()->username,         
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'account_code'  => 'required|max:50',
            'account_name'  => 'required|max:50',
            'account_no'    => 'max:50',
            'bank_name'     => 'max:50',
            'branch_name'   => 'max:50',
            'ifsc_code'     => 'max:50',
        ]);

        return Account::where('account_id',$request->account_id)->update([
            'account_code'  => $request->account_code,
            'account_name'  => $request->account_name,
            'account_no'    => $request->account_no,
            'bank_name'     => $request->bank_name,
            'branch_name'   => $request->branch_name,
            'ifsc_code'     => $request->ifsc_code,
            'updated_by'    => Auth::User()->username,      
        ]);
    }

    public function destroy(Account $account)
    {
        return Account::where('account_id',$account->account_id)->delete();
    }

    public function get_accounts()
    {
        return Account::get();
    }
}
