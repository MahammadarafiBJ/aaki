<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransferReturn extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $primaryKey = 'transfer_return_id';

    protected $dates = ['deleted_at'];

    public function Branch()
    {
        return $this->hasOne('App\Branch','branch_id','branch_id')->withTrashed();
    }
    
    public function TransferReturnProducts()
    {
        return $this->hasMany('App\TransferReturnProduct','transfer_return_id','transfer_return_id')->with('Tax');
    }

    public function SubTotal($transfer_return_id)
    {
     $sub_total = round(TransferReturnProduct::where('transfer_return_id',$transfer_return_id)->sum('amount'),2);
     return $sub_total;
    }

    public function Discount($transfer_return_id)
    {
        $discount = round(TransferReturnProduct::where('transfer_return_id',$transfer_return_id)->sum('discount_amount'),2);
        return $discount;
    }

    public function TotalTax($transfer_return_id)
    {
        $total_tax = round(TransferReturnProduct::where('transfer_return_id',$transfer_return_id)->sum('tax_amount'),2);
        return $total_tax;
    }

    public function Taxes($transfer_return_id)
    {
        return TransferReturnProduct::where('transfer_return_id',$transfer_return_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }

    public function TaxableValue($transfer_return_id,$tax_id)
    {
        $amount = round(TransferReturnProduct::where([['transfer_return_id',$transfer_return_id],['tax_id',$tax_id]])->sum('amount'),2);

        $discount = round(TransferReturnProduct::where([['transfer_return_id',$transfer_return_id],['tax_id',$tax_id]])->sum('discount_amount'),2);
         
        $preference = Preference::first();

        if ($preference->discount_on == 'Exc') {
            return $amount - $discount;
        }else{
            return $amount; 
        }
         
    }
}
