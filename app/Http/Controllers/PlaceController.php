<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Place;
use Auth;

class PlaceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return Place::
        	where('place_code', 'like', '%'.$request->search.'%')
        	->orWhere('place_name', 'like', '%'.$request->search.'%')
        	->paginate(5);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'place_code' => 'required|max:50|unique:places',
            'place_name' => 'required|max:50|unique:places',
        ]);

        return Place::create([
            'place_code' => $request->place_code,
            'place_name' => $request->place_name,
            'created_by' => Auth::User()->username,
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'place_code' => 'required|max:50',
            'place_name' => 'required|max:50',
        ]);

        return Place::where('place_id',$request->place_id)->update([
            'place_code' => $request->place_code,
            'place_name' => $request->place_name,
            'updated_by' => Auth::User()->username,
        ]);
    }

    public function destroy(Place $place)
    {
        Place::where('place_id',$place->place_id)->delete();
    }

    public function get_places()
    {
        return Place::get();
    }
}
