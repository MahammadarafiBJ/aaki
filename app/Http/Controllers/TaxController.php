<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tax;
use Auth;

class TaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return Tax::
        	where('tax_name', 'like', '%'.$request->search.'%')
        	->orWhere('tax_rate', 'like', '%'.$request->search.'%')
        	->orWhere('cgst_name', 'like', '%'.$request->search.'%')
        	->orWhere('cgst_rate', 'like', '%'.$request->search.'%')
        	->orWhere('sgst_name', 'like', '%'.$request->search.'%')
        	->orWhere('sgst_rate', 'like', '%'.$request->search.'%')
        	->orWhere('igst_name', 'like', '%'.$request->search.'%')
        	->orWhere('igst_rate', 'like', '%'.$request->search.'%')
        	->paginate(5);
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'tax_name' => 'required|max:50',
    		'tax_rate' => 'required|numeric',
    		'cgst_name' => 'required|max:50',
    		'cgst_rate' => 'required|numeric',
            'sgst_name' => 'required|max:50',
            'sgst_rate' => 'required|numeric',
            'igst_name' => 'required|max:50',
            'igst_rate' => 'required|numeric',
    	]);

    	Tax::create([
    		'tax_name' => $request->tax_name,
    		'tax_rate' => $request->tax_rate,
    		'cgst_name' => $request->cgst_name,
    		'cgst_rate' => $request->cgst_rate,
            'sgst_name' => $request->sgst_name,
            'sgst_rate' => $request->sgst_rate,
            'igst_name' => $request->igst_name,
            'igst_rate' => $request->igst_rate,
    		'created_by' => Auth::User()->username, 
    	]);
	}

	public function update(Request $request)
    {
        $this->validate($request, [
            'tax_name' => 'required|max:50',
            'tax_rate' => 'required|numeric',
            'cgst_name' => 'required|max:50',
            'cgst_rate' => 'required|numeric',
            'sgst_name' => 'required|max:50',
            'sgst_rate' => 'required|numeric',
            'igst_name' => 'required|max:50',
            'igst_rate' => 'required|numeric',
        ]);

        Tax::where('tax_id',$request->tax_id)->update([
            'tax_name' => $request->tax_name,
    		'tax_rate' => $request->tax_rate,
    		'cgst_name' => $request->cgst_name,
    		'cgst_rate' => $request->cgst_rate,
            'sgst_name' => $request->sgst_name,
            'sgst_rate' => $request->sgst_rate,
            'igst_name' => $request->igst_name,
            'igst_rate' => $request->igst_rate,
            'updated_by' => Auth::User()->username, 
        ]);
    }

    public function destroy(Tax $tax)
    {
        Tax::where('tax_id',$tax->tax_id)->delete();
    }

    public function get_taxes(Tax $tax)
    {
        return Tax::get();
    }
}
