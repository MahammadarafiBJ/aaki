<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bill extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','bill_no','bill_date','reference_no','reference_date','vendor_id','billing_address','shipping_address','source_id','destination_id','payment_term_id','due_date','sub_total','discount_amount','tax_amount','total_amount','round_off','grand_total','term_id','terms','note','bill_status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'bill_id';

    protected $dates = ['deleted_at'];

     public function Vendor()
    {
        return $this->hasOne('App\Contact','contact_id','vendor_id')->withTrashed();
    }

    public function BillProducts()
    {
        return $this->hasMany('App\BillProduct','bill_id','bill_id')->with('Tax');
    }

    public function SourcePlace()
    {
        return $this->hasOne('App\Place','place_id','source_id')->withTrashed();
    }
    public function DestinationPlace()
    {
        return $this->hasOne('App\Place','place_id','destination_id')->withTrashed();
    }
    public function SubTotal($bill_id)
    {
        $sub_total = round(BillProduct::where('bill_id',$bill_id)->sum('amount'),2);
        return $sub_total;
    }
    public function Discount($bill_id)
    {
        $discount = round(BillProduct::where('bill_id',$bill_id)->sum('discount_amount'),2);
        return $discount;
    }
    public function TotalTax($bill_id)
    {
        $total_tax = round(BillProduct::where('bill_id',$bill_id)->sum('tax_amount'),2);
        return $total_tax;
    }
    public function Taxes($bill_id)
    {
        return BillProduct::where('bill_id',$bill_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }
    public function TaxableValue($bill_id,$tax_id)
    {
        $amount =  round(BillProduct::where([['bill_id',$bill_id],['tax_id',$tax_id]])->sum('amount'),2);

        $discount = round(BillProduct::where([['bill_id',$bill_id],['tax_id',$tax_id]])->sum('discount_amount'),2);

        return round($amount-$discount);
    }
    // public function BillPaymentTotal(){
    //     return $this->hasOne('App\PaymentParticular','reference_id','bill_id')
    //                 ->selectRaw('reference_id,SUM(paid_amount) as paid_amount')
    //                 ->where('reference','App\Bill')
    //                 ->groupBy('reference_id');
    // }
    public function BillTax(){
        return $this->hasMany('App\BillProduct','bill_id','bill_id')
                    ->selectRaw('bill_id,tax_id,SUM(amount)-SUM(discount_amount) as amount,SUM(tax_amount)/2 as cgst_amount,SUM(tax_amount)/2 as sgst_amount,SUM(tax_amount) as igst_amount,SUM(discount_amount) as discount_amount,SUM(sub_total) as sub_total,SUM(quantity) as quantity')
                    ->with('Tax')
                    ->groupBy(['bill_id','tax_id']);
    }
    
}
