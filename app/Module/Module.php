<?php

namespace App\Module;

class Module 
{
	public function new_message($mobile_no, $text)
    {
        $postData = array(
            'authkey' => '176688AGU07NkJuVFt5e452174P1',
            'mobiles' => $mobile_no,
            'message' => $text,
            'sender' => 'AAKIMY',
            'route' => 4
        );

        $url="http://api.msg91.com/api/sendhttp.php";

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);

        if(curl_errno($ch))
        {
            return [
                'result' => 'Error',
                'data' => curl_error($ch),
            ];
        }
        else
        {
            return [
                'result' => 'Success',
                'data' => $output,
            ];
        }

        curl_close($ch);
    }
}