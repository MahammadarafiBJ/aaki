<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {
            $table->bigIncrements('transfer_id');
            $table->string('fiscal_year',50);
            $table->string('transfer_no',50);
            $table->date('transfer_date');
            $table->bigInteger('branch_id')->unsigned();
            $table->double('sub_total',15,2);
            $table->double('discount_amount',15,2);
            $table->double('tax_amount',15,2);
            $table->double('total_amount',15,2);
            $table->double('round_off',15,2);
            $table->double('grand_total',15,2);
            $table->bigInteger('term_id')->unsigned()->nullable();
            $table->longtext('terms')->nullable();
            $table->longtext('note')->nullable();
            $table->string('transfer_status',50)->default('Open');
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('branch_id')->references('branch_id')->on('branches');
            $table->foreign('term_id')->references('term_id')->on('terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transfers');
    }
}
