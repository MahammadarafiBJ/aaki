<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;
use Auth;

class BranchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        return Branch::
            where('branch_code', 'like', '%'.$request->search.'%')
            ->orWhere('branch_name', 'like', '%'.$request->search.'%')
            ->orWhere('contact_person', 'like', '%'.$request->search.'%')
            ->orWhere('email', 'like', '%'.$request->search.'%')
            ->orWhere('mobile_no', 'like', '%'.$request->search.'%')
            ->orWhere('phone_no', 'like', '%'.$request->search.'%')
            ->orWhere('address', 'like', '%'.$request->search.'%')
            ->paginate(10);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'branch_code'       => 'required|max:50',
            'branch_name'       => 'required|max:50',
            'contact_person'    => 'max:50',
            'email'             => 'sometimes|nullable|email|max:50',
            'mobile_no'         => 'sometimes|nullable|regex:/[0-9]{10}/|digits:10|numeric',
            'phone_no'          => 'sometimes|nullable|regex:/[0-9]{10}/|digits:11|numeric',
            'address'           => 'max:2550',
        ]);

        Branch::create([
            'branch_code'       => $request->branch_code,
            'branch_name'       => $request->branch_name,
            'contact_person'    => $request->contact_person,
            'email'             => $request->email,
            'mobile_no'         => $request->mobile_no,
            'phone_no'          => $request->phone_no,
            'address'           => $request->address,
            'created_by'        => Auth::User()->username,
        ]);
    }

    public function edit(Branch $branch)
    {
        return $branch;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'branch_code'       => 'required|max:50',
            'branch_name'       => 'required|max:50',
            'contact_person'    => 'max:50',
            'email'             => 'sometimes|nullable|email|max:50',
            'mobile_no'         => 'sometimes|nullable|regex:/[0-9]{10}/|digits:10|numeric',
            'phone_no'          => 'sometimes|nullable|regex:/[0-9]{10}/|digits:11|numeric',
            'address'           => 'max:2550',
        ]);

        Branch::where('branch_id',$request->branch_id)->update([
            'branch_code'       => $request->branch_code,
            'branch_name'       => $request->branch_name,
            'contact_person'    => $request->contact_person,
            'email'             => $request->email,
            'mobile_no'         => $request->mobile_no,
            'phone_no'          => $request->phone_no,
            'address'           => $request->address,
            'updated_by'        => Auth::User()->username,
        ]);
    }

    public function destroy(Branch $branch)
    {
        Branch::where('branch_id',$branch->branch_id)->delete();
    }
    public function get_branches()
    {
        return Branch::get();
    }
}
