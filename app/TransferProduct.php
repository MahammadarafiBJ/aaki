<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class TransferProduct extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'transfer_id','product_id','product_type','product_code','hsn_code','category_id','product_name','description','product_unit','price_id','barcode','purchase_rate_exc','sales_rate_exc','purchase_rate_inc','sales_rate_inc','quantity','amount','discount','discount_type','discount_amount','tax_id','tax_amount','sub_total','transfer_product_status','created_by','updated_by'
    ];

    protected $primaryKey = 'transfer_product_id';

    protected $dates = ['deleted_at'];

    public function Tax()
    {
    	return $this->hasOne('App\Tax','tax_id','tax_id')->withTrashed();
    }
    public function Transfer()
    {
        return $this->belongsTo('App\Transfer','transfer_id','transfer_id')->with('Branch');
    }
}
