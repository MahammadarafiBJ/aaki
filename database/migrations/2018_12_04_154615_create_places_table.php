<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->bigIncrements('place_id');
            $table->string('place_code',50);
            $table->string('place_name',50);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('places')->insert(
            [
                [
                    'place_code' => '29',
                    'place_name' => 'KARNATAKA',
                    'created_by' => 'mdbstech',
                ],
                [
                    'place_code' => '01',
                    'place_name' => 'JAMMU AND KASHMIR',
                    'created_by' => 'mdbstech',
                ],

                [
                    'place_code' => '02',
                    'place_name' => 'HIMACHAL PRADESH',
                    'created_by' => 'mdbstech',
                ],

                [   
                    'place_code' => '03',
                    'place_name' => 'PUNJAB',
                    'created_by' => 'mdbstech',
                ],
                
                [   'place_code' => '04',
                    'place_name' => 'CHANDIGARH',
                     'created_by' => 'mdbstech',
                ],
                
                [   'place_code' => '05',
                    'place_name' => 'UTTARAKHAND',
                     'created_by' => 'mdbstech',
                ],
                
                [   'place_code' => '06',
                    'place_name' => 'HARYANA',
                     'created_by' => 'mdbstech',
                ],
                
                [   'place_code' => '07',
                    'place_name' => 'DELHI',
                    'created_by' => 'mdbstech',
                ],
                
                [   'place_code' => '08',
                    'place_name' => 'RAJASTHAN',
                     'created_by' => 'mdbstech',
                ],

                [   'place_code' => '09',
                    'place_name' => 'UTTAR  PRADESH',
                    'created_by' => 'mdbstech',
                ],

                [   'place_code' => '10',
                    'place_name' => 'BIHAR',
                    'created_by' => 'mdbstech',
                ],

                [  'place_code' => '11',
                    'place_name' => 'SIKKIM',
                     'created_by' => 'mdbstech',
                ],

                [   'place_code' => '12',
                    'place_name' => 'ARUNACHAL PRADESH',
                    'created_by' => 'mdbstech',
                ],

                [   'place_code' => '13',
                    'place_name' => 'NAGALAND',
                    'created_by' => 'mdbstech',
                ],

                [ 'place_code' => '14',
                    'place_name' => 'MANIPUR',
                    'created_by' => 'mdbstech',
                ],

                [ 'place_code' => '15',
                    'place_name' => 'MIZORAM',
                    'created_by' => 'mdbstech',
                ],

                [ 
                    'place_code' => '16',
                    'place_name' => 'TRIPURA',
                    'created_by' => 'mdbstech',
                ],

                [ 
                    'place_code' => '17',
                    'place_name' => 'MEGHLAYA',
                    'created_by' => 'mdbstech',
                ],

                [  
                    'place_code' => '18',
                    'place_name' => 'ASSAM',
                     'created_by' => 'mdbstech',
                ],

                [   'place_code' => '19',
                    'place_name' => 'WEST BENGAL',
                     'created_by' => 'mdbstech',
                ],

                [   
                    'place_code' => '20',
                    'place_name' => 'JHARKHAND',
                    'created_by' => 'mdbstech',
                ],

                [   

                    'place_code' => '21',
                    'place_name' => 'ODISHA',
                     'created_by' => 'mdbstech',
                ],

                [    'place_code' => '22',
                    'place_name' => 'CHATTISGARH',
                     'created_by' => 'mdbstech',
                ],
                [
                    'place_code' => '23',
                    'place_name' => 'MADHYA PRADESH',
                     'created_by' => 'mdbstech',
                ],

                [    'place_code' => '24',
                    'place_name' => 'GUJARAT',
                     'created_by' => 'mdbstech',
                ],

                [    'place_code' => '25',
                    'place_name' => 'DAMAN AND DIU',
                     'created_by' => 'mdbstech',
                ],

                [    'place_code' => '26',
                    'place_name' => 'DADRA AND NAGAR HAVELI',
                     'created_by' => 'mdbstech',
                ],

                [    'place_code' => '27',
                    'place_name' => 'MAHARASHTRA',
                     'created_by' => 'mdbstech',
                ],

                [    'place_code' => '28',
                    'place_name' => 'ANDHRA PRADESH(BEFORE DIVISION)',
                     'created_by' => 'mdbstech',
                ],

                [    'place_code' => '30',
                    'place_name' => 'GOA',
                     'created_by' => 'mdbstech',
                ],

                [    
                    'place_code' => '31',
                    'place_name' => 'LAKSHWADEEP',
                     'created_by' => 'mdbstech',
                ],
                [    'place_code' => '32',
                    'place_name' => 'KERALA',
                     'created_by' => 'mdbstech',
                ],
                [    'place_code' => '33',
                    'place_name' => 'TAMIL NADU',
                     'created_by' => 'mdbstech',
                ],
                [    'place_code' => '34',
                    'place_name' => 'PUDUCHERRY',
                     'created_by' => 'mdbstech',
                ],
                [    'place_code' => '35',
                    'place_name' => 'ANDAMAN AND NICOBAR ISLANDS',
                     'created_by' => 'mdbstech',
                ],
                [

                    'place_code' => '36',
                    'place_name' => 'TELANGANA',
                    'created_by' => 'mdbstech',
                ],

                [   
                    'place_code' => '37',
                    'place_name' => 'ANDHRA PRADESH (NEW)',
                    'created_by' => 'mdbstech',
                ],
                   
                
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
