<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
     use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'branch_id', 'name', 'username', 'email', 'password', 'user_role', 'mobile_no', 'address', 'avatar','player_id','push_token', 'created_by', 'updated_by'
    ];
    
    protected $primaryKey = 'user_id';

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Branch()
    {
        return $this->hasOne('App\Branch','branch_id','branch_id')->withTrashed();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    public function getJWTCustomClaims()
    {
        return [];
    }
}
