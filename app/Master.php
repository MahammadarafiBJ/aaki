<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Master extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'master_name', 'master_value', 'created_by', 'updated_by',
    ];

    protected $primaryKey = 'master_id';

    protected $dates = ['deleted_at'];
}
