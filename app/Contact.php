<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'contact_type','contact_code','contact_name','email','mobile_no','phone_no','pan_no','gstin_no','billing_address','shipping_address','account_no','bank_name','branch_name','ifsc_code','payment_term_id','place_id','date_of_birth','wedding_anniversary','card_no','created_by', 'updated_by',
    ];

    protected $primaryKey = 'contact_id';

    protected $dates = ['deleted_at'];

    public function Customer()
    {
        return $this->hasMany('App\Contact','contact_id','customer_id')->withThrased();
    }
    public function Vendor()
    {
        return $this->hasMany('App\Contact','contact_id','vendor_id')->withThrased();
    }
    
    //vendor
    // public function VendorPurchaseOrders()
    // {
    //     return $this->hasMany('App\PurchaseOrder','vendor_id','contact_id');
    // }
    // public function VendorBills()
    // {
    //     return $this->hasMany('App\Bill','vendor_id','contact_id');
    // }
    // public function VendorPayments()
    // {
    //     return $this->hasMany('App\Payment','vendor_id','contact_id')->with('Account');
    // }
    // //customer
    // public function CustomerQuotations()
    // {
    //     return $this->hasMany('App\Quotation','customer_id','contact_id');
    // }

    // public function CustomerProformas()
    // {
    //     return $this->hasMany('App\Proforma','customer_id','contact_id');
    // }
    // public function CustomerDeliverys()
    // {
    //     return $this->hasMany('App\Delivery','customer_id','contact_id');
    // }
    // public function CustomerInvoices()
    // {
    //     return $this->hasMany('App\Invoice','customer_id','contact_id');
    // }
    // public function CustomerReceipts()
    // {
    //     return $this->hasMany('App\Receipt','customer_id','contact_id')->with('Account');
    // }
    // public function CustomerInvoiceReceipt()
    // {
    //     return $this->hasMany('App\Invoice','customer_id','contact_id')->with('InvoiceReceiptTotal');
    // }
    // public function VendorBillPayment()
    // {
    //     return $this->hasMany('App\Bill','vendor_id','contact_id')->with('BillPaymentTotal');
    // }
}
