<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransferReturnProduct extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $primaryKey = 'transfer_return_product_id';

    protected $dates = ['deleted_at'];

    public function Tax()
    {
    	return $this->hasOne('App\Tax','tax_id','tax_id')->withTrashed();
    }
    
    public function TransferReturn()
    {
        return $this->belongsTo('App\TransferReturn','transfer_return_id','transfer_return_id')->with('Branch');
    }
}
