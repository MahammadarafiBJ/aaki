<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->bigIncrements('price_id');
            $table->bigInteger('product_id')->unsigned();
            $table->longText('barcode');
            $table->double('purchase_rate_exc',15,2)->default(0);
            $table->double('sales_rate_exc',15,2)->default(0);
            $table->bigInteger('tax_id')->unsigned();
            $table->double('purchase_rate_inc',15,2)->default(0);
            $table->double('sales_rate_inc',15,2)->default(0);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('product_id')->references('product_id')->on('products');
            $table->foreign('tax_id')->references('tax_id')->on('taxes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
