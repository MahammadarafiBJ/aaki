<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = [
        'org_code', 'org_name', 'email', 'mobile_no',  'phone_no', 'website', 'address', 'pan_no', 'gstin_no', 'logo', 'created_by', 'updated_by',
    ];
    protected $primaryKey = 'org_id';
}
