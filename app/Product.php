<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_type', 'product_code', 'hsn_code', 'category_id', 'product_name', 'description', 'product_unit','product_size', 'min_stock','discount','created_by', 'updated_by',
    ];

    protected $primaryKey = 'product_id';
    protected $dates = ['deleted_at'];

    public function Category()
    {
    	return $this->hasOne('App\Category','category_id','category_id')->withTrashed();
    }

    public function Prices()
    {
        return $this->hasMany('App\Price','product_id','product_id')->with('Tax');
    }
    public function StockBillProducts()
    {
        return $this->hasMany('App\BillProduct','product_id','product_id')->with('Bill');
    }
    public function StockTransferProducts()
    {
        return $this->hasMany('App\TransferProduct','product_id','product_id')->with('Transfer');
    }
    public function StockInvoiceProducts()
    {
        return $this->hasMany('App\InvoiceProduct','product_id','product_id')->with('Invoice');
    }
    // public function BillStock()
    // {
    //     return $this->hasOne('App\BillProduct','product_id','product_id')
    //                 ->selectRaw('product_id,SUM(quantity) as quantity')
    //                 ->groupBy('product_id');
    // }
    // public function TransferStock()
    // {
    //     return $this->hasOne('App\TransferProduct','product_id','product_id')
    //                 ->selectRaw('product_id,SUM(quantity) as quantity')
    //                 ->groupBy('product_id');
    // }
    // public function InvoiceStock()
    // {
    //     return $this->hasOne('App\InvoiceProduct','product_id','product_id')
    //                 ->selectRaw('product_id,SUM(quantity) as quantity')
    //                 ->groupBy('product_id');
    // }
    // public function CurrentStock($product_id)
    // {
    //     $preference = Preference::first();
    //     $product = Product::where('product_id',$product_id)->first();
    //     $purc_qty = 0;$sale_qty = 0; $transfer_qty = 0;

    //     $product->load(['BillStock']);
    //     $purc_qty = $product->CalcStock($product->BillStock);

    //     $product->load(['TransferStock']);
    //     $transfer_qty = $product->CalcStock($product->TransferStock);

    //     $product->load(['InvoiceStock']);
    //     $sale_qty = $product->CalcStock($product->InvoiceStock);

    //     return [
    //         'product_id'               => $product_id,
    //         'min_stock'                => $product->min_stock,
    //         'opening_stock'            => $transfer_qty-$sale_qty,
    //         //'opening_stock'            => $purc_qty-$sale_qty,
    //         'purchase_stock'           => $purc_qty,
    //         'transfer_stock'           => $transfer_qty,
    //         'sales_stock'              => $sale_qty,
    //     ];
    // }
    // public function CalcStock($stock,$stock_return)
    // {
    //     $qty = 0;
    //     if(!is_null($stock)){
    //         $qty = $stock->quantity;
    //     }
    //     return $qty;
    // }
}