<!DOCTYPE html>
<html>
<head>
	<title>PDF</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/pdf.css') }}">
	<style type="text/css">
		td{
			line-height: 10px;
		}
		hr {
			line-height: 8px;
		}
	</style>
</head>
<body>
    <table class="table-100">
        <tr>
            <td align="center"><h2>{{ $org->org_name }}</h2></td>
        </tr>
        <tr>
            <td align="center"><p>{{ $branch->address }}</p></td>
        </tr>
        <tr>
            <td align="center">Phone No  : {{ $branch->mobile_no }} </td>
            <hr>
        </tr>
        
        <tr>
            <td align="center">
            	<span>TAX INVOICE</span>
            </td>
        </tr>
    </table>
    <hr>
    <table class="table-100">
        <tr>
            <td>
                <b>Billing Location : </b>
                {{ $branch->branch_name }}
            </td>
        </tr>
        <tr>
            <td>
                <b>GSTIN : </b> 
                {{ $org->gstin_no }}
            </td>
            <hr>
        </tr>
    </table>
    <table class="table-100">
        <tr>
            <td>
                <b>Name : </b>
                {{ $invoice->Customer->contact_name }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Mobile No : </b> 
                {{ $invoice->Customer->mobile_no }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Card No : </b> 
                {{ $invoice->Customer->card_no }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Billed By : </b> 
                {{ $invoice->created_by }}
            </td>
        </tr>
    </table>
    {{-- <table class="table-100">
        <tr>
            <td>
            	<b>Invoice No : </b>
            	{{ $invoice->invoice_no }}
            </td>
        </tr>
       	<tr>
            <td>
            	<b>Invoice Date:</b> 
            	{{ date('d-m-Y',strtotime($invoice->invoice_date)) }}
            </td>
        </tr>
        <tr>
            <td>
            	<b>Name :</b> 
            	{{ $invoice->Customer->contact_name }}
            </td>
        </tr>
    </table> --}}

    <table class="table-border table-100 th-font" border="1">
        <tr>
            <td align="center"><b>#</b></td>
            <td align="center"><b>Name</b></td>
            <td align="center"><b>RATE</b></td>
            <td align="center"><b>QTY</b></td>
            <td align="center"><b>AMOUNT</b></td>
        </tr>
        @foreach($invoice->InvoiceProducts as $key=>$invoice_product)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $invoice_product->product_name }}</td>
                <td align="right">{{ number_format($invoice_product->sales_rate_exc,2) }}</td>
                <td align="right">{{ number_format($invoice_product->quantity,2) }}</td>
                <td align="right">{{ number_format($invoice_product->amount,2) }}</td>
            </tr>
        @endforeach
        @php
            $total_amount = $invoice->SubTotal($invoice->invoice_id);
            $discount = $invoice->Discount($invoice->invoice_id);
            $sub_total = $total_amount - $discount;
            $total_tax = $invoice->TotalTax($invoice->invoice_id);
            $total = $sub_total + $total_tax;
            $grand_total = round($total);
            $round_off = $grand_total - $total;
        @endphp
        <tr>
            <td colspan="4" align="right">TOTAL</td>
            <td align="right"><b>{{ number_format($total_amount,2) }}</b></td>
        </tr>

        @foreach($distinct_taxes as $tax)
            @php
                $tax_amount = $tax->TotalAmount($tax->tax_id,$invoice->invoice_id);
                $cgst = round(($tax_amount * $tax->Tax->cgst_rate)/100,2);
                $sgst = round(($tax_amount * $tax->Tax->sgst_rate)/100,2);
            @endphp
            @if($cgst != 0 || $sgst != 0)
                <tr>
                    <td colspan="4" align="right">SGST ( {{ $tax->Tax->cgst_rate }} %)</td>
                    <td align="right"><b>{{ number_format($sgst,2) }}</b></td>
                </tr>
                <tr>
                    <td colspan="4" align="right">CGST ({{ $tax->Tax->cgst_rate }} %)</td>
                    <td align="right"><b>{{ number_format($cgst,2) }}</b></td>
                </tr>
            @endif
        @endforeach
        <!-- <tr>
            <td colspan="4" align="right">GST</td>
            <td align="right"><b>{{ number_format($total_tax,2) }}</b></td>
        </tr> -->
        @if(!$discount==0)
        <tr>
            <td colspan="4" align="right">DISCOUNT</td>
            <td align="right"><b>{{ number_format($discount,2) }}</b></td>
        </tr>
        @endif
        <tr>
            <td colspan="4" align="right">ROUND OFF</td>
            <td align="right"><b>{{ number_format($round_off,2) }}</b></td>
        </tr>
        <tr>
            <td colspan="4" align="right">GRAND TOTAL</td>
            <td align="right"><b>{{ number_format($grand_total,2) }}</b></td>
        </tr>
        @if($invoice->card_discount_amount)
        <tr>
            <td colspan="4" align="right">CARD DISCOUNT</td>
            <td align="right"><b>{{ number_format($invoice->card_discount_amount,2) }}</b></td>
        </tr>
        @endif
        <tr>
            <td colspan="4" align="right">PAID AMOUNT</td>
            <td align="right"><b>{{ number_format($invoice->payable_amount,2) }}</b></td>
        </tr>
    </table>
    <hr>
    <table class="table-100">
        <tr>
            <td>
                
                <b>Invoice No : </b>
                {{ $invoice->invoice_no }}
            </td>
        </tr>
        <tr>
            <td>
                <b>Invoice Date:</b> 
                {{ date('d-m-Y',strtotime($invoice->invoice_date)) }}
            </td>
        </tr>
        <hr>
    </table>
    <b>TERMS & CONDITIONS </b>
    <ul>
        <li class="li"> Merchandise can be exchanged within 10 days from the date of purchase provided the merchandise is unsused,intact and in saleable condition,along with original product tag and invoice as proof of purchase.</li><br>

        <li> Exchange of merchandise bought during SALE peroid will be permitted during the same SALE period only.</li><br>

        <li> Only Credit Notes will be issued on exchange and no cash refund will be given.Credit Notes will be issued equivalent to the invoiced value of the product exchanged.</li><br>

        <li> 'Landmark Rewards' points will be awarded only if the registered mobile number or membership number is quoted at the time of billing.</li><br>

        <li> Aaki shall not be responsible for any loss/damage or personal injury to a customer directly/indirectly by use of the products/merchandise purchased from the Lifestyle stores.</li><br>

        <li> Aaki reserves the right to alter/modify any terms and conditions at any point of time without assigning any reason or intimation whatsoever.
        
    </ul>
               
  
    <!-- <table style="margin-top:10px; margin-bottom: 30px; " class="table-100 th-font" border="1">
        <tr>
            <td align="center" rowspan="2"><b>Total</b></td>
            @if($invoice->source_id==$invoice->destination_id)
            <td align="center" colspan="2"><b>CGST</b></td>
            <td align="center" colspan="2"><b>SGST</b></td>
            @else
            <td align="center" colspan="2"><b>IGST</b></td>
            @endif
            <td align="center" rowspan="2"><b>GST</b></td>
            <td align="center" rowspan="2"><b>Total</b></td>
        </tr>
        <tr>
            @if($invoice->source_id==$invoice->destination_id)
            <td align="center"><b>Rate</b></td>
            <td align="center"><b>Amount</b></td>
            <td align="center"><b>Rate</b></td>
            <td align="center"><b>Amount</b></td>
            @else
            <td align="center"><b>Rate</b></td>
            <td align="center"><b>Amount</b></td>
            @endif
        </tr>
        @php
            $final_taxable_value = 0;
            $final_gst = 0;
            $final_cgst = 0;
            $final_sgst = 0;
            $final_igst = 0;
            $final_grand_total = 0;
        @endphp
        @foreach($invoice->Taxes($invoice->invoice_id) as $tax)
            @php
                $taxable_value = $invoice->TaxableValue($invoice->invoice_id,$tax->tax_id);
                $cgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);
                $sgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);
                $igst = round(($taxable_value * $tax->Tax->igst_rate)/100,2);
                if($invoice->source_id==$invoice->destination_id)
                {
                    $gst = $cgst + $sgst;
                }
                else
                {
                    $gst = $igst;
                }
                $grand_total = $gst + $taxable_value;
                $final_taxable_value += $taxable_value;
                $final_cgst +=$cgst;
                $final_sgst +=$sgst;
                $final_igst +=$igst;
                $final_gst +=$gst;
                $final_grand_total +=$grand_total;
            @endphp
            <tr>
                <td align="right">{{ number_format($taxable_value,2) }}</td>
                @if($invoice->source_id==$invoice->destination_id)
                    <td align="right">{{ $tax->Tax->cgst_name }}</td>
                    <td align="right">{{ number_format($cgst,2) }}</td>
                    <td align="right">{{ $tax->Tax->sgst_name }}</td>
                    <td align="right">{{ number_format($sgst,2) }}</td>
                @else
                    <td align="right">{{ $tax->Tax->igst_name }}</td>
                    <td align="right">{{ number_format($igst,2) }}</td>
                @endif
                <td align="right">{{ number_format($gst,2) }}</td>
                <td align="right">{{ number_format($grand_total,2) }}</td>
            </tr>
        @endforeach
        <tr>
            <td align="right"><b>{{ number_format($final_taxable_value,2) }}</b></td>
            @if($invoice->source_id==$invoice->destination_id)
                <td></td>
                <td align="right"><b>{{ number_format($final_cgst,2) }}</b></td>
                <td></td>
                <td align="right"><b>{{ number_format($final_sgst,2) }}</b></td>
            @else
                <td></td>
                <td align="right"><b>{{ number_format($final_igst,2) }}</b></td>
            @endif
            <td align="right"><b>{{ number_format($final_gst,2) }}</b></td>
            <td align="right"><b>{{ number_format($final_grand_total,2) }}</b></td>
        </tr>       
    </table> -->

    <table class="table-100">
        <tr>
            <td align="center"><b>Thank you visit again</b></td>
        </tr>
    </table>
</body>
</html>