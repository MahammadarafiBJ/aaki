<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->bigIncrements('branch_id');
            $table->string('branch_code',50);
            $table->string('branch_name',50);
            $table->string('contact_person',50)->nullable();
            $table->string('email',50)->nullable();
            $table->string('mobile_no',50)->nullable();
            $table->string('phone_no',50)->nullable();
            $table->text('address')->nullable();
            $table->string('logo',255)->default('logo.png');
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('branches')->insert(
            [
                [
                    'branch_code' => '001',
                    'branch_name' => 'Mysore',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
