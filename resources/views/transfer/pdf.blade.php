<!DOCTYPE html>
<html>
<head>
    <title>{{ $transfer->transfer_no }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pdf.css') }}">
</head>
<body>
    <htmlpageheader name="page-header">
        <table class="table-100 table-border">
            <tr>
                <td align="center" colspan="2"><h3>TRANSFER</h3></td>
            </tr>
            <tr>
                <td><b>Phone No:</b> {{ $org->phone_no }} </td>
                <td align="right"><b>Mobile No :</b> {{ $org->mobile_no }} </td>
            </tr>
            <tr>
                <td><b>PAN NO : {{ $org->pan_no }}</b></td>
                <td align="right"><b>GSTIN : {{ $org->gstin_no }}</b></td>
            </tr>
            <tr>
                <td align="center" colspan="2"><h2>{{ $org->org_name }}</h2></td>
            </tr>
            <tr>
                <td align="center" colspan="2"><span>{{ $org->address }}</span></td>
            </tr>
        </table>
    </htmlpageheader>
    <table class="table-100" border="1">
        <tr>
            <td rowspan="3">
                <span><b>Branch</b></span>
                <br/><span><b>{{ $transfer->Branch->branch_name }}</b></span>
                
            </td>
            <td><b>Transfer No : </b><br>{{ $transfer->transfer_no }}</td>
            <td><b>Transfer Date:</b> <br>{{ date('d-m-Y',strtotime($transfer->transfer_date)) }}</td>
        </tr>
        
    </table>
    <table class="table-border table-100 th-font" border="1">
        <tr>
            <td align="center"><b>SL.#</b></td>
            <td align="center"><b>PRODUCT CODE</b></td>
            <td align="center"><b>PRODUCT DESCRIPTION</b></td>
            <td align="center"><b>HSN/SAC</b></td>
            <td align="center"><b>QTY</b></td>
            <td align="center"><b>UNIT</b></td>
            <td align="center"><b>RATE</b></td>
            <td align="center"><b>TAX</b></td>
            <td align="center"><b>AMOUNT</b></td>
        </tr>
         @php 
            $i = 1;
            $j=0; 
        @endphp
        @foreach($transfer->TransferProducts as $trans)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $trans->product_code }}</td>
                <td>{{ $trans->product_name }}</td>
                <td align="center">{{ $trans->hsn_code }}</td>
                <td align="center">{{ number_format($trans->quantity,2) }}</td>
                <td align="center">{{ $trans->product_unit }}</td>
                <td align="right">{{ number_format($trans->sales_rate_exc,2) }}</td>
                <td align="right">{{ $trans->Tax->tax_name }}</td>
                <td align="right">{{ number_format($trans->amount,2) }}</td>
            </tr>
        @endforeach
        @for($j=$i;$j<=15;$j++)
            <tr>
                <td><br></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endfor
        @php
            $total_amount = $transfer->SubTotal($transfer->transfer_id);
            $discount = $transfer->Discount($transfer->transfer_id);
            $sub_total = $total_amount - $discount;
            $total_tax = $transfer->TotalTax($transfer->transfer_id);
            $total = $sub_total + $total_tax;
            $grand_total = round($total);
            $round_off = $grand_total - $total;
        @endphp
        <tr>
            <td colspan="7" align="right">TOTAL</td>
            <td colspan="3" align="right"><b>{{ number_format($total_amount,2) }}</b></td>
        </tr>
        <tr>
            <td colspan="7" align="right">GST</td>
            <td colspan="3" align="right"><b>{{ number_format($total_tax,2) }}</b></td>
        </tr>
        @if(!$discount==0)
        <tr>
            <td colspan="7" align="right">DISCOUNT</td>
            <td colspan="3" align="right"><b>{{ number_format($discount,2) }}</b></td>
        </tr>
        @endif
        <tr>
            <td colspan="7" align="right">ROUND OFF</td>
            <td colspan="3" align="right"><b>{{ number_format($round_off,2) }}</b></td>
        </tr>
        <tr>
            <td colspan="7" align="right">GRAND TOTAL</td>
            <td colspan="3" align="right"><b>{{ number_format($grand_total,2) }}</b></td>
        </tr>
        <tr>
            <td colspan="12">
                <span>TOTAL TRANSFER VALUE (IN WORDS)</span><br>
                <span><b>RUPEES {{ convert_number_to_words(round($grand_total)) }} ONLY</b></span>
            </td>
        </tr>
    </table>
    <table style="margin-left: -6px !important;" class="table-100">
        <tr >
            <td class="width-50">
                <table class="table-100 th-font" border="1">
                    <tr>
                        <td rowspan="2"><b>Total</b></td>
                        <td colspan="2"><b>CGST</b></td>
                        <td colspan="2"><b>SGST</b></td>
                        <td rowspan="2"><b>GST</b></td>
                        <td rowspan="2"><b>Grand Total</b></td>
                    </tr>
                    <tr>
                        <td><b>Rate</b></td>
                        <td><b>Amount</b></td>
                        <td><b>Rate</b></td>
                        <td><b>Amount</b></td>
                    </tr>
                    @php
                        $final_taxable_value = 0;
                        $final_gst = 0;
                        $final_cgst = 0;
                        $final_sgst = 0;
                        $final_igst = 0;
                        $final_grand_total = 0;
                    @endphp
                    @foreach($transfer->Taxes($transfer->transfer_id) as $tax)
                        @php
                            $taxable_value = $transfer->TaxableValue($transfer->transfer_id,$tax->tax_id);
                            if($preference->discount_on == 'Exc'){
                                $cgst = round($transfer->tax_amount/2,2);
                                $sgst = round($transfer->tax_amount/2,2);
                            }else{
                                $cgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);
                                $sgst = round(($taxable_value * $tax->Tax->sgst_rate)/100,2);
                            }
                            $igst = round(($taxable_value * $tax->Tax->igst_rate)/100,2);
                            $gst = $cgst + $sgst;
                            $grand_total = $gst + $taxable_value;
                            $final_taxable_value += $taxable_value;
                            $final_cgst +=$cgst;
                            $final_sgst +=$sgst;
                            $final_igst +=$igst;
                            $final_gst +=$gst;
                            $final_grand_total +=$grand_total;
                        @endphp
                        <tr>
                            <td align="right">{{ number_format($taxable_value,2) }}</td>
                            <td align="right">{{ $tax->Tax->cgst_name }}</td>
                            <td align="right">{{ number_format($cgst,2) }}</td>
                            <td align="right">{{ $tax->Tax->sgst_name }}</td>
                            <td align="right">{{ number_format($sgst,2) }}</td>
                            <td align="right">{{ number_format($gst,2) }}</td>
                            <td align="right">{{ number_format($grand_total,2) }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td align="right"><b>{{ number_format($final_taxable_value,2) }}</b></td>
                        <td></td>
                        <td align="right"><b>{{ number_format($final_cgst,2) }}</b></td>
                        <td></td>
                        <td align="right"><b>{{ number_format($final_sgst,2) }}</b></td>
                        <td align="right"><b>{{ number_format($final_gst,2) }}</b></td>
                        <td align="right"><b>{{ number_format($final_grand_total,2) }}</b></td>
                    </tr>       
                </table>
            </td>
            <td class="width-50">
                <table class="table-100">
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            For {{ $org->org_name }}
                        </td>
                    </tr>   
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="center">
                            Authorized Signatory
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    @if(!empty($transfer->terms))
        <table class="table-100 th-font">
            <tr>
                <td><b>Terms &amp; Conditions</b></td>
            </tr>
            <tr>
                <td>
                    <pre>{{ $transfer->terms }}</pre>
                </td>
            </tr>
        </table>
    @endif
     <htmlpagefooter name="page-footer">
        <table class="table-100">
            <tr>
                <td align="left">This is a computer generated Transfer and does not require seal and signature.</td>
                <td align="right">PAGE {PAGENO}</td>
            </tr>
        </table>
    </htmlpagefooter>
</body>
</html>
@php
    function convert_number_to_words($no)
    {   
        $words = array('0'=> 'ZERO' ,'1'=> 'ONE' ,'2'=> 'TWO' ,'3' => 'THREE','4' => 'FOUR','5' => 'FIVE','6' => 'SIX','7' => 'SEVEN','8' => 'EIGHT','9' => 'NINE','10' => 'TEN','11' => 'ELEVEN','12' => 'TWELVE','13' => 'THIRTEEN','14' => 'FOURTEEN','15' => 'FIFTEEN','16' => 'SIXTEEN','17' => 'SEVENTEEN','18' => 'EIGHTEEN','19' => 'NINETEEN','20' => 'TWENTY','30' => 'THIRTY','40' => 'FOURTY','50' => 'FIFTY','60' => 'SIXTY','70' => 'SEVENTY','80' => 'EIGHTY','90' => 'NINTY','100' => 'HUNDRED','1000' => 'THOUSAND','100000' => 'LAKH','10000000' => 'CRORE');
        if($no == 0)
        {
            return '';
        }
        else 
        {
            $novalue='';
            $highno=$no;
            $remainno=0;
            $value=100;
            $value1=1000;       
            while($no>=100)    
            {
                if(($value <= $no) &&($no  < $value1))    
                {
                    $novalue=$words["$value"];
                    $highno = (int)($no/$value);
                    $remainno = $no % $value;
                    break;
                }
                $value= $value1;
                $value1 = $value * 100;
            }       
            if(array_key_exists("$highno",$words))
            {
                return $words["$highno"]." ".$novalue." ".convert_number_to_words($remainno);
            }
            else 
            {
                $unit=$highno%10;
                $ten =(int)($highno/10)*10;            
                return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".convert_number_to_words($remainno);
            }
        }
    }
@endphp