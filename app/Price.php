<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Price extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'product_id','barcode', 'purchase_rate_exc', 'sales_rate_exc', 'tax_id', 'purchase_rate_inc', 'sales_rate_inc', 'created_by', 'updated_by',
    ];

    protected $primaryKey = 'price_id';

    protected $dates = ['deleted_at'];

    public function Product()
    {
        return $this->belongsTo('App\Product','product_id','product_id')->withTrashed();
    }

    public function Tax()
    {
        return $this->hasOne('App\Tax','tax_id','tax_id')->withTrashed();
    }

    public function BillStock()
    {
        return $this->hasOne('App\BillProduct','price_id','price_id')
                    ->selectRaw('price_id,SUM(quantity) as quantity')
                    ->groupBy('price_id');
    }
    public function TotalBranchStock()
    {
        return $this->hasOne('App\TransferProduct','price_id','price_id')
                    ->selectRaw('price_id,SUM(quantity) as quantity')
                    ->groupBy('price_id');
    }
    public function TransferStock()
    {
        $branch_id = Auth::User()->branch_id;
        return $this->hasOne('App\TransferProduct','price_id','price_id')
                    ->whereHas('Transfer',function($q) use($branch_id){
                        $q->where('branch_id',$branch_id);
                    })->selectRaw('price_id,SUM(quantity) as quantity')
                    ->groupBy('price_id');
    }
    public function InvoiceStock()
    {
        $branch_id = Auth::User()->branch_id;
        return $this->hasOne('App\InvoiceProduct','price_id','price_id')
                    ->whereHas('Invoice',function($q) use($branch_id){
                        $q->where('branch_id',$branch_id);
                    })->selectRaw('price_id,SUM(quantity) as quantity')
                    ->groupBy('price_id');
    }
    public function TotalBranchReturnStock()
    {
        return $this->hasOne('App\TransferReturnProduct','price_id','price_id')
                    ->selectRaw('price_id,SUM(quantity) as quantity')
                    ->groupBy('price_id');
    }

    public function TransferReturnStock()
    {
        $branch_id = Auth::User()->branch_id;
        return $this->hasOne('App\TransferReturnProduct','price_id','price_id')
                    ->whereHas('TransferReturn',function($q) use($branch_id){
                        $q->where('branch_id',$branch_id);
                    })->selectRaw('price_id,SUM(quantity) as quantity')
                    ->groupBy('price_id');
    }

    public function CurrentStock($price_id)
    {
        $product = Price::where('price_id',$price_id)->first();

        $purc_qty = 0;$sale_qty = 0; $transfer_qty = 0; $total_branch_qty = 0;

        $product->load(['BillStock']);
        $purc_qty = $product->CalcStock($product->BillStock);

        $product->load(['TransferStock']);
        $transfer_qty = $product->CalcStock($product->TransferStock);
        
        $product->load(['InvoiceStock']);
        $sale_qty = $product->CalcStock($product->InvoiceStock);

        //Total Sales Stock
        $product->load(['TotalBranchStock']);
        $total_branch_qty = $product->CalcStock($product->TotalBranchStock);

        // Return
        $product->load(['TransferReturnStock']);
        $transfer_return_qty = $product->CalcStock($product->TransferReturnStock);

        $product->load(['TotalBranchReturnStock']);
        $total_branch_return_qty = $product->CalcStock($product->TotalBranchReturnStock);

        return [
            'price_id'               => $price_id,
            'min_stock'              => $product->min_stock,
            'opening_stock'          => $transfer_qty-$sale_qty-$transfer_return_qty,
            'purchase_stock'         => $purc_qty,
            'transfer_stock'         => $transfer_qty,
            'transfer_return_stock'  => $transfer_return_qty,
            'sales_stock'            => $sale_qty,
            'main_stock'             => $purc_qty-$total_branch_qty+$total_branch_return_qty,
        ];
    }
    public function CalcStock($stock)
    {
        $qty = 0;
        if(!is_null($stock)){
            $qty = $stock->quantity;
        }
        return $qty;
    }
}
