<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// use App\Imports\ProductImport;
// use Maatwebsite\Excel\Facades\Excel;
// use Validator;

use App\Product;
use App\Price;
// use App\Store;
// use App\Warehouse;
use App\BillProduct;
use App\InvoiceProduct;
use App\TransferProduct;
use App\TransferReturnProduct;
// use App\DeliveryProduct;
// use App\InvoiceReturnProduct;
// use App\DeliveryReturnProduct;
use App\Preference;
use App\Organization;
use App\Category;
use App\Tax;
use App\Branch;
use Auth;
use PDF;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_product_code(Request $request)
    {
        $id = Product::withTrashed()->max('product_id');
        // $id = Product::max('product_id');
        $product_code = $id + 1;
        $length = strlen($product_code);
        $n = 4 - $length;
        for ($i=0; $i < $n; $i++) 
        { 
            $product_code = '0'.$product_code;
        }
        return $product_code;
    }

    public function display(Request $request)
    {
        $products = Product::
            where('product_type', 'like', '%'.$request->search.'%')
            ->orWhere('product_code', 'like', '%'.$request->search.'%')
            ->orWhere('hsn_code', 'like', '%'.$request->search.'%')
            ->orWhere('product_name', 'like', '%'.$request->search.'%')
            ->orWhere('product_unit', 'like', '%'.$request->search.'%')
            ->with('Category')
            ->paginate(10);
        // $products->each(function($product,$key){
        //     $product->current_stock = $product->CurrentStock($product->product_id);
        // }); 
        return $products;
    }

    public function validation(Request $request)
    {
        $this->validate($request, [
            'barcode'           => 'required',
            'purchase_rate_exc' => 'required|numeric',    
            'sales_rate_exc'    => 'required|numeric',    
            'tax_id'            => 'required',    
            'purchase_rate_inc' => 'required|numeric',    
            'sales_rate_inc'    => 'required|numeric',    
        ]);
        return $request;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'product_type'  => 'required|max:50',
            'product_code'  => 'required|max:50|unique:products',
            'hsn_code'      => 'max:50',
            'category_id'   => 'required|numeric',
            'product_name'  => 'required',
            'product_unit'  => 'max:50',
            'product_size'  => 'max:50',
            'min_stock'     => 'numeric',
        ]);
        
        $product = Product::create([
            'product_type'  => $request->product_type,
            'product_code'  => $request->product_code,
            'hsn_code'      => $request->hsn_code,
            'category_id'   => $request->category_id,
            'product_name'  => $request->product_name,
            'description'   => $request->description,
            'product_unit'  => $request->product_unit,
            'product_size'  => $request->product_size?$request->product_size:0,
            'min_stock'     => $request->min_stock,
            'discount'      => $request->discount,
            'created_by'    => Auth::User()->username,
        ]);

        foreach ($request->prices as $price) 
        {
            $price = Price::create([
                'product_id'        => $product->product_id,
                'barcode'           => $price['barcode'],
                'purchase_rate_exc' => $price['purchase_rate_exc'],
                'sales_rate_exc'    => $price['sales_rate_exc'],
                'tax_id'            => $price['tax_id'],
                'purchase_rate_inc' => $price['purchase_rate_inc'],
                'sales_rate_inc'    => $price['sales_rate_inc'],
                'created_by'        => Auth::User()->username,
            ]);
        }
        
        return Product::where('product_id',$product['product_id'])->with('Prices')->first();
    }

    public function edit(Product $product)
    {
        return Product::where('product_id',$product->product_id)->with('Prices')->first();
    }

    public function get_products(Request $request)
    {
        $products = Price::whereHas('Product',function($value) use($request){
                $value->where('product_code', 'like', '%'.$request->search.'%')
                      ->orWhere('product_name', 'like', '%'.$request->search.'%')
                      ->orWhere('hsn_code', 'like', '%'.$request->search.'%')
                      ->orderBy('product_name')
                      ->orderBy('product_code');
            })
            ->orWhere('barcode','like', '%'.$request->search.'%')
            ->limit(5)
            ->with('Product','Tax')
            ->get();

        $products->each(function($product,$key){
            $product->current_stock = $product->CurrentStock($product->price_id);
        });
         
        return $products;
    }

    public function view(Product $product)
    {
        return Product::where('product_id',$product->product_id)->with('Category')->first();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'product_type'  => 'required|max:50',
            'product_code'  => 'required|max:50',
            'hsn_code'      => 'max:50',
            'category_id'   => 'required|numeric',
            'product_name'  => 'required',
            'product_unit'  => 'max:50',
            'product_size'  => 'max:50',
            'min_stock'     => 'numeric',
        ]);

        Product::where('product_id',$request->product_id)->update([
            'product_type'  => $request->product_type,
            'product_code'  => $request->product_code,
            'hsn_code'      => $request->hsn_code,
            'category_id'   => $request->category_id,
            'product_name'  => $request->product_name,
            'description'   => $request->description,
            'product_unit'  => $request->product_unit,
            'product_size'  => $request->product_size?$request->product_size:0,
            'min_stock'     => $request->min_stock,
            'discount'      => $request->discount,
            'updated_by'    => Auth::User()->username, 
        ]);

        foreach ($request->deleted_prices as $price) 
        {
            Price::where('price_id',$price['price_id'])->delete();
        }


        foreach ($request->prices as $price) 
        {
            $product = Product::where('product_id',$request['product_id'])->first();
            
            if(empty($price['price_id'])) {
                Price::create([
                    'product_id'        => $product->product_id,
                    'barcode'           => $price['barcode'],
                    'purchase_rate_exc' => $price['purchase_rate_exc'],
                    'sales_rate_exc'    => $price['sales_rate_exc'],
                    'tax_id'            => $price['tax_id'],
                    'purchase_rate_inc' => $price['purchase_rate_inc'],
                    'sales_rate_inc'    => $price['sales_rate_inc'],
                    'created_by'        => Auth::User()->username,
                ]);
            }else{
                
                Price::where('price_id',$price['price_id'])->update([
                    'product_id'        => $product->product_id,
                    'barcode'           => $price['barcode'],
                    'purchase_rate_exc' => $price['purchase_rate_exc'],
                    'sales_rate_exc'    => $price['sales_rate_exc'],
                    'tax_id'            => $price['tax_id'],
                    'purchase_rate_inc' => $price['purchase_rate_inc'],
                    'sales_rate_inc'    => $price['sales_rate_inc'],
                    'updated_by'        => Auth::User()->username,
                ]);
            }
        }
    }

    public function destroy(Product $product)
    {
        
        Product::where('product_id',$product->product_id)->delete();
        Price::where('product_id',$product->product_id)->delete();
    }

    public function get_barcode(Request $request)
    {
        // return  Product::whereHas('Prices',function($query) use($request){
        //     $query->where('barcode',$request->input('barcode'));
        // })->with('Prices','Category')->first();
        $price  = Price::where('barcode',$request->input('barcode'))->with('Product','Tax')->first();
        if ($price) {
            $price->current_stock = $price->CurrentStock($price->price_id);
        }
        
        return $price;

    }

    public function get_product_list()
    {
        return Product::with('Prices')->get();
    }

    // public function new_price(Request $request)
    // {

    //     if ($request->product_id != '') {
    //         $price = Price::create([
    //             'product_id'        => $request->product_id,
    //             'barcode'           => $request['enable_barcode'],
    //             'purchase_rate_exc' => $request['purchase_rate_exc'],
    //             'sales_rate_exc'    => $request['sales_rate_exc'],
    //             'tax_id'            => $request['tax_id'],
    //             'purchase_rate_inc' => $request['purchase_rate_inc'],
    //             'sales_rate_inc'    => $request['sales_rate_inc'],
    //             'created_by'        => Auth::User()->username,
    //         ]);

    //         return $price;
    //     }
    // }

    public function add_new_barcode(Request $request)
    {
        if ($request->product_id != '') {

            $this->validate($request, [
                'product_id'        => 'required',
                'barcode'           => 'required',
                'purchase_rate_exc' => 'required|numeric',
                'sales_rate_exc'    => 'required|numeric',
                'tax_id'            => 'required',
                'purchase_rate_inc' => 'required|numeric',
                'sales_rate_inc'    => 'required|numeric',
            ]);

            $price = Price::create([
                'product_id'        => $request->product_id,
                'barcode'           => $request->barcode,
                'purchase_rate_exc' => $request->purchase_rate_exc,
                'sales_rate_exc'    => $request->sales_rate_exc,
                'tax_id'            => $request->tax_id,
                'purchase_rate_inc' => $request->purchase_rate_inc,
                'sales_rate_inc'    => $request->sales_rate_inc,
                'created_by'        => Auth::User()->username,
            ]);

            return Price::where('price_id',$price->price_id)->with('Product','Tax')->first();

        }else{

            $this->validate($request, [
                'category_id'   => 'required|numeric',
                'product_name'  => 'required',
            ]);

            $id = Product::withTrashed()->max('product_id');
            $product_code = $id + 1;
            $length = strlen($product_code);
            $n = 4 - $length;
            for ($i=0; $i < $n; $i++) 
            { 
                $product_code = '0'.$product_code;
            }

            $product = Product::create([
                'product_type'  => 'Goods',
                'product_code'  => $product_code,
                'category_id'   => $request->category_id,
                'product_name'  => $request->product_name,
                'product_unit'  => 'No',
                'product_size'  => $request->product_size?$request->product_size:0,
                'min_stock'     => 2,
                'created_by'    => Auth::User()->username,
            ]);

            $price = Price::create([
                'product_id'        => $product->product_id,
                'barcode'           => $request->barcode,
                'purchase_rate_exc' => $request->purchase_rate_exc,
                'sales_rate_exc'    => $request->sales_rate_exc,
                'tax_id'            => $request->tax_id,
                'purchase_rate_inc' => $request->purchase_rate_inc,
                'sales_rate_inc'    => $request->sales_rate_inc,
                'created_by'        => Auth::User()->username,
            ]);

            return Price::where('price_id',$price->price_id)->with('Product','Tax')->first();
        }
    }



    public function stock_report(Request $request)
    {
        $this->validate($request, [
            'from_date' => 'required|date'
        ]);
        
        $date = Date('Y-m-d',strtotime($request->from_date));
        $org = Organization::first();
        $preference = Preference::first();
        $auth_branch = Branch::where('branch_id',Auth::User()->branch_id)->first();
        
        $products = Product::with(['StockBillProducts','StockTransferProducts','StockInvoiceProducts','Prices','Category'])->get();

        if ($request->category_id!='') {
            $products = $products->filter(function($value) use($request){
                return ($value->category_id == $request->category_id);
            })->values();
        }

        $result = [];

        foreach ($products as $product) 
        {
            //Purchase
            $purchase = BillProduct::where('product_id',$product->product_id)
                ->whereHas('Bill', function($query) use($date){
                    $query->where('bill_date','<',$date);
                })->get();

            $bill = BillProduct::where('product_id',$product->product_id)
                ->whereHas('Bill', function($query) use($date){
                    $query->where('bill_date',$date);
                })->get();

            //Transfer
            $transfer = TransferProduct::where('product_id',$product->product_id)
                ->whereHas('Transfer', function($query) use($date){
                    $query->where('transfer_date','<',$date);
                })->get();
            
            $branch = TransferProduct::where('product_id',$product->product_id)
                ->whereHas('Transfer', function($query) use($date){
                    $query->where('transfer_date',$date);
                })->get();

            //Transfer Return
            $transfer_return = TransferReturnProduct::where('product_id',$product->product_id)
                ->whereHas('TransferReturn', function($query) use($date){
                    $query->where('transfer_return_date','<',$date);
                })->get();
            
            $branch_return = TransferReturnProduct::where('product_id',$product->product_id)
                ->whereHas('TransferReturn', function($query) use($date){
                    $query->where('transfer_return_date',$date);
                })->get();

            //Sales
            $sales = InvoiceProduct::where('product_id',$product->product_id)
                ->whereHas('Invoice', function($query) use($date){
                    $query->where('invoice_date','<',$date);
                })->get();

            $invoice = InvoiceProduct::where('product_id',$product->product_id)
                ->whereHas('Invoice', function($query) use($date){
                    $query->where('invoice_date',$date);
                })->get();

            // Purchase
            $purchase_stock = $purchase->sum('quantity');
            $bill_stock     = $bill->sum('quantity');

            //Transfer
            $transfer_stock = $transfer->sum('quantity');
            $branch_stock   = $branch->sum('quantity');

            //Transfer Return
            $transfer_return_stock = $transfer_return->sum('quantity');
            $branch_return_stock   = $branch_return->sum('quantity');

            //Sales
            $sales_stock    = $sales->sum('quantity');
            $invoice_stock  = $invoice->sum('quantity');

            if (Auth::User()->user_role == 'Super Admin' && $request->stock_type == 'Main Stock') {
                $opening_stock = $purchase_stock - $sales_stock;
                $closing_stock = $opening_stock + $bill_stock - $invoice_stock;
            }else{
                if ($request->branch_id != '') {
                    $auth_branch = Branch::where('branch_id',$request->branch_id)->first();
                    //Transfer
                    $transfer_stock = $this->BranchFilter($transfer,'Transfer',$request);
                    $branch_stock   = $this->BranchFilter($branch,'Transfer',$request);

                    //Transfer Return
                    $transfer_return_stock = $this->BranchFilter($transfer_return,'TransferReturn',$request);
                    $branch_return_stock   = $this->BranchFilter($branch_return,'TransferReturn',$request);

                    //Sales
                    $sales_stock    = $this->BranchFilter($sales,'Invoice',$request);
                    $invoice_stock  = $this->BranchFilter($invoice,'Invoice',$request);
                }

                $opening_stock = $transfer_stock - $sales_stock ;
                $closing_stock = $opening_stock + $branch_stock - $invoice_stock - $branch_return_stock;
            }

            $data = [
                'product_id'            => $product->product_id,
                'product_code'          => $product->product_code,
                'product_name'          => $product->product_name,
                'category_name'         => $product->Category->category_name,
                'opening_stock'         => $opening_stock,
                'purchase_stock'        => $bill_stock,
                'transfer_stock'        => $branch_stock,
                'transfer_return_stock' => $branch_return_stock,
                'sales_stock'           => $invoice_stock,
                'closing_stock'         => $closing_stock,
            ];
            array_push($result, $data);
        }
        switch ($request->display_type) {
            case 'pdf':
                PDF::loadView('stock.report', compact('org','preference','result','request','auth_branch'), [], [
                'margin_top' => 10
                ])->stream('stock-report.pdf');
                break;
            case 'excel':
                return view('stock.report',compact('org','preference','result','request','auth_branch'));
                break;
            
            default:
                return view('stock.report',compact('org','preference','result','request','auth_branch'));
                break;
        }
    }

    public function BranchFilter($data,$val,$request)
    {
        $sum = $data->filter(function($value) use($request,$val){
            return ($value->$val['Branch']['branch_id'] == $request->branch_id);
        })->values();

        return $sum->sum('quantity');
    }








    

    // public function excel_upload(Request $request)
    // {
    //     if($request->hasFile('product_excel')){
    //         Validator::make(
    //             [
    //                 'extension' => strtolower($request['product_excel']->getClientOriginalExtension()),
    //             ],
    //             [
    //                 'extension'      => 'required|in:xlsx,xls,ods',
    //             ]
    //         )->validate();

    //         $excel_products =  (new ProductImport)->toCollection(request()->file('product_excel'));
    //         $product_array = $excel_products->toArray();
    //         $products = $product_array[0];
            
    //         Validator::make($products, [
    //             "*.product_type" => 'required',
    //             "*.product_code" => 'required',
    //             "*.product_name" => 'required',
    //             "*.category"     => 'required|exists:categories,category_id',
    //             "*.min_stock"    => 'required|numeric',
    //             "*.purchase_rate_inc" => 'required|numeric',
    //             "*.sales_rate_inc"    => 'required|numeric',
    //             "*.tax"               => 'required|exists:taxes,tax_id',
    //             "*.purchase_rate_exc" => 'required|numeric',
    //             "*.sales_rate_exc"    => 'required|numeric',
    //         ])->validate();

    //         foreach($products as $product){
    //             // $category = Category::where('category_name',$product['category'])->first();
    //             $new_product = Product::create([
    //                 'product_type' => $product['product_type'],
    //                 'product_code' => $product['product_code'],
    //                 'hsn_code'     => $product['hsn_code'],
    //                 'category_id'  => $product['category'],
    //                 'product_name' => $product['product_name'],
    //                 'description'  => $product['description'],
    //                 'product_unit' => $product['product_unit'],
    //                 'min_stock'    => $product['min_stock'],
    //                 'created_by'   => Auth::User()->username,
    //             ]);
    //             // $tax = Tax::where('tax_name',$product['tax'])->first();
    //             $new_price = Price::create([
    //                 'product_id'        => $new_product['product_id'],
    //                 'purchase_rate_exc' => $product['purchase_rate_exc'],
    //                 'sales_rate_exc'    => $product['sales_rate_exc'],
    //                 'tax_id'            => $product['tax'],
    //                 'purchase_rate_inc' => $product['purchase_rate_inc'],
    //                 'sales_rate_inc'    => $product['sales_rate_inc'],
    //                 'created_by'        => Auth::User()->username,
    //             ]);
    //             $warehouse = Warehouse::first();
    //             Store::create([
    //                 'product_id'   => $new_product->product_id,
    //                 'price_id'     => $new_price->price_id,
    //                 'opening_stock'=> $product['opening_stock'],
    //                 'warehouse_id' => $warehouse->warehouse_id,
    //                 'created_by'   => Auth::User()->username,
    //             ]);  
    //         }
    //     }
    //     if($request->hasFile('stock_excel')){
    //             Store::create([
    //                 'product_id' => $product->product_id,
    //                 'price_id' => $price->price_id,
    //                 'warehouse_id' => 1,
    //                 'created_by' => Auth::User()->username,
    //             ]);
    //     }
    // }

    







    // public function current_stock(Request $request)
    // {
    //     $products = Product::orderBy('product_name')->get();
    //     return $products->filter(function($product,$key){
    //         $product->current_stock = $product->CurrentStock($product->product_id);
    //         return $product->current_stock['opening_stock']<=$product->current_stock['min_stock'];
    //     })->values();
    //     // $array[] = '';
    //     // $array_product[]='';
    //     // $int=0;
    //     // $preference = Preference::first();
    //     // switch($preference->inventory_on_calculation){
    //     //     case 'Invoice':
    //     //         $products->load(['BillStock','InvoiceStock']);
    //     //     break;
    //     //     case 'Delivery Challan':
    //     //         $products->load(['BillStock','DeliveryStock']);
    //     //     break;
    //     //     default:
    //     //     $products;
    //     // }
    //     // foreach($products as $product){
    //     //     //array_push($array ,$product['min_stock']) ;
    //     //     //if(($product['BillStock'])!=''){
    //     //         if($preference->inventory_on_calculation=='Invoice'){
    //     //             if($product['BillStock']!=''){
    //     //                 if(($product['BillStock']['quantity']-$product['InvoiceStock']['quantity']) <= $product['min_stock']){
    //     //                    array_push($array,$product['BillStock']['quantity']-$product['invoice_stock']['quantity']) ;
    //     //                    array_push($array_product,$product);
    //     //                 }
    //     //                 //array_push($array_product,$product['min_stock']);
    //     //             }
    //     //         }else{
    //     //             if(($product['BillStock']['quantity']-$product['DeliveryStock']['quantity']) <= $product['min_stock']){
    //     //                 array_push($array,$product['BillStock']['quantity']-$product['invoice_stock']['quantity']) ;
    //     //                 array_push($array_product,$product);
    //     //             }
    //     //         }
    //     //     }
    //     // //}

    //     // return array_except($array_product,0);
        

    //     //return (count($array)-1);
    // }

    // public function adjustment_stock(Request $request)
    // {

    //     $product_id = $request->product_id;
    //     $warehouse_id = $request->warehouse_id;
    //     $preference = Preference::first();
    //     $date = $request->reference_date;

    //     //Adjustment
    //     $adjustment = Adjustment::where('product_id',$product_id)->where('reference_date',$date)->get();

    //     if ($warehouse_id != null && $request->product_id !=null) {
    //         $adjustment = $adjustment->filter(function($value,$key) use($request){
    //         return ($value->warehouse_id == $request->warehouse_id);
    //         })->values();
    //     }
        
    //     $adjustment_stock = $adjustment->sum('adjusted_quantity');

    //     $old_adjustment = Adjustment::where('product_id',$product_id)->where('reference_date','<',$date)->get();

    //     if ($warehouse_id != null && $request->product_id !=null) {
    //         $old_adjustment = $old_adjustment->filter(function($value,$key) use($request){
    //         return ($value->warehouse_id == $request->warehouse_id);
    //         })->values();
    //     }

    //     $old_adjustment_stock = $old_adjustment->sum('adjusted_quantity');
        
    //     //Purchase
    //     $purchase = BillProduct::where('product_id',$product_id)
    //         ->whereHas('Bill', function($query) use($date){
    //             $query->where('bill_date','<',$date);
    //         })->get();

    //     if ($warehouse_id != null && $request->product_id !=null) {
    //         $purchase = $purchase->filter(function($value,$key) use($request){
    //         return ($value->warehouse_id == $request->warehouse_id);
    //         })->values();
    //     }
        
    //     $purchase_stock = $purchase->sum('quantity');


    //     //Bill
    //     $bill = BillProduct::where('product_id',$product_id)
    //         ->whereHas('Bill', function($query) use($date){
    //             $query->where('bill_date',$date);
    //         })->get();

    //     if ($warehouse_id != null && $request->product_id !=null) {
    //         $bill = $bill->filter(function($value,$key) use($request){
    //         return ($value->warehouse_id == $request->warehouse_id);
    //         })->values();
    //     }
        
    //     $bill_stock = $bill->sum('quantity');


    //     //Stock from invoice, Invoice Return, DC and DC Return

    //     if($preference->inventory_on_calculation=='Invoice') 
    //     {
    //         $sale = InvoiceProduct::where('product_id',$product_id)
    //             ->whereHas('Invoice', function($query) use($date){
    //                 $query->where('invoice_date','<',$date);
    //             })->get();

    //         if ($warehouse_id != null && $request->product_id !=null) {
    //             $sale = $sale->filter(function($value,$key) use($request){
    //             return ($value->warehouse_id == $request->warehouse_id);
    //             })->values();
    //         }
            
    //         $sales_stock = $sale->sum('quantity');

    //         $invoice = InvoiceProduct::where('product_id',$product_id)
    //             ->whereHas('Invoice', function($query) use($date){
    //                 $query->where('invoice_date',$date);
    //             })->get();

    //         if ($warehouse_id != null && $request->product_id !=null) {
    //             $invoice = $invoice->filter(function($value,$key) use($request){
    //             return ($value->warehouse_id == $request->warehouse_id);
    //             })->values();
    //         }
            
    //         $invoice_stock = $invoice->sum('quantity');

    //         //Invoice Return
    //         $sale_return = InvoiceReturnProduct::where('product_id',$product_id)
    //             ->whereHas('InvoiceReturn', function($query) use($date){
    //                 $query->where('invoice_return_date','<',$date);
    //             })->get();

    //         if ($warehouse_id != null && $request->product_id !=null) {
    //             $sale_return = $sale_return->filter(function($value,$key) use($request){
    //             return ($value->warehouse_id == $request->warehouse_id);
    //             })->values();
    //         }
            
    //         $sales_return_stock = $sale_return->sum('quantity');

    //         $invoice_return = InvoiceReturnProduct::where('product_id',$product_id)
    //             ->whereHas('InvoiceReturn', function($query) use($date){
    //                 $query->where('invoice_return_date',$date);
    //             })->get();

    //         if ($warehouse_id != null && $request->product_id !=null) {
    //             $invoice_return = $invoice_return->filter(function($value,$key) use($request){
    //             return ($value->warehouse_id == $request->warehouse_id);
    //             })->values();
    //         }
            
    //         $invoice_return_stock = $invoice_return->sum('quantity');

    //     }
    //     else
    //     {
    //         $sale = DeliveryProduct::where('product_id',$product_id)
    //             ->whereHas('Delivery', function($query) use($date){
    //                 $query->where('delivery_date','<',$date);
    //             })->get();

    //         if ($warehouse_id != null && $request->product_id !=null) {
    //             $sale = $sale->filter(function($value,$key) use($request){
    //             return ($value->warehouse_id == $request->warehouse_id);
    //             })->values();
    //         }
            
    //         $sales_stock = $sale->sum('quantity');

    //         $invoice = DeliveryProduct::where('product_id',$product_id)
    //             ->whereHas('Delivery', function($query) use($date){
    //                 $query->where('delivery_date',$date);
    //             })->get();

    //         if ($warehouse_id != null && $request->product_id !=null) {
    //             $invoice = $invoice->filter(function($value,$key) use($request){
    //             return ($value->warehouse_id == $request->warehouse_id);
    //             })->values();
    //         }
            
    //         $invoice_stock = $invoice->sum('quantity');

            
    //         //DC Return
    //         $sale_return = DeliveryReturnProduct::where('product_id',$product_id)
    //             ->whereHas('DeliveryReturn', function($query) use($date){
    //                 $query->where('delivery_return_date','<',$date);
    //             })->get();

    //         if ($warehouse_id != null && $request->product_id !=null) {
    //             $sale_return = $sale_return->filter(function($value,$key) use($request){
    //             return ($value->warehouse_id == $request->warehouse_id);
    //             })->values();
    //         }
            
    //         $sales_return_stock = $sale_return->sum('quantity');

    //         $invoice_return = DeliveryReturnProduct::where('product_id',$product_id)
    //             ->whereHas('DeliveryReturn', function($query) use($date){
    //                 $query->where('delivery_return_date',$date);
    //             })->get();

    //         if ($warehouse_id != null && $request->product_id !=null) {
    //             $invoice_return = $invoice_return->filter(function($value,$key) use($request){
    //             return ($value->warehouse_id == $request->warehouse_id);
    //             })->values();
    //         }
            
    //         $invoice_return_stock = $invoice_return->sum('quantity');
    //     }
        
        
    //     $prdt = Product::where('product_id',$request->product_id)->first();
        

    //     $opening_stock = $prdt->Store->opening_stock + $purchase_stock - $sales_stock + $old_adjustment_stock + $sales_return_stock;

    //     $closing_stock = $opening_stock + $adjustment_stock + $bill_stock - $invoice_stock + $invoice_return_stock;

    //     // dd('op',$opening_stock,'cl',$closing_stock,'cs',$prdt->Store->opening_stock,'ps',$purchase_stock,'sls',$sales_stock,'os',$opening_stock,'ads',$adjustment_stock,'bs',$bill_stock,'is',$invoice_stock);

    //     $product =(object) array(
    //         //'opening_stock' => $opening_stock,
    //         'closing_stock' => $closing_stock,
    //     );
        
    //     return response()->json($product);
    // }


}
