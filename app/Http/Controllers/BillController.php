<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use PDF;
use App\Bill;
use App\BillProduct;
use App\Preference;
use App\Organization;
use App\Contact;
use App\Product;
use App\Price;
// use App\Store;
// use App\Payment;
// use App\PaymentParticular;
// use App\PurchaseOrderProduct;
// use App\PurchaseOrder;
use App\Account;
use App\PaymentTerm;
use App\Master;

use App\Imports\BillsImport;
use Maatwebsite\Excel\Facades\Excel;

class BillController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_bill_no(Request $request)
    {
        $preference = Preference::first();
        $id = Bill::where('fiscal_year',$preference->fiscal_year)->max('bill_id');
        $id = $id + 1;
        $n = $preference->bill_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $bill_no = $preference->bill_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $bill_no;
    }

    public function validation(Request $request)
    {
        $this->validate($request, [
            'product_id'        => 'required|numeric',
            'product_code'      => 'required|max:50',
            'product_name'      => 'required|max:255',
            'price_id'          => 'required|numeric',
            'purchase_rate_exc' => 'required|numeric',
            'purchase_rate_inc' => 'required|numeric',
            'discount'          => 'required|numeric',
            'discount_type'     => 'required|max:50',
            'discount_amount'   => 'required|numeric',
            'tax_id'            => 'required|numeric',
            'tax_amount'        => 'required|numeric',
            'quantity'          => 'required|numeric',
            'amount'            => 'required|numeric',
            'sub_total'         => 'required|numeric',
        ]);
        return $request;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'bill_no'           => 'required|max:50',
            'bill_date'         => 'required|date',
            'reference_no'      => 'max:50',
            'reference_date'    => 'sometimes|nullable|date',
            'vendor_id'         => 'required|numeric',
            'source_id'         => 'required|numeric',
            'destination_id'    => 'required|numeric',
            'sub_total'         => 'required|numeric',
            'discount_amount'   => 'required|numeric',
            'tax_amount'        => 'required|numeric',
            'round_off'         => 'required|numeric',
            'total_amount'      => 'required|numeric',
            'grand_total'       => 'required|numeric',
            'term_id'           => 'sometimes|nullable|numeric',
            'terms'             => 'max:2550',
            'note'              => 'max:2550',
            'due_date'          => 'required|date',
            'payment_term_id'   => 'required|numeric',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->vendor_id)->first();
        //$purchase_order_ids[] = '';

        $bill = Bill::create([
            'fiscal_year'       => $preference->fiscal_year,
            'bill_no'           => $request->bill_no,
            'bill_date'         => date("Y-m-d", strtotime($request->bill_date)),
            'due_date'          => date("Y-m-d", strtotime($request->due_date)),
            'reference_no'      => $request->reference_no,
            'reference_date'    => $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'vendor_id'         => $request->vendor_id,
            'billing_address'   => $contact->billing_address,
            'shipping_address'  => $contact->shipping_address,
            'source_id'         => $request->source_id,
            'payment_term_id'   => $request->payment_term_id,
            'destination_id'    => $request->destination_id,
            'sub_total'         => $request->sub_total,
            'discount_amount'   => $request->discount_amount,
            'tax_amount'        => $request->tax_amount,
            'total_amount'      => $request->total_amount,
            'round_off'         => $request->round_off,
            'grand_total'       => $request->grand_total,
            'term_id'           => $request->term_id,
            'terms'             => $request->terms,
            'note'              => $request->note,
            'bill_status'       => 'Open',
            'created_by'        => Auth::User()->username,
        ]);

        foreach ($request->bill_products as $bill_product) 
        {
            $product = Product::where('product_id',$bill_product['product_id'])->withTrashed()->first();
            $price = Price::where('price_id',$bill_product['price_id'])->withTrashed()->first();

            $bill_up = BillProduct::create([
                'bill_id'           => $bill->bill_id,
                'product_id'        => $bill_product['product_id'],
                'product_type'      => $product['product_type'],
                'product_code'      => $bill_product['product_code'],
                'hsn_code'          => $product->hsn_code,
                'category_id'       => $product->category_id,
                'product_name'      => $bill_product['product_name'],
                'description'       => $product->description,
                'product_unit'      => $product->product_unit,
                'price_id'          => $bill_product['price_id'],
                'barcode'           => $price->barcode,
                'purchase_rate_exc' => $bill_product['purchase_rate_exc'],
                'sales_rate_exc'    => $price->sales_rate_exc,
                'purchase_rate_inc' => $bill_product['purchase_rate_inc'],
                'sales_rate_inc'    => $price->sales_rate_inc,
                'quantity'          => $bill_product['quantity'],
                'amount'            => $bill_product['amount'],
                'discount'          => $bill_product['discount'],
                'discount_type'     => $bill_product['discount_type'],
                'discount_amount'   => $bill_product['discount_amount'],
                'tax_id'            => $bill_product['tax_id'],
                'tax_amount'        => $bill_product['tax_amount'],
                'sub_total'         => $bill_product['sub_total'],
                'created_by'        => Auth::User()->username,
            ]);
        }

    }

    public function display(Request $request)
    {
        return Bill::whereHas('Vendor', function($query) use($request){
                $query->where('contact_name','like', "%$request->search%");
            })
            ->orWhere('bill_no', 'like', '%'.$request->search.'%')
            ->orWhere('bill_date', 'like', '%'.$request->search.'%')
            ->orWhere('sub_total', 'like', '%'.$request->search.'%')
            ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
            ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
            ->orWhere('grand_total', 'like', '%'.$request->search.'%')
            ->with('Vendor')
            ->orderBy('bill_id','DESC')
            ->paginate(10);
    }

    public function view(Bill $bill)
    {
        $bill = Bill::where('bill_id',$bill->bill_id)->with(['BillProducts','Vendor'])->first();
        $pre = Preference::first();
        //$bill->frontend_status = $pre->frontEndStatus($bill,'Bill',['PaymentParticularsCheck']);
        return $bill;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'bill_no'           => 'required|max:50',
            'bill_date'         => 'required|date',
            'reference_no'      => 'max:50',
            'reference_date'    => 'sometimes|nullable|date',
            'vendor_id'         => 'required|numeric',
            'source_id'         => 'required|numeric',
            'destination_id'    => 'required|numeric',
            'sub_total'         => 'required|numeric',
            'discount_amount'   => 'required|numeric',
            'tax_amount'        => 'required|numeric',
            'round_off'         => 'required|numeric',
            'total_amount'      => 'required|numeric',
            'grand_total'       => 'required|numeric',
            'term_id'           => 'sometimes|nullable|numeric',
            'terms'             => 'max:2550',
            'note'              => 'max:2550',
            'due_date'          => 'required|date',
            'payment_term_id'   => 'required|numeric',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->vendor_id)->first();
        $bill = Bill::where('bill_id',$request->bill_id)->first();
        $bill_products = Bill::where('bill_id',$request->bill_id)->get();
        Bill::where('bill_id',$request->bill_id)->update([
            'fiscal_year'       => $bill->fiscal_year,
            'bill_no'           => $request->bill_no,
            'bill_date'         => date("Y-m-d", strtotime($request->bill_date)),
            'due_date'          => date("Y-m-d", strtotime($request->due_date)),
            'reference_no'      => $request->reference_no,
            'reference_date'    => $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : $bill->reference_date,
            'vendor_id'         => $request->vendor_id,
            'billing_address'   => $contact->billing_address,
            'shipping_address'  => $contact->shipping_address,
            'source_id'         => $request->source_id,
            'payment_term_id'   => $request->payment_term_id,
            'destination_id'    => $request->destination_id,
            'sub_total'         => $request->sub_total,
            'discount_amount'   => $request->discount_amount,
            'tax_amount'        => $request->tax_amount,
            'total_amount'      => $request->total_amount,
            'round_off'         => $request->round_off,
            'grand_total'       => $request->grand_total,
            'term_id'           => $request->term_id,
            'terms'             => $request->terms,
            'note'              => $request->note,
            'updated_by'        => Auth::User()->username,
        ]);

        foreach ($request->deleted_bill_products as $bill_product) 
        {
            BillProduct::where('bill_product_id',$bill_product['bill_product_id'])->delete();
        }

        foreach ($request->bill_products as $bill_product) 
        {
            $product = Product::where('product_id',$bill_product['product_id'])->first();
            $price = Price::where('price_id',$bill_product['price_id'])->first();
            //$store = Store::where('store_id',$bill_product['store_id'])->first();

            if(empty($bill_product['bill_product_id'])) {
                BillProduct::create([
                    'bill_id'           => $bill->bill_id,
                    'product_id'        => $bill_product['product_id'],
                    'product_type'      => $product->product_type,
                    'product_code'      => $bill_product['product_code'],
                    'hsn_code'          => $product->hsn_code,
                    'category_id'       => $product->category_id,
                    'product_name'      => $bill_product['product_name'],
                    'description'       => $product->description,
                    'product_unit'      => $product->product_unit,
                    'price_id'          => $bill_product['price_id'],
                    'barcode'           => $price->barcode,
                    'purchase_rate_exc' => $bill_product['purchase_rate_exc'],
                    'sales_rate_exc'    => $price->sales_rate_exc,
                    'purchase_rate_inc' => $bill_product['purchase_rate_inc'],
                    'sales_rate_inc'    => $price->sales_rate_inc,
                    'quantity'          => $bill_product['quantity'],
                    'amount'            => $bill_product['amount'],
                    'discount'          => $bill_product['discount'],
                    'discount_type'     => $bill_product['discount_type'],
                    'discount_amount'   => $bill_product['discount_amount'],
                    'tax_id'            => $bill_product['tax_id'],
                    'tax_amount'        => $bill_product['tax_amount'],
                    'sub_total'         => $bill_product['sub_total'],
                    'created_by'        => Auth::User()->username,
                ]);
            }
            else
            {
                BillProduct::where('bill_product_id',$bill_product['bill_product_id'])->update([
                    'bill_id'           => $bill->bill_id,
                    'product_id'        => $bill_product['product_id'],
                    'product_type'      => $product->product_type,
                    'product_code'      => $bill_product['product_code'],
                    'hsn_code'          => $product->hsn_code,
                    'category_id'       => $product->category_id,
                    'product_name'      => $bill_product['product_name'],
                    'description'       => $product->description,
                    'product_unit'      => $product->product_unit,
                    'price_id'          => $bill_product['price_id'],
                    'barcode'           => $price->barcode,
                    'purchase_rate_exc' => $bill_product['purchase_rate_exc'],
                    'sales_rate_exc'    => $price->sales_rate_exc,
                    'purchase_rate_inc' => $bill_product['purchase_rate_inc'],
                    'sales_rate_inc'    => $price->sales_rate_inc,
                    'quantity'          => $bill_product['quantity'],
                    'amount'            => $bill_product['amount'],
                    'discount'          => $bill_product['discount'],
                    'discount_type'     => $bill_product['discount_type'],
                    'discount_amount'   => $bill_product['discount_amount'],
                    'tax_id'            => $bill_product['tax_id'],
                    'tax_amount'        => $bill_product['tax_amount'],
                    'sub_total'         => $bill_product['sub_total'],
                    'updated_by'        => Auth::User()->username,
                ]);
            }
        }
    }
    public function destroy(Bill $bill)
    {
        BillProduct::where('bill_id',$bill->bill_id)->forceDelete();
        Bill::where('bill_id',$bill->bill_id)->forceDelete();
    }

    //Excel import bills
    public function import(Request $request)
    {

        $this->validate($request,[
            'file' => 'required|file|mimes:xlsx',
        ]);
        Excel::import(new BillsImport,$request->file('file'));
    }
    public function report(Request $request)
    {
        $this->validate($request, [
            'from_date' => 'required|date',
            'to_date'   => 'required|date',
        ]);

        
        $from_date = Date('Y-m-d',strtotime($request->from_date));
        $to_date = Date('Y-m-d',strtotime($request->to_date));
        $org = Organization::first();
        $preference = Preference::first();
    
        $bills = Bill::whereBetween('bill_date',[$from_date,$to_date])->get();
        

        switch ($request->display_type) {
            case 'pdf':
                PDF::loadView('bill.report', compact('org','preference','bills','request'), [], [
                'margin_top' => 10
                ])->stream('bill-report.pdf');
                break;
            case 'excel':
                return view('bill.report',compact('org','preference','bills','request'));
                break;
            
            default:
                return view('bill.report',compact('org','preference','bills','request'));
                break;
        }
    }


    // public function pdf(Bill $bill)
    // {
    //     $org = Organization::first();

    //     // $preference = Preference::first();
    //     // $default_pdf = 'purchase_order.'.$preference->purchase_order_pdf;
    //     // if($preference->purchase_order_pdf == 'pdf1' )
    //     // {
    //     //  $margin_top = 41.8;
    //     // }
    //     // elseif($preference->purchase_order_pdf == 'pdf2')
    //     // {
    //     //  $margin_top = 41.8;
    //     // }
    //     // elseif($preference->purchase_order_pdf == 'pdf3')
    //     // {
    //     //  $margin_top = 48;
    //     // }
    //     // elseif($preference->purchase_order_pdf == 'pdf4')
    //     // {
    //     //  $margin_top = 48;
    //     // }
    //     // else
    //     // {
    //     //  $margin_top = 10;
    //     // }

    //     $bill = Bill::where('bill_id',$bill->bill_id)->with('Vendor','BillProducts')->first();

    //     PDF::loadView('bill.pdf', compact('bill','org'), [], [
    //         'margin_top' => 41.8,
    //     ])->stream($bill->bill_no.'.pdf');
    // }

    // public function get_unpaid_bills(Request $request)
    // {
    //     $bills =  Bill::where('vendor_id',$request->vendor_id)->get();
    //     $result = [];
    //     foreach ($bills as $bill) 
    //     {
    //         $paid_amount = PaymentParticular::where('reference_id',$bill->bill_id)->sum('paid_amount');
    //         $due_amount = $bill->grand_total - $paid_amount;
    //         if($due_amount!=0)
    //         {
    //             $data = [
    //                 'check' => false,
    //                 'bill_id' => $bill->bill_id,
    //                 'bill_no' => $bill->bill_no,
    //                 'bill_date' => $bill->bill_date,
    //                 'grand_total' => $bill->grand_total,
    //                 'paid_amount' => $paid_amount,
    //                 'due_amount' => $due_amount,
    //                 'payable_amount' => 0.00,
    //             ];
    //             array_push($result, $data);
    //         }
    //     }
    //     return $result;
    // }
    // public function report(Request $request)
    // {
    //     $org = Organization::first();
    //     //$preference= Preference::first();
    //     if($request->vendor_id=='')
    //     {
    //         $bills = Bill::
    //             whereBetween('bill_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
    //             ->with('BillProducts')
    //             ->with('Vendor')->get();
    //         $vendor = '';
    //     }
    //     else
    //     {
    //         $bills = Bill::
    //             where('vendor_id',$request->vendor_id)
    //             ->whereBetween('bill_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])
    //            ->with('BillProducts')
    //             ->with('Vendor')->get();
    //         $vendor = Contact::where('contact_id',$request->vendor_id)->first();
    //     }
    //     if($request->display_type=='display')
    //     {
    //         return $bills;
    //     }
    //     else if($request->display_type=='pdf')
    //     {
    //         PDF::loadView('bill.report', compact('bills','org','request','vendor','preference'), [], [
    //             'margin_top' => 10
    //         ])->stream('bill-report.pdf');
    //     }
    //     else
    //     {
    //         return view('bill.report', compact('bills','org','request','vendor','preference'));
    //     }
    // }

    // //convert_purchase
    // public function convert_purchase(Request $request)
    // {
    //     //dd($request->input());
    //     $purchase_order_product = PurchaseOrderProduct::whereIn('purchase_order_product_id',$request->input())->with('Tax','PurchaseOrder')->with('Vendor')->get();
    //     return $purchase_order_product;
    // }
    
    // public function pdf(Bill $bill)
    // {
    //     $org = Organization::first();

    //     // $preference = Preference::first();
    //     // $default_pdf = 'purchase_order.'.$preference->purchase_order_pdf;
    //     // if($preference->purchase_order_pdf == 'pdf1' )
    //     // {
    //     //  $margin_top = 41.8;
    //     // }
    //     // elseif($preference->purchase_order_pdf == 'pdf2')
    //     // {
    //     //  $margin_top = 41.8;
    //     // }
    //     // elseif($preference->purchase_order_pdf == 'pdf3')
    //     // {
    //     //  $margin_top = 48;
    //     // }
    //     // elseif($preference->purchase_order_pdf == 'pdf4')
    //     // {
    //     //  $margin_top = 48;
    //     // }
    //     // else
    //     // {
    //     //  $margin_top = 10;
    //     // }

    //     $bill = Bill::where('bill_id',$bill->bill_id)->with('Vendor','BillProducts')->first();

    //     PDF::loadView('bill.pdf1', compact('bill','org'), [], [
    //         'margin_top' => 41.8,
    //     ])->stream($bill->bill_no.'.pdf');
    // }

    // public function tax_report(Request $request)
    // {
    //     $org = Organization::first();
        
    //     $bills = Bill::
    //         whereBetween('bill_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])->with(['BillTax','Vendor'])->get();
    //     if($request->display_type=='display')
    //     {
    //         return $bills;
    //     }
    //     else if($request->display_type=='pdf')
    //     {
    //         PDF::loadView('bill.gst_report', compact('bills','org','request'), [], [
    //             'margin_top' => 10
    //         ])->stream('bill-gst-report.pdf');
    //     }
    //     else
    //     {
    //         return view('bill.gst_report', compact('bills','org','request'));
    //     }
    // }

}
