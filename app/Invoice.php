<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','invoice_type','invoice_no','invoice_date','reference_no','reference_date','branch_id','customer_id','billing_address','shipping_address','source_id','payment_term_id','due_date','destination_id','sub_total','discount_amount','tax_amount','total_amount','round_off','grand_total','card_discount_amount','payable_amount','term_id','terms','note','invoice_status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'invoice_id';

    protected $dates = ['deleted_at'];

    public function Customer()
    {
        return $this->hasOne('App\Contact','contact_id','customer_id')->withTrashed();
    }

    public function Branch()
    {
        return $this->hasOne('App\Branch','branch_id','branch_id')->withTrashed();
    }

    public function InvoiceProducts()
    {
        return $this->hasMany('App\InvoiceProduct','invoice_id','invoice_id')->with('Tax');
    }
    public function PaymentTerm()
    {
        return $this->hasOne('App\PaymentTerm', 'payment_term_id', 'payment_term_id');
    }
    public function SourcePlace()
    {
        return $this->hasOne('App\Place','place_id','source_id')->withTrashed();
    }
    public function DestinationPlace()
    {
        return $this->hasOne('App\Place','place_id','destination_id')->withTrashed();
    }
    public function SubTotal($invoice_id)
    {
     $sub_total = round(InvoiceProduct::where('invoice_id',$invoice_id)->sum('amount'),2);
     return $sub_total;
    }
    public function Discount($invoice_id)
    {
        $discount = round(InvoiceProduct::where('invoice_id',$invoice_id)->sum('discount_amount'),2);
        return $discount;
    }
    public function TotalTax($invoice_id)
    {
     $total_tax = round(InvoiceProduct::where('invoice_id',$invoice_id)->sum('tax_amount'),2);
     return $total_tax;
    }
    public function Taxes($invoice_id)
    {
     return InvoiceProduct::where('invoice_id',$invoice_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }
    public function TaxableValue($invoice_id,$tax_id)
    {
        $amount = round(InvoiceProduct::where([['invoice_id',$invoice_id],['tax_id',$tax_id]])->sum('amount'),2);
        $discount = round(InvoiceProduct::where([['invoice_id',$invoice_id],['tax_id',$tax_id]])->sum('discount_amount'),2);
        
        return $amount;
        // -$discount
    }
    public function InvoiceReceiptTotal(){
        return $this->hasOne('App\ReceiptParticular','reference_id','invoice_id')
                    ->selectRaw('reference_id,SUM(paid_amount) as paid_amount')
                    ->where('reference','App\Invoice')
                    ->groupBy('reference_id');
    }
    public function InvoiceTax(){
        return $this->hasMany('App\InvoiceProduct','invoice_id','invoice_id')
                    ->selectRaw('invoice_id,tax_id,SUM(amount)-SUM(discount_amount) as amount,SUM(tax_amount)/2 as cgst_amount,SUM(tax_amount)/2 as sgst_amount,SUM(tax_amount) as igst_amount,SUM(discount_amount) as discount_amount,SUM(sub_total) as sub_total,SUM(quantity) as quantity')
                    ->with('Tax')
                    ->groupBy(['invoice_id','tax_id']);
    }
    // public function MorphReceipts()
    // {
    //     return $this->morphMany('App\ReceiptParticular', 'reference');
    // }
    
}
