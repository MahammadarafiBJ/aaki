<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_products', function (Blueprint $table) {
            $table->bigIncrements('invoice_product_id');
            $table->bigInteger('invoice_id')->unsigned();
            $table->string('reference',50)->nullable();
            $table->bigInteger('reference_id')->unsigned()->nullable();
            $table->bigInteger('product_id')->unsigned();
            $table->string('product_type',50);
            $table->string('product_code',50);
            $table->string('hsn_code',50)->nullable();
            $table->bigInteger('category_id')->unsigned();
            $table->longtext('product_name');
            $table->longtext('description')->nullable();
            $table->string('product_unit',50)->nullable();
            $table->bigInteger('price_id')->unsigned();
            $table->longtext('barcode');
            $table->double('purchase_rate_exc',15,2);
            $table->double('sales_rate_exc',15,2);
            $table->double('purchase_rate_inc',15,2);
            $table->double('sales_rate_inc',15,2);
            $table->double('quantity',15,2);
            $table->double('amount',15,2);
            $table->double('discount',15,2);
            $table->string('discount_type',50);
            $table->double('discount_amount',15,2);
            $table->bigInteger('tax_id')->unsigned();
            $table->double('tax_amount',15,2);
            $table->double('sub_total',15,2);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('invoice_id')->references('invoice_id')->on('invoices');
            $table->foreign('product_id')->references('product_id')->on('products');
            $table->foreign('category_id')->references('category_id')->on('categories');
            $table->foreign('price_id')->references('price_id')->on('prices');
            $table->foreign('tax_id')->references('tax_id')->on('taxes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_products');
    }
}
