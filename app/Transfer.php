<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transfer extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'fiscal_year','transfer_no','transfer_date','branch_id','sub_total','discount_amount','tax_amount','total_amount','round_off','grand_total','term_id','terms','note','transfer_status','created_by', 'updated_by',
    ];

    protected $primaryKey = 'transfer_id';

    protected $dates = ['deleted_at'];

    public function Branch()
    {
        return $this->hasOne('App\Branch','branch_id','branch_id')->withTrashed();
    }
    public function TransferProducts()
    {
        return $this->hasMany('App\TransferProduct','transfer_id','transfer_id')->with('Tax');
    }

    public function SubTotal($transfer_id)
    {
     $sub_total = round(TransferProduct::where('transfer_id',$transfer_id)->sum('amount'),2);
     return $sub_total;
    }
    public function Discount($transfer_id)
    {
        $discount = round(TransferProduct::where('transfer_id',$transfer_id)->sum('discount_amount'),2);
        return $discount;
    }
    public function TotalTax($transfer_id)
    {
     $total_tax = round(TransferProduct::where('transfer_id',$transfer_id)->sum('tax_amount'),2);
     return $total_tax;
    }
    public function Taxes($transfer_id)
    {
     return TransferProduct::where('transfer_id',$transfer_id)->distinct()->orderBy('tax_id')->get(['tax_id']);
    }
    public function TaxableValue($transfer_id,$tax_id)
    {
         $amount = round(TransferProduct::where([['transfer_id',$transfer_id],['tax_id',$tax_id]])->sum('amount'),2);

         $discount = round(TransferProduct::where([['transfer_id',$transfer_id],['tax_id',$tax_id]])->sum('discount_amount'),2);
         
         $preference = Preference::first();

         if ($preference->discount_on == 'Exc') {
             return $amount - $discount;
         }else{
            return $amount; 
         }
         
    }
}
