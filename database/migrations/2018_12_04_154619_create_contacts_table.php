<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->bigIncrements('contact_id');
            $table->string('contact_type',50);
            $table->string('contact_code',50);
            $table->string('contact_name',255);
            $table->string('email',50)->nullable();
            $table->string('mobile_no',50)->unique();
            $table->string('phone_no',50)->nullable();
            $table->string('pan_no',50)->nullable();
            $table->string('gstin_no',50)->nullable();
            $table->string('billing_address',255)->nullable();
            $table->string('shipping_address',255)->nullable();
            $table->string('account_no',50)->nullable();
            $table->string('branch_name',50)->nullable();
            $table->string('bank_name',50)->nullable();
            $table->string('ifsc_code',50)->nullable();
            $table->bigInteger('payment_term_id')->unsigned();
            $table->bigInteger('place_id')->unsigned();
            $table->date('date_of_birth')->nullable();
            $table->date('wedding_anniversary')->nullable();
            $table->string('card_no',50)->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('payment_term_id')->references('payment_term_id')->on('payment_terms');
            $table->foreign('place_id')->references('place_id')->on('places');
        });

        DB::table('contacts')->insert(
            [
                [
                    'contact_type' => 'Vendor',
                    'contact_code' => '001',
                    'contact_name' => 'MDBS Tech Private Limited',
                    'mobile_no' => '9876543210',
                    'payment_term_id' => 1,
                    'place_id' =>1,
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
