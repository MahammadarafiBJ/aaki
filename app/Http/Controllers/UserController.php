<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use App\Branch;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function display(Request $request)
    {
        $branch_id = Auth::User()->branch_id;

        if (Auth::User()->user_role == 'Super Admin') {
            $user = User::where('name', 'like', '%'.$request->search.'%')
                      ->orWhere('username', 'like', '%'.$request->search.'%')
                      ->orWhere('email', 'like', '%'.$request->search.'%')
                      ->orWhere('mobile_no', 'like', '%'.$request->search.'%')
                      ->orWhere('user_role', 'like', '%'.$request->search.'%')
                      ->orwhereHas('Branch',function($query) use($request){
                        $query->where('branch_name','like', '%'.$request->search.'%');
                      })
                      ->with('Branch')
                      ->paginate(10);
        }else{
            $user = User::where('branch_id',$branch_id)
                ->where(function($value) use($request){
                    $value->where('name', 'like', '%'.$request->search.'%')
                      ->orWhere('username', 'like', '%'.$request->search.'%')
                      ->orWhere('email', 'like', '%'.$request->search.'%')
                      ->orWhere('mobile_no', 'like', '%'.$request->search.'%')
                      ->orWhere('user_role', 'like', '%'.$request->search.'%')
                      ->orwhereHas('Branch',function($query) use($request){
                        $query->where('branch_name','like', '%'.$request->search.'%');
                      });
                })->with('Branch')
                ->paginate(10);
        }
        return $user;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|max:50',
            'username'  => 'required|max:50|unique:users',
            'email'     => 'required|email|max:50|unique:users',
            'password'  => 'required|min:6',
            'user_role' => 'required|max:50',
            'mobile_no' => 'required|max:13|regex:/[0-9]{10}/',
            'address'   => 'required|max:2550',
            'branch_id' => 'required|numeric',

        ]);

        User::create([
            'branch_id' => $request->branch_id,
            'name'      => $request->name,
            'username'  => $request->username,
            'email'     => $request->email,
            'password'  => Hash::make($request->password),
            'user_role' => $request->user_role,
            'mobile_no' => $request->mobile_no,
            'address'   => $request->address,
            'created_by'    => Auth::User()->username,
        ]);
    }

    public function edit(User $user)
    {
        return $user;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|max:50',
            'username'  => 'required|string|max:50|unique:users,username,'.$request->user_id.',user_id',
            'email'     => 'required|string|email|max:50|unique:users,email,'.$request->user_id.',user_id',
            'user_role' => 'required|max:50',
            'mobile_no' => 'required|max:13|regex:/[0-9]{10}/',
            'address'   => 'required|max:2550',
            'branch_id' => 'required|numeric',
        ]);

        User::where('user_id',$request->user_id)->update([
            'branch_id' => $request->branch_id,
            'name'      => $request->name,
            'username'  => $request->username,
            'email'     => $request->email,
            'user_role' => $request->user_role,
            'mobile_no' => $request->mobile_no,
            'address'   => $request->address,
        ]);
    }

    public function destroy(User $user)
    {
        User::where('user_id',$user->user_id)->delete();
    }

    public function get_users(Request $request)
    {
        switch ($request->type) {
            case 'employees':
                return User::where('user_role','Employee')->get();
                break;
            default:
                return User::where('user_id',Auth::user()->user_id)->first();
                break;
        }
        
    }

    public function update_profile(Request $request)
    {
        $this->validate($request, [
            'name'      => 'required|max:50',
            'username'  => 'required|string|max:50|unique:users,username,'.$request->user_id.',user_id',
            'email'     => 'required|string|email|max:50|unique:users,email,'.$request->user_id.',user_id',
            'mobile_no' => 'required|max:13|regex:/[0-9]{10}/',
            'address'   => 'required|max:2550',
        ]);

        User::where('user_id',$request->user_id)->update([
            'branch_id' => $request['branch_id'],
            'name'      => $request['name'],
            'username'  => $request['username'],
            'email'     => $request['email'],
            'user_role' => $request['user_role'],
            'mobile_no' => $request['mobile_no'],
            'address'   => $request['address'],
            'updated_by'    => Auth::User()->username,         
        ]);

        if ($request->password !='' && $request->password_confirmation !='') {
           User::where('user_id',$request->user_id)->update([
                'password'      => bcrypt($request->password),
                'updated_by'    => Auth::User()->username,         
            ]);
        }

        $data = $request->avatar;
        
        if (strpos($data, 'base64') !== false) {
            $image_parts = explode(";base64,", $request->avatar);
            $image_base64 = base64_decode($image_parts[1]);
            $image_type_aux = explode("image/", $image_parts[0]);
            $png_url = date('Ymdhis').".".$image_type_aux[1];
            $path = public_path().'/storage/users/' . $png_url;
            file_put_contents($path, $image_base64);
            User::where('user_id',$request->user_id)->update([
                'avatar'    => $png_url,
            ]);
        }
        return User::where('user_id',$request->user_id)->first();
    }
}
