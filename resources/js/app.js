require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'

import moment from 'moment'
Vue.use(require('vue-moment'));
window.moment = require('moment');

// window.myModule = require('./modal');

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import vDialogs from 'v-dialogs';
Vue.use(vDialogs);


import { PaginationPlugin } from 'bootstrap-vue'
Vue.use(PaginationPlugin)

import { ModalPlugin } from 'bootstrap-vue';
Vue.use(ModalPlugin);

let Myheader = require('./components/Header.vue').default;
let Mymenu = require('./components/Menu.vue').default;
let Myfooter = require('./components/Footer.vue').default;

let Home = require('./components/Home.vue').default;
let Configuration = require('./components/Configuration.vue').default;
let Organization = require('./components/configuration/Organization.vue').default;
let Master = require('./components/configuration/Master.vue').default;
let Place = require('./components/configuration/Place.vue').default;
let Tax = require('./components/configuration/Tax.vue').default;
let Account = require('./components/configuration/Account.vue').default;
let Term = require('./components/configuration/Term.vue').default;
let PaymentTerm = require('./components/configuration/PaymentTerm.vue').default;
let Preference = require('./components/configuration/Preference.vue').default;
let Report = require('./components/Report.vue').default;

//Branch
let CreateBranch = require('./components/branch/Create.vue').default;
let DisplayBranch = require('./components/branch/Display.vue').default;

//Users
let CreateUser = require('./components/user/Create.vue').default;
let DisplayUser = require('./components/user/Display.vue').default;
let Profile = require('./components/user/Profile.vue').default;

//Contacts
let CreateContact = require('./components/contact/Create.vue').default;
let DisplayContact = require('./components/contact/Display.vue').default;
let ViewContact = require('./components/contact/View.vue').default;

//Product
let Category = require('./components/product/Category.vue').default;
let CreateProduct = require('./components/product/Create.vue').default;
let DisplayProduct = require('./components/product/Display.vue').default;
let ViewProduct = require('./components/product/View.vue').default;
let StockReport = require('./components/product/StockReport.vue').default;

//Expense
let CreateExpense = require('./components/expense/Create.vue').default;
let DisplayExpense = require('./components/expense/Display.vue').default;
let ViewExpense = require('./components/expense/View.vue').default;
let PreviewExpense = require('./components/expense/Preview.vue').default;
let ReportExpense = require('./components/expense/Report.vue').default;

//Income
let CreateIncome = require('./components/income/Create.vue').default;
let DisplayIncome = require('./components/income/Display.vue').default;
let ViewIncome = require('./components/income/View.vue').default;
let PreviewIncome = require('./components/income/Preview.vue').default;
let ReportIncome = require('./components/income/Report.vue').default;

//Bill
let CreateBill = require('./components/bill/Create.vue').default;
let DisplayBill = require('./components/bill/Display.vue').default;
let ViewBill = require('./components/bill/View.vue').default;
let PreviewBill = require('./components/bill/Preview.vue').default;
let ReportBill = require('./components/bill/Report.vue').default;
// let ReportBillTax = require('./components/bill/ReportBillTax.vue').default;

//Transfer
let CreateTransfer = require('./components/transfer/Create.vue').default;
let DisplayTransfer = require('./components/transfer/Display.vue').default;
let ViewTransfer = require('./components/transfer/View.vue').default;
let PreviewTransfer = require('./components/transfer/Preview.vue').default;
let ReportTransfer = require('./components/transfer/Report.vue').default;

//Invoice
let CreateInvoice = require('./components/invoice/Create.vue').default;
let DisplayInvoice = require('./components/invoice/Display.vue').default;
let ViewInvoice = require('./components/invoice/View.vue').default;
let PreviewInvoice = require('./components/invoice/Preview.vue').default;
let ReportInvoice = require('./components/invoice/Report.vue').default;

//Transfer
let CreateTransferReturn = require('./components/transfer_return/Create.vue').default;
let DisplayTransferReturn = require('./components/transfer_return/Display.vue').default;
let ViewTransferReturn = require('./components/transfer_return/View.vue').default;
let PreviewTransferReturn = require('./components/transfer_return/Preview.vue').default;
let ReportTransferReturn = require('./components/transfer_return/Report.vue').default;

const routes = [
	{ path: '/', component: Home, name: 'home'},
    { path: '/home', component: Home, name: 'home'},

    //Configuration
    { path: '/configuration', name:'configuration', component: Configuration },
    { path: '/organization', name:'organization', component: Organization },
    { path: '/master', name:'master', component: Master },
    { path: '/place', name:'place', component: Place },
    { path: '/tax', name:'tax', component: Tax },
    { path: '/account', name:'account', component: Account },
    { path: '/term', name:'term', component: Term },
    { path: '/payment_term', name:'payment_term', component: PaymentTerm },
    { path: '/preference', name:'preference', component: Preference },
    { path: '/report', name:'report', component: Report },

    //Branch
    { path: '/branch/create', name:'branch-create', component: CreateBranch },
    { path: '/branch/display', name:'branch-display', component: DisplayBranch },
    { path: '/branch/edit/:branch_id', name:'branch-edit', component:CreateBranch },

    //User
    { path: '/user/create', name:'user-create', component: CreateUser },
    { path: '/user/display', name:'user-display', component: DisplayUser },
    { path: '/user/edit/:user_id', name:'user-edit', component:CreateUser },
    { path: '/profile',component:Profile },

    //Contact
    { path: '/contact/create', name:'contact-create', component: CreateContact },
    { path: '/contact/display', name:'contact-display', component: DisplayContact },
    { path: '/contact/view/:contact_id', name:'contact-view', component: ViewContact }, 
    { path: '/contact/edit/:contact_id', name:'contact-edit', component:CreateContact },

    //Product
    { path: '/category', name:'category', component: Category },
    { path: '/product/create', name:'product-create', component: CreateProduct },
    { path: '/product/display', name:'product-display', component: DisplayProduct },
    { path: '/product/edit/:product_id', name:'product-edit', component:CreateProduct },
    { path: '/product/view/:product_id', name:'product-view', component: ViewProduct },
    { path: '/product/stock_report', name:'stock-report', component: StockReport },

    { path: '/expense/create', name:'expense-create', component: CreateExpense },
    { path: '/expense/display', name:'expense-display', component: DisplayExpense },
    { path: '/expense/view/:expense_id', name:'expense-view', component: ViewExpense },
    { path: '/expense/edit/:expense_id', name:'expense-edit', component: CreateExpense },
    { path: '/expense/preview/:expense_id', name:'expense-preview', component:PreviewExpense },
    //{ path: '/expense/report', name:'expense-report', component: ReportExpense },

    { path: '/income/create', name:'income-create', component: CreateIncome },
    { path: '/income/display', name:'income-display', component: DisplayIncome },
    { path: '/income/view/:income_id', name:'income-view', component: ViewIncome },
    { path: '/income/edit/:income_id', name:'income-edit', component: CreateIncome },
    { path: '/income/preview/:income_id', name:'income-preview', component:PreviewIncome },
    //{ path: '/income/report', name:'income-report', component: ReportIncome }, 

    { path: '/bill/create', name:'bill-create', component: CreateBill },
    { path: '/bill/display', name:'bill-display', component: DisplayBill },
    { path: '/bill/view/:bill_id', name:'bill-view', component: ViewBill },
    { path: '/bill/edit/:bill_id', name:'bill-edit', component: CreateBill },
    { path: '/bill/preview/:bill_id', name:'bill-preview', component:PreviewBill },
    { path: '/bill/report', name:'bill-report', component: ReportBill },
    //{ path: '/bill/tax_report', name:'bill-tax-report', component: ReportBillTax },
    //{ path: '/purchase_order/convert/bill/', name:'purchase-order-convert-bill', component: CreateBill },
    
    { path: '/transfer/create', name:'transfer-create', component: CreateTransfer },
    { path: '/transfer/display', name:'transfer-display', component: DisplayTransfer },
    { path: '/transfer/view/:transfer_id', name:'transfer-view', component: ViewTransfer },
    { path: '/transfer/edit/:transfer_id', name:'transfer-edit', component: CreateTransfer },
    { path: '/transfer/preview/:transfer_id', name:'transfer-preview', component:PreviewTransfer },
    { path: '/transfer/report', name:'transfer-report', component: ReportTransfer },

    { path: '/invoice/create', name:'invoice-create', component: CreateInvoice },
    { path: '/invoice/display', name:'invoice-display', component: DisplayInvoice },
    { path: '/invoice/view/:invoice_id', name:'invoice-view', component: ViewInvoice },
    { path: '/invoice/edit/:invoice_id', name:'invoice-edit', component: CreateInvoice },
    { path: '/invoice/preview/:invoice_id', name:'invoice-preview', component:PreviewInvoice },
    { path: '/invoice/report', name:'invoice-report', component: ReportInvoice },
    // { path: '/invoice/convert/delivery/', name:'invoice-convert-delivery', component: CreateDelivery },

    { path: '/transfer_return/create', name:'transfer-return-create', component: CreateTransferReturn },
    { path: '/transfer_return/display', name:'transfer-return-display', component: DisplayTransferReturn },
    { path: '/transfer_return/view/:transfer_return_id', name:'transfer-return-view', component: ViewTransferReturn },
    { path: '/transfer_return/edit/:transfer_return_id', name:'transfer-return-edit', component: CreateTransferReturn },
    { path: '/transfer_return/preview/:transfer_return_id', name:'transfer-return-preview', component:PreviewTransferReturn },
    { path: '/transfer_return/report', name:'transfer-return-report', component: ReportTransferReturn },
];

const router = new VueRouter({
    // mode: 'history',
    routes
});

const app = new Vue({
    el: '#app',
    router,
    components: {Myheader, Mymenu, Myfooter},
    methods:{
        windowreload(){
            location.reload(true);
        }
    }
});
