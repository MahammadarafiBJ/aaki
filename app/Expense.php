<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expense extends Model
{
    use SoftDeletes;

    // protected $fillable = [
    //     'fiscal_year','voucher_no','voucher_date','account_id','contact_id','payment_mode','reference_no','reference_date','amount','term_id','terms','note','created_by','updated_by'
    // ];

    protected $guarded = [];

    protected $primaryKey = 'expense_id';

    protected $dates = ['deleted_at'];

    public function Contact()
    {
    	return $this->hasOne('App\Contact','contact_id','contact_id')->withTrashed();
    }

    public function Account()
    {
    	return $this->hasOne('App\Account','account_id','account_id')->withTrashed();
    }
    public function Term()
    {
        return $this->hasOne('App\Term','term_id','term_id')->withTrashed();
    }

    public function Branch()
    {
        return $this->hasOne('App\Branch','branch_id','branch_id')->withTrashed();
    }
}
