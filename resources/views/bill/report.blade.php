@php
    if($request->display_type=='excel')
    {
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=bill-report.xls");
    }
@endphp
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/report.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <table border="1">
        <thead>
            <tr>
                <th colspan="12" class="text-center">PURCHASE BILL REPORT</th>
            </tr>
            <tr>
                <td colspan="12">
                    <div>
                        <strong>{{ $org->org_name }}</strong>
                    </div>
                    <div>{{ $org->address }}</div>
                    <div>Email: {{ $org->email }}</div>
                    <div>Phone: {{ $org->mobile_no }}</div>
                    <div>PAN: {{ $org->pan_no }}</div>
                    <div>GSTIN: {{ $org->gstin_no }}</div>
                </td>
            </tr>
        </thead>
        <tbody>
        @if($request->report_type == 'Brief Report')
            <tr>
                <th class="text-center">#</th>
                <th class="text-nowrap">Purchase Bill No</th>
                <th class="text-nowrap">Purchase Bill Date</th>
                <th class="text-right" colspan="2">Sub Total</th>
                @if($preference->discount_on=='Inc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right" colspan="2">Discount</th>
                @if($preference->discount_on=='Exc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Total Amount</th>
                <th class="text-right">Round off</th>
                <th class="text-right" colspan="2">Grand Total</th>
            </tr>
            @php
                $sub_total = 0;
                $discount_amount = 0;
                $tax_amount = 0;
                $total_amount = 0;
                $round_off = 0;
                $grand_total = 0;
            @endphp
            @foreach($bills as $key => $bill)
            <tr>
                <td class="text-center">
                        {{ $key+1 }}
                    </td>
                    <td>
                        {{ $bill->bill_no }}
                    </td>
                    <td>
                        {{ date('d-m-Y',strtotime($bill->bill_date)) }}
                    </td>
                    <td class="text-right" colspan="2">
                        {{ number_format($bill->sub_total,2) }}
                    </td>
                    @if($preference->discount_on=='Inc')
                    <td class="text-right">
                        {{ number_format($bill->tax_amount,2) }}
                    </td>
                    @endif
                    <td class="text-right" colspan="2">
                        {{ number_format($bill->discount_amount,2) }}
                    </td>
                    @if($preference->discount_on=='Exc')
                    <td class="text-right">
                        {{ number_format($bill->tax_amount,2) }}
                    </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($bill->total_amount,2) }}
                    </td>
                    <td class="text-right">
                        {{ number_format($bill->round_off,2) }}
                    </td>
                    <td class="text-right" colspan="2">
                        {{ number_format($bill->grand_total,2) }}
                    </td>
                    @php
                        $sub_total += $bill->sub_total;
                        $discount_amount += $bill->discount_amount;
                        $tax_amount += $bill->tax_amount;
                        $total_amount += $bill->total_amount;
                        $round_off += $bill->round_off;
                        $grand_total += $bill->grand_total;
                    @endphp
            </tr>
            @endforeach
            <tr>
                <th colspan="3" class="text-right">Total</th>
                <th class="text-right" colspan="2">{{ number_format($sub_total,2) }}</th>
                @if($preference->discount_on=='Inc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right" colspan="2">{{ number_format($discount_amount,2) }}</th>
                @if($preference->discount_on=='Exc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($total_amount,2) }}</th>
                <th class="text-right">{{ number_format($round_off,2) }}</th>
                <th class="text-right" colspan="2">{{ number_format($grand_total,2) }}</th>
            </tr>
        @endif
        @if($request->report_type == 'Detail Report')
            @foreach($bills as $key=> $bill)
                <tr>
                    <th colspan="12">{{ $key+1 }}</th>
                </tr>
                <tr>
                    <td colspan="12">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div><b>Purchase Bill No : </b>{{ $bill->bill_no }}</div>
                        <div><b>Purchase Bill Date : </b>{{ date('d-m-Y',strtotime($bill->bill_date)) }}</div>
                        <div><b>Ref No  : </b>{{ $bill->reference_no }}</div>
                        <div><b>Ref Date  : </b>{{ date('d-m-Y',strtotime($bill->reference_date)) }}</div> 
                    </td>
                </tr>
                <tr>
                    <th class="text-center">#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th class="text-right">P.Rate(Exc)</th>
                    <th class="text-right">P.Rate(Inc)</th>
                    @if($preference->discount_on=='Inc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right" >Discount</th>
                    @if($preference->discount_on=='Exc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Qty</th>
                    <th class="text-right">Amount</th>
                </tr>
                @foreach($bill->BillProducts as $val=> $bill_product)
                    <tr>
                        <td class="text-center">
                             {{ $val+1 }} 
                        </td>
                        <td>
                            {{ $bill_product->product_code }}
                        </td>
                        <td>
                            {{ $bill_product->product_name }}
                        </td>
                        <td class="text-right">
                            {{ number_format($bill_product->purchase_rate_exc,2) }}
                        </td>
                        <td class="text-right">
                           {{ number_format($bill_product->purchase_rate_inc,2) }}
                        </td>
                        @if($preference->discount_on=='Inc')
                        <td class="text-right">
                            {{ number_format($bill_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td class="text-right">
                            {{ number_format($bill_product->discount_amount,2) }}
                        </td>
                        @if($preference->discount_on=='Exc')
                        <td class="text-right">
                            {{ number_format($bill_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td class="text-right">
                            {{ number_format($bill_product->quantity,2) }}
                        </td>
                        <td class="text-right">
                            {{ number_format($bill_product->amount,2) }}
                        </td>                     
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Terms and Conditions : </strong><br>
                        {{ $bill->terms }}
                    </td>
                    <th class="text-right" colspan="2">Sub Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($bill->sub_total,2) }}
                    </th>
                </tr>
                @if($preference->discount_on=='Inc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($bill->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <th class="text-right" colspan="2">Discount</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($bill->discount_amount,2) }}
                    </th>
                </tr>
                @if($preference->discount_on=='Exc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($bill->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Note : </strong><br>
                        {{ $bill->note  }}
                    </td>
                    <th class="text-right" colspan="2">Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($bill->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Round off</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($bill->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Grand Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($bill->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</body>
</html>