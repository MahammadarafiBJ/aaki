<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('product_id');
            $table->string('product_type',50);
            $table->string('product_code',50);
            $table->string('hsn_code',50)->nullable();
            $table->bigInteger('category_id')->unsigned();
            $table->longtext('product_name');
            $table->longtext('description')->nullable();
            $table->string('product_unit',50)->default('No');
            $table->double('product_size',15,2)->nullable();
            $table->double('min_stock',15,2)->default(2);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('category_id')->references('category_id')->on('categories');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
