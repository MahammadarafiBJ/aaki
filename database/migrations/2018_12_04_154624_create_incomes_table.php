<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incomes', function (Blueprint $table) {
            $table->bigIncrements('income_id');
            $table->string('fiscal_year',50);
            $table->string('receipt_no',50);
            $table->date('receipt_date');
            $table->bigInteger('account_id')->unsigned();
            $table->bigInteger('contact_id')->unsigned();
            $table->string('payment_mode',50)->default('Cash');
            $table->string('reference_no',50)->nullable();
            $table->date('reference_date')->nullable();
            $table->double('amount',15,2);
            $table->bigInteger('term_id')->unsigned()->nullable();
            $table->longtext('terms')->nullable();
            $table->longtext('note')->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('contact_id')->references('contact_id')->on('contacts');
            $table->foreign('account_id')->references('account_id')->on('accounts');
            $table->foreign('term_id')->references('term_id')->on('terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incomes');
    }
}
