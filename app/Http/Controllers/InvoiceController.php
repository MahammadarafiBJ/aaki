<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use PDF;
use App\Invoice;
use App\InvoiceProduct;
use App\Preference;

use App\Organization;
use App\Contact;
use App\Product;
use App\Price;

use App\Tax;

use App\Account;
use App\PaymentTerm;
use App\Master;
use App\Branch;

use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;

use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

use Mike42\Escpos\PrintConnectors\FilePrintConnector;

use Mike42\Escpos\Printer;

use App\Module\Module;


class InvoiceController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_invoice_no(Request $request)
    {
        $preference = Preference::first();
        $id = Invoice::where('fiscal_year',$preference->fiscal_year)->max('invoice_id');
        $id = $id + 1;
        $n = $preference->invoice_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $invoice_no = $preference->invoice_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $invoice_no;
    }

    public function display(Request $request)
    {
        $preference = Preference::first();

        if (Auth::User()->user_role == 'Super Admin' && $preference->branch_id == 1) {
            return Invoice::whereHas('Customer', function($query) use($request){
                            $query->where('contact_name','like', "%$request->search%");
                         })
                        ->orWhere('invoice_no', 'like', '%'.$request->search.'%')
                        ->orWhere('invoice_date', 'like', '%'.$request->search.'%')
                        ->orWhere('sub_total', 'like', '%'.$request->search.'%')
                        ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
                        ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
                        ->orWhere('grand_total', 'like', '%'.$request->search.'%')
                        ->with('Customer','Branch')
                        ->orderBy('invoice_id','DESC')
                        ->paginate(10);
        }else{
            return Invoice::where('branch_id',Auth()->User()->branch_id)
                          ->where(function($que) use($request){
                                $que->whereHas('Customer', function($query) use($request){
                                        $query->where('contact_name','like', "%$request->search%");
                                    })
                                    ->orWhere('invoice_no', 'like', '%'.$request->search.'%')
                                    ->orWhere('invoice_date', 'like', '%'.$request->search.'%')
                                    ->orWhere('sub_total', 'like', '%'.$request->search.'%')
                                    ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
                                    ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
                                    ->orWhere('grand_total', 'like', '%'.$request->search.'%');
                          })->with('Customer','Branch')
                          ->orderBy('invoice_id','DESC')
                          ->paginate(10);
        }
    }


    public function validation(Request $request)
    {
        $this->validate($request, [
            'product_id'        => 'required|numeric',
            'product_code'      => 'required|max:50',
            'product_name'      => 'required|max:2550',
            'price_id'          => 'required|numeric',
            'barcode'           => 'sometimes',
            'sales_rate_exc'    => 'required|numeric',
            'sales_rate_inc'    => 'required|numeric',
            'discount'          => 'required|numeric',
            'discount_type'     => 'required|max:50',
            'discount_amount'   => 'required|numeric',
            'tax_id'            => 'required|numeric',
            'tax_amount'        => 'required|numeric',
            'quantity'          => 'required|numeric|not_in:0',
            'amount'            => 'required|numeric',
            'sub_total'         => 'required|numeric',
        ]);
        return $request;
    }

    public function store(Request $request)
    {
       
        $this->validate($request, [
            'invoice_no'        => 'required|max:50',
            'invoice_date'      => 'required|date',
            'invoice_type'      => 'required|max:50',
            'reference_no'      => 'max:50',
            'reference_date'    => 'sometimes|nullable|date',
            'customer_id'       => 'required|numeric',
            'source_id'         => 'required|numeric',
            'destination_id'    => 'required|numeric',
            'sub_total'         => 'required|numeric',
            'discount_amount'   => 'required|numeric',
            'tax_amount'        => 'required|numeric',
            'round_off'         => 'required|numeric',
            'total_amount'      => 'required|numeric',
            'grand_total'       => 'required|numeric',
            'payable_amount'    => 'required|numeric',
            'term_id'           => 'sometimes|nullable|numeric',
            'terms'             => 'max:2550',
            'note'              => 'max:2550',
            'due_date'          => 'required|date',
            'payment_term_id'   => 'required|numeric',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();

        $invoice = Invoice::create([
            'fiscal_year'       => $preference->fiscal_year,
            'invoice_no'        => $request->invoice_no,
            'invoice_date'      => date("Y-m-d", strtotime($request->invoice_date)),
            'due_date'          => date("Y-m-d", strtotime($request->due_date)),
            'invoice_type'      => $request->invoice_type,
            'reference_no'      => $request->reference_no,
            'reference_date'    => $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : NULL,
            'customer_id'       => $request->customer_id,
            'branch_id'         => Auth::User()->branch_id,
            'billing_address'   => $contact->billing_address,
            'shipping_address'  => $contact->shipping_address,
            'source_id'         => $request->source_id,
            'payment_term_id'   => $request->payment_term_id,
            'destination_id'    => $request->destination_id,
            'sub_total'         => $request->sub_total,
            'discount_amount'   => $request->discount_amount,
            'tax_amount'        => $request->tax_amount,
            'total_amount'      => $request->total_amount,
            'round_off'         => $request->round_off,
            'grand_total'       => $request->grand_total,
            'card_discount_amount'=> $request->card_discount_amount,
            'payable_amount'    => $request->payable_amount,
            'term_id'           => $request->term_id,
            'terms'             => $request->terms,
            'note'              => $request->note,
            'invoice_status'    => 'Open',
            'created_by'        => Auth::User()->username,
        ]);

        foreach ($request->invoice_products as $invoice_product) 
        {
            $product = Product::where('product_id',$invoice_product['product_id'])->first();
            $price = Price::where('price_id',$invoice_product['price_id'])->first();

            $invoice_product_up = InvoiceProduct::create([
                'invoice_id'        => $invoice->invoice_id,
                'product_id'        => $invoice_product['product_id'],
                'product_type'      => $product->product_type,
                'product_code'      => $invoice_product['product_code'],
                'hsn_code'          => $product->hsn_code,
                'category_id'       => $product->category_id,
                'product_name'      => $invoice_product['product_name'],
                'description'       => $product->description,
                'product_unit'      => $product->product_unit,
                'price_id'          => $invoice_product['price_id'],
                'barcode'           => $price->barcode,
                'purchase_rate_exc' => $price->purchase_rate_exc,
                'sales_rate_exc'    => $invoice_product['sales_rate_exc'],
                'purchase_rate_inc' => $price->purchase_rate_inc,
                'sales_rate_inc'    => $invoice_product['sales_rate_inc'],
                'quantity'          => $invoice_product['quantity'],
                'amount'            => $invoice_product['amount'],
                'discount'          => $invoice_product['discount'],
                'discount_type'     => $invoice_product['discount_type'],
                'discount_amount'   => $invoice_product['discount_amount'],
                'tax_id'            => $invoice_product['tax_id'],
                'tax_amount'        => $invoice_product['tax_amount'],
                'sub_total'         => $invoice_product['sub_total'],
                'created_by'        => Auth::User()->username,
            ]);
        }
        
        $module = new Module;
        $message  = 'Thank you for Shopping with AAKI. Bill No='.$invoice->invoice_no.', Bill Date='.date('d-m-Y',strtotime($invoice->invoice_date)).', Amount='.$invoice->grand_total.'.';

        $module->new_message($invoice->Customer->mobile_no,$message);

        // $this->print($invoice->invoice_id);
        
        return $invoice;
    }

    public function send_message(Invoice $invoice)
    {
        $module = new Module;
        $message  = 'Thank you for Shopping with AAKI. Bill No='.$invoice->invoice_no.', Bill Date='.date('d-m-Y',strtotime($invoice->invoice_date)).', Amount='.$invoice->grand_total.'.';

        $module->new_message($invoice->Customer->mobile_no,$message);
        
        return $invoice;
    }

    // public function print($invoice_id)
    // {
    //     $invoice = Invoice::where('invoice_id',$invoice_id)->first();
    //     $branch = Branch::where('branch_id',$invoice->branch_id)->first();
    //     $preference = Preference::first();
    //     $org = Organization::first();
    //     try 
    //     {
    //         if ($preference->printer_type == 'USB') {
    //             $connector = new WindowsPrintConnector($preference->printer_ip);
    //         }
    //         if ($preference->printer_type == 'LAN') {
    //             $connector = new NetworkPrintConnector($preference->printer_ip,'9100');
    //         }        
    //         $printer = new Printer($connector);

    //         $printer->setJustification(Printer::JUSTIFY_CENTER);
    //         $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);

    //         $printer->text($org->org_name."\n");
    //         $printer->selectPrintMode(Printer::MODE_FONT_A);
    //         if($org->gstin!="")
    //         {
    //             $printer->text("GSTIN:".$org->gstin."\n");
    //         }
    //         $printer->text($branch->address."\n");
    //         $printer->text("Ph No : ".$branch->mobile_no."\n");
    //         $printer->text("  ----------------------------------------------\n");
    //         $printer->text(" Tax Invoice"."\n");
    //         $printer->text("  ----------------------------------------------\n");
    //         $printer->setJustification(Printer::JUSTIFY_LEFT);
    //         $customer = $invoice->Customer->contact_name;
    //         $printer->text("  Customer     : " .$customer."\n");
    //         $printer->text("  Invoice No   : " .$invoice->invoice_no."\n");
    //         $printer->text("  Invoice Date : " .date('d-m-Y',strtotime($invoice->invoice_date))."\n");
            
    //         $printer->text("  ----------------------------------------------\n");
    //         $line = sprintf('  %-26.26s %3.3s %7.7s %7.7s', 'Item', 'Qty', 'Rate', 'Amount');
    //         $printer->text($line."\n");
    //         $printer->text("  ----------------------------------------------\n");

    //         $total = 0;
    //         $final_discount = 0; 
    //         $sub_total=0;
            
    //         foreach($invoice->InvoiceProducts as $invoice_product)
    //         {
    //             $price_total= round(($invoice_product->sales_rate_exc * $invoice_product->quantity),2);

    //             $cgst = ($invoice_product->tax_amount)/2;
    //             $sgst = ($invoice_product->tax_amount)/2;
    //             $cgst_per = ($invoice_product->Tax->cgst_rate);
    //             $sgst_per = ($invoice_product->Tax->sgst_rate);
                

    //             $line = sprintf('  %-25.25s %3.2f %7.2f %7.2f',$invoice_product->product_name, $invoice_product->quantity, $invoice_product->sales_rate_exc, $invoice_product->amount);
    //             $printer->text($line."\n");

                
    //             $total += $price_total;
    //             $final_discount += $invoice_product->discount_amount;
    //             $sub_total += ($invoice_product->sales_rate_exc * $invoice_product->quantity);
    //         }
    //         $printer->text("  ----------------------------------------------\n");
    //         $printer->setJustification(Printer::JUSTIFY_RIGHT);
    //         $line = sprintf("%33s %7.2f", "Sub Total  : ", $invoice->sub_total);
    //         $printer->text($line."\n");
            
    //         foreach($invoice->Taxes($invoice->invoice_id) as $tax)
    //         {
    //             $taxable_value = $invoice->TaxableValue($invoice->invoice_id,$tax->tax_id);
    //             $cgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);
    //             $sgst = round(($taxable_value * $tax->Tax->cgst_rate)/100,2);

    //             if($tax->Tax->cgst_rate !='0' && $tax->Tax->cgst_rate !='0')
    //             {
    //                 $line = sprintf("%22s %0.2f %0.4s %7.2f", "CGST @ ", $tax->Tax->cgst_name ,"% : " ,$cgst);
    //                 $printer->text($line."\n");
    //                 $line = sprintf("%22s %0.2f %0.4s %7.2f", "SGST @ ", $tax->Tax->sgst_name ,"% : " ,$sgst);
    //                 $printer->text($line."\n");
    //             }
    //         }

    //         if(!$invoice->discount_amount == 0)
    //         {
    //             $line = sprintf("%33s %7.2f", "Discount  : ", $invoice->discount_amount);
    //             $printer->text($line."\n");
    //         }

    //         if(!$invoice->round_off == 0)
    //         {
    //             $line = sprintf("%33s %7.2f", "Round Off  : ", $invoice->round_off);
    //             $printer->text($line."\n");
    //         }
    //         if(!$invoice->points == 0)
    //         {
    //             $printer->text("  ----------------------------------------------\n");
    //             $line = sprintf("%33s %7.2f", "Total  : ", $invoice->grand_total);
    //             $printer->text($line."\n");

    //             $line = sprintf("%33s %7.2f", "Points  : ", $invoice->points);
    //             $printer->text($line."\n");
    //         }
    //         $printer->text("  ----------------------------------------------\n");
    //         $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    //         $printer->text("Grand Total : ". number_format($invoice->payable_amount,2)."\n");
    //         $printer->selectPrintMode(Printer::MODE_FONT_A);
    //         $printer->text("  ----------------------------------------------\n");
    //         $printer->selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
    //         $printer->setJustification(Printer::JUSTIFY_CENTER);
    //         $printer->text(" THANK YOU VISIT AGAIN "."\n");
    //     }
    //     finally 
    //     {
    //         $printer->cut();
    //         $printer->close();
    //     }
    // }


    public function view(Invoice $invoice)
    {
        $invoice = Invoice::where('invoice_id',$invoice->invoice_id)->with(['InvoiceProducts','Customer'])->first();
        $pre = Preference::first();

        $invoice->InvoiceProducts->each(function($trans_p,$key){
            $price = Price::where('price_id',$trans_p->price_id)->first();
            if ($price) {
                $trans_p->current_stock = $price->CurrentStock($price->price_id);
            }
        }); 

        return $invoice;
    }

    public function destroy(Invoice $invoice)
    {
        InvoiceProduct::where('invoice_id',$invoice->invoice_id)->forceDelete();
        Invoice::where('invoice_id',$invoice->invoice_id)->forceDelete();

    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'invoice_no'        => 'required|max:50',
            'invoice_date'      => 'required|date',
            'invoice_type'      => 'required|max:50',
            'reference_no'      => 'max:50',
            'reference_date'    => 'sometimes|nullable|date',
            'customer_id'       => 'required|numeric',
            'source_id'         => 'required|numeric',
            'destination_id'    => 'required|numeric',
            'sub_total'         => 'required|numeric',
            'discount_amount'   => 'required|numeric',
            'tax_amount'        => 'required|numeric',
            'round_off'         => 'required|numeric',
            'total_amount'      => 'required|numeric',
            'grand_total'       => 'required|numeric',
            'payable_amount'    => 'required|numeric',
            'term_id'           => 'sometimes|nullable|numeric',
            'terms'             => 'max:2550',
            'note'              => 'max:2550',
            'due_date'          => 'required|date',
            'payment_term_id'   => 'required|numeric',
        ]);

        $preference = Preference::first();
        $contact = Contact::where('contact_id',$request->customer_id)->first();
        $invoice = Invoice::where('invoice_id',$request->invoice_id)->first();
        $invoice_products = Invoice::where('invoice_id',$request->invoice_id)->get();

        Invoice::where('invoice_id',$request->invoice_id)->update([
            'fiscal_year'       => $invoice->fiscal_year,
            'invoice_no'        => $request->invoice_no,
            'invoice_type'      => $request->invoice_type,
            'invoice_date'      => date("Y-m-d", strtotime($request->invoice_date)),
            'due_date'          => date("Y-m-d", strtotime($request->due_date)),
            'reference_no'      => $request->reference_no,
            'reference_date'    => $request->reference_date ? date('Y-m-d',strtotime($request->reference_date)) : $invoice->reference_date,
            'branch_id'         => Auth::User()->branch_id,
            'customer_id'       => $request->customer_id,
            'billing_address'   => $contact->billing_address,
            'shipping_address'  => $contact->shipping_address,
            'source_id'         => $request->source_id,
            'payment_term_id'   => $request->payment_term_id,
            'destination_id'    => $request->destination_id,
            'sub_total'         => $request->sub_total,
            'discount_amount'   => $request->discount_amount,
            'tax_amount'        => $request->tax_amount,
            'total_amount'      => $request->total_amount,
            'round_off'         => $request->round_off,
            'grand_total'       => $request->grand_total,
            'card_discount_amount'=> $request->card_discount_amount,
            'payable_amount'    => $request->payable_amount,
            'term_id'           => $request->term_id,
            'terms'             => $request->terms,
            'note'              => $request->note,
            'updated_by'        => Auth::User()->username,
        ]);

        foreach ($request->deleted_invoice_products as $invoice_product) 
        {
            InvoiceProduct::where('invoice_product_id',$invoice_product['invoice_product_id'])->delete();
        }

        foreach ($request->invoice_products as $invoice_product) 
        {
            $product = Product::where('product_id',$invoice_product['product_id'])->first();
            $price = Price::where('price_id',$invoice_product['price_id'])->first();

            if(empty($invoice_product['invoice_product_id'])) {
                InvoiceProduct::create([
                    'invoice_id'        => $invoice->invoice_id,
                    'product_id'        => $invoice_product['product_id'],
                    'product_type'      => $product->product_type,
                    'product_code'      => $invoice_product['product_code'],
                    'hsn_code'          => $product->hsn_code,
                    'category_id'       => $product->category_id,
                    'product_name'      => $invoice_product['product_name'],
                    'description'       => $product->description,
                    'product_unit'      => $product->product_unit,
                    'price_id'          => $invoice_product['price_id'],
                    'barcode'           => $price->barcode,
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc'    => $invoice_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc'    => $invoice_product['sales_rate_inc'],
                    'quantity'          => $invoice_product['quantity'],
                    'amount'            => $invoice_product['amount'],
                    'discount'          => $invoice_product['discount'],
                    'discount_type'     => $invoice_product['discount_type'],
                    'discount_amount'   => $invoice_product['discount_amount'],
                    'tax_id'            => $invoice_product['tax_id'],
                    'tax_amount'        => $invoice_product['tax_amount'],
                    'sub_total'         => $invoice_product['sub_total'],
                    'created_by'        => Auth::User()->username,
                ]);
            }
            else
            {
                InvoiceProduct::where('invoice_product_id',$invoice_product['invoice_product_id'])->update([
                    'invoice_id'        => $invoice->invoice_id,
                    'product_id'        => $invoice_product['product_id'],
                    'product_type'      => $product->product_type,
                    'product_code'      => $invoice_product['product_code'],
                    'hsn_code'          => $product->hsn_code,
                    'category_id'       => $product->category_id,
                    'product_name'      => $invoice_product['product_name'],
                    'description'       => $product->description,
                    'product_unit'      => $product->product_unit,
                    'price_id'          => $invoice_product['price_id'],
                    'barcode'           => $price->barcode,
                    'purchase_rate_exc' => $price->purchase_rate_exc,
                    'sales_rate_exc'    => $invoice_product['sales_rate_exc'],
                    'purchase_rate_inc' => $price->purchase_rate_inc,
                    'sales_rate_inc'    => $invoice_product['sales_rate_inc'],
                    'quantity'          => $invoice_product['quantity'],
                    'amount'            => $invoice_product['amount'],
                    'discount'          => $invoice_product['discount'],
                    'discount_type'     => $invoice_product['discount_type'],
                    'discount_amount'   => $invoice_product['discount_amount'],
                    'tax_id'            => $invoice_product['tax_id'],
                    'tax_amount'        => $invoice_product['tax_amount'],
                    'sub_total'         => $invoice_product['sub_total'],
                    'updated_by'        => Auth::User()->username,
                ]);
            }
        }
    }

    public function pdf(Invoice $invoice)
    {
        $org = Organization::first();
        $branch = Branch::where('branch_id',$invoice->branch_id)->first();
        $preference = Preference::first();
        $distinct_taxes = InvoiceProduct::where('invoice_id',$invoice->invoice_id)->distinct()->get('tax_id');
        PDF::loadView('invoice.small_pdf', compact('org','branch','preference','invoice','distinct_taxes'), [], [
            'margin_top' => 5,
            'margin_left' => 5,
            'margin_right' => 36,
            'margin_bottom' => 5,
            'format' =>'A',
        ])->stream($invoice->invoice_no.'.pdf');
    }

    public function report(Request $request)
    {
        $this->validate($request, [
            'from_date' => 'required|date',
            'to_date'   => 'required|date',
        ]);

        $from_date = Date('Y-m-d',strtotime($request->from_date));
        $to_date = Date('Y-m-d',strtotime($request->to_date));
        $org = Organization::first();
        $preference = Preference::first();
    
        $invoices = Invoice::whereBetween('invoice_date',[$from_date,$to_date])->get();
        
        if ($request->customer_id != '') {
            $invoices = $invoices->filter(function($value) use($request){
                return ($value->customer_id == $request->customer_id);
            })->values();
        }

        if ($request->branch_id != '') {
            $invoices = $invoices->filter(function($value) use($request){
                return ($value->branch_id == $request->branch_id);
            })->values();
        }

        switch ($request->display_type) {
            case 'pdf':
                PDF::loadView('invoice.report', compact('org','preference','invoices','request'), [], [
                'margin_top' => 10
                ])->stream('invoice-report.pdf');
                break;
            case 'excel':
                return view('invoice.report',compact('org','preference','invoices','request'));
                break;
            
            default:
                return view('invoice.report',compact('org','preference','invoices','request'));
                break;
        }
    }

    // public function tax_report(Request $request)
    // {
    //     $org = Organization::first();
    //     $invoices = Invoice::
    //         whereBetween('invoice_date', [date('Y-m-d',strtotime($request->from_date)), date('Y-m-d',strtotime($request->to_date))])->with(['InvoiceTax','Customer'])->get();

    //     if($request->display_type=='display')
    //     {
    //         return $invoices;
    //     }
    //     else if($request->display_type=='pdf')
    //     {
    //           PDF::loadView('invoice.gst_report', compact('invoices','org','request'), [], [
    //                 'margin_top' => 10
    //             ])->stream('invoice-gst-report.pdf');
    //     }
    //     else
    //     {
    //         return view('invoice.gst_report', compact('invoices','org','request'));
    //     }
    // }
}
