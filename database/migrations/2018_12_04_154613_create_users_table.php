<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('user_id');
            $table->bigInteger('branch_id')->unsigned();
            $table->string('name',50);
            $table->string('username',50)->unique();
            $table->string('email',50)->unique();
            $table->string('password',255);
            $table->string('user_role',50);
            $table->string('mobile_no', 20);
            $table->text('address');
            $table->string('avatar',50)->default('avatar.png');
            $table->rememberToken();
            $table->text('player_id')->nullable();
            $table->text('push_token')->nullable();
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('branch_id')->references('branch_id')->on('branches');
        });

         DB::table('users')->insert(
            [
                [
                    'branch_id' => 1,
                    'name' => 'MDBS Tech Private Limited',
                    'username' => 'mdbstech',
                    'email' => 'mdbstech@gmail.com',
                    'password' => Hash::make('qwerty'),
                    'user_role' => 'Super Admin',
                    'mobile_no' => '9535342875',
                    'address' => '#2738/12, 3rd Cross, Kalidasa Road, Mysore - 570020',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
