@php
    if($request->display_type=='excel')
    {
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;Filename=invoice-report.xls");
    }
@endphp
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ asset('css/report.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <table border="1">
        <thead>
            <tr>
                <th colspan="13" class="text-center">INVOICE REPORT</th>
            </tr>
            <tr>
                <td colspan="13">
                    <div>
                        <strong>{{ $org->org_name }}</strong>
                    </div>
                    <div>{{ $org->address }}</div>
                    <div>Email: {{ $org->email }}</div>
                    <div>Phone: {{ $org->mobile_no }}</div>
                    <div>PAN: {{ $org->pan_no }}</div>
                    <div>GSTIN: {{ $org->gstin_no }}</div>
                </td>
            </tr>
        </thead>
        <tbody>
        @if($request->report_type == 'Brief Report')
            <tr>
                <th class="text-center">#</th>
                <th>Invoice Type</th>
                <th>Invoice No</th>
                <th>Invoice Date</th>
                <th class="text-nowrap">Branch</th>
                <th>Customer</th>
                <th>Payment Term</th>
                <th class="text-right">Sub Total</th>
                @if($preference->discount_on == 'Inc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Discount</th>
                @if($preference->discount_on == 'Exc')
                <th class="text-right">Tax</th>
                @endif
                <th class="text-right">Total Amount</th>
                <th class="text-right">Round off</th>
                <th class="text-right">Grand Total</th>
            </tr>
            @php
                $sub_total = 0;
                $discount_amount = 0;
                $tax_amount = 0;
                $total_amount = 0;
                $round_off = 0;
                $grand_total = 0;
            @endphp
            @foreach($invoices as $key => $invoice)
                <tr>
                    <td class="text-center">
                        {{ $key+1 }}
                    </td>
                    <td>
                        {{ $invoice->invoice_type }}
                    </td>
                    <td>
                        {{ $invoice->invoice_no }}
                    </td>
                    <td>
                        {{ date('d-m-Y',strtotime($invoice->invoice_date)) }}
                    </td>
                    <td>
                        {{ $invoice->Branch->branch_name }}
                    </td>
                    <td>
                        {{ $invoice->Customer->contact_name }}
                    </td>
                    <td>
                        {{ $invoice->PaymentTerm->payment_term }}
                    </td>
                    <td class="text-right">
                        {{ number_format($invoice->sub_total,2) }}
                    </td>
                    @if($preference->discount_on == 'Inc')
                    <td class="text-right">
                        {{ number_format($invoice->tax_amount,2) }}
                    </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($invoice->discount_amount,2) }}
                    </td>
                    @if($preference->discount_on == 'Exc')
                    <td class="text-right">
                        {{ number_format($invoice->tax_amount,2) }}
                    </td>
                    @endif
                    <td class="text-right">
                        {{ number_format($invoice->total_amount,2) }}
                    </td>
                    <td class="text-right">
                        {{ number_format($invoice->round_off,2) }}
                    </td>
                    <td class="text-right">
                        {{ number_format($invoice->grand_total,2) }}
                    </td>
                    @php
                        $sub_total += $invoice->sub_total;
                        $discount_amount += $invoice->discount_amount;
                        $tax_amount += $invoice->tax_amount;
                        $total_amount += $invoice->total_amount;
                        $round_off += $invoice->round_off;
                        $grand_total += $invoice->grand_total;
                    @endphp
                </tr>
            @endforeach
            <tr>
                <th colspan="7" class="text-right">Total</th>
                <th class="text-right">{{ number_format($sub_total,2) }}</th>
                @if($preference->discount_on == 'Inc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($discount_amount,2) }}</th>
                @if($preference->discount_on == 'Exc')
                <th class="text-right">{{ number_format($tax_amount,2) }}</th>
                @endif
                <th class="text-right">{{ number_format($total_amount,2) }}</th>
                <th class="text-right">{{ number_format($round_off,2) }}</th>
                <th class="text-right">{{ number_format($grand_total,2) }}</th>
            </tr>
        @endif
        @if($request->report_type == 'Detail Report')
            @foreach($invoices as $key => $invoice)
                <tr>
                    <th colspan="10">{{ $key+1 }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <div>
                            <strong>To:</strong>
                        </div>
                        <div><b> Name : </b>{{ $invoice->Customer->contact_name }}</div>
                        <div><b> Email : </b>{{ $invoice->Customer->email }}</div>
                        <div><b> Mobile : </b>{{ $invoice->Customer->mobile_no }}</div>
                        <div><b> PAN No : </b>{{ $invoice->Customer->pan_no }}</div>
                        <div><b> GSTIN : </b>{{ $invoice->Customer->gstin_no }}</div>
                        <div><b> B.Address : </b>{{ $invoice->Customer->billing_address }}</div>
                        <div><b> S.Address : </b>{{ $invoice->Customer->shipping_address }}</div>
                    </td>
                    <td colspan="5">
                        <div>
                            <strong>Details:</strong>
                        </div>
                        <div><b>Invoice Type : </b> {{ $invoice->invoice_type }}</div>
                        <div><b>Invoice No : </b> {{ $invoice->invoice_no }}</div>
                        <div><b>Invoice Date : </b> {{ date('d-m-Y',strtotime($invoice->invoice_date)) }}</div>
                        <div><b>Payment Term : </b>  {{ $invoice->PaymentTerm->payment_term }}</div> 
                    </td>
                </tr>
                <tr>
                    <th class="text-center">#</th>
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th class="text-right">S.Rate(Exc)</th>
                    <th class="text-right">S.Rate(Inc)</th>
                    @if($preference->discount_on == 'Inc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Discount</th>
                    @if($preference->discount_on == 'Exc')
                    <th class="text-right">Tax</th>
                    @endif
                    <th class="text-right">Qty</th>
                    <th class="text-right">Amount</th>
                </tr>
                @php $j=0; @endphp
                @foreach($invoice->InvoiceProducts as $invoice_product)
                    <tr>
                        <td class="text-center">
                             {{ ++$j }} 
                        </td>
                        <td>
                            {{ $invoice_product->product_code }}
                        </td>
                        <td>
                            {{ $invoice_product->product_name }}
                        </td>
                        <td class="text-right">
                            {{ number_format($invoice_product->sales_rate_exc,2) }}
                        </td>
                        <td class="text-right">
                           {{ number_format($invoice_product->sales_rate_inc,2) }}
                        </td>
                        @if($preference->discount_on == 'Inc')
                        <td class="text-right">
                            {{ number_format($invoice_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td class="text-right">
                            {{ number_format($invoice_product->discount_amount,2) }}
                        </td>
                        @if($preference->discount_on == 'Exc')
                        <td class="text-right">
                            {{ number_format($invoice_product->tax_amount,2) }}
                        </td>
                        @endif
                        <td class="text-right">
                            {{ number_format($invoice_product->quantity,2) }}
                        </td>
                        <td class="text-right">
                            {{ number_format($invoice_product->amount,2) }}
                        </td>                     
                    </tr>
                @endforeach
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Terms and Conditions : </strong><br>
                        {{ $invoice->terms }}
                    </td>
                    <th class="text-right" colspan="2">Sub Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->sub_total,2) }}
                    </th>
                </tr>
                @if($preference->discount_on == 'Inc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <th class="text-right" colspan="2">Discount</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->discount_amount,2) }}
                    </th>
                </tr>
                @if($preference->discount_on == 'Exc')
                <tr>
                    <th class="text-right" colspan="2">Tax</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->tax_amount,2) }}
                    </th>
                </tr>
                @endif
                <tr>
                    <td rowspan="3" colspan="5">
                        <strong>Note : </strong><br>
                        {{ $invoice->note  }}
                    </td>
                    <th class="text-right" colspan="2">Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->total_amount,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Round off</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->round_off,2) }}
                    </th>
                </tr>
                <tr>
                    <th class="text-right" colspan="2">Grand Total</th>
                    <th class="text-right" colspan="2">
                        {{ number_format($invoice->grand_total,2) }}
                    </th>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</body>
</html>