<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferences', function (Blueprint $table) {
            $table->increments('preference_id');
            $table->string('bill_prefix',50);
            $table->string('bill_no_length',50);
            $table->string('bill_pdf',50);
            $table->string('transfer_prefix',50);
            $table->string('transfer_no_length',50);
            $table->string('transfer_pdf',50);
            $table->string('invoice_prefix',50);
            $table->string('invoice_no_length',50);
            $table->string('invoice_pdf',50);
            $table->string('receipt_prefix',50);
            $table->string('receipt_no_length',50);
            $table->string('receipt_pdf',50);
            $table->string('voucher_prefix',50);
            $table->string('voucher_no_length',50);
            $table->string('voucher_pdf',50);
            $table->string('payment_prefix',50);
            $table->string('payment_no_length',50);
            $table->string('payment_pdf',50);      
            $table->string('fiscal_year',50);
            $table->string('discount_on',50);
            $table->double('discount',15,2);
            $table->double('card_amount',15,2)->default(0.00);
            $table->double('card_discount',15,2)->default(0.00);
            $table->string('allow_negative_stock',50);
            $table->string('printer_type',50);
            $table->string('printer_ip',50);
            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('preferences')->insert(
            [
                [
                    'bill_prefix'           => 'BILL',
                    'bill_no_length'        => '4',
                    'bill_pdf'              => 'pdf1',
                    'transfer_prefix'       => 'TRF',
                    'transfer_no_length'    => '4',
                    'transfer_pdf'          => 'pdf1',
                    'invoice_prefix'        => 'IN',
                    'invoice_no_length'     => '4',
                    'invoice_pdf'           => 'pdf1',
                    'receipt_prefix'        => 'REC',
                    'receipt_no_length'     => '4',
                    'receipt_pdf'           => 'pdf1',
                    'voucher_prefix'        => 'V',
                    'voucher_no_length'     => '4',
                    'voucher_pdf'           => 'pdf1',
                    'payment_prefix'        => 'PYM',
                    'payment_no_length'     => '4',
                    'payment_pdf'           => 'pdf1',
                    'fiscal_year'           => '2018-19',
                    'discount_on'           => 'Inc',
                    'allow_negative_stock'  => 'No',
                    'discount'              => '10',
                    'card_amount'           => '1000',
                    'card_discount'         => '5',
                    'printer_type'          => 'USB',
                    'printer_ip'            => 'USB',
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preferences');
    }
}
