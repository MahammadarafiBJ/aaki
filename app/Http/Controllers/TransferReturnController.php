<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use PDF;
use App\TransferReturn;
use App\TransferReturnProduct;
use App\Transfer;
use App\TransferProduct;
use App\Preference;
use App\Organization;
use App\Product;
use App\Price;
use App\Branch;

class TransferReturnController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_transfer_return_no(Request $request)
    {
        $preference = Preference::first();
        $id = TransferReturn::where('fiscal_year',$preference->fiscal_year)->max('transfer_return_id');
        $id = $id + 1;
        $n = $preference->transfer_return_no_length - strlen($id);
        for($i=0;$i<$n;$i++)
        {
            $id = '0'.$id;
        }
        $transfer_return_no = $preference->transfer_return_prefix.'/'.$id.'/'.$preference->fiscal_year;
        return $transfer_return_no;
    }

    public function display(Request $request)
    {
        $preference = Preference::first();

        if (Auth::User()->user_role == 'Super Admin' && $preference->branch_id == 1) {
            return TransferReturn::whereHas('Branch', function($query) use($request){
		                $query->where('branch_name','like', "%$request->search%");
		            })
		            ->orWhere('transfer_return_no', 'like', '%'.$request->search.'%')
		            ->orWhere('transfer_return_date', 'like', '%'.$request->search.'%')
		            ->orWhere('sub_total', 'like', '%'.$request->search.'%')
		            ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
		            ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
		            ->orWhere('grand_total', 'like', '%'.$request->search.'%')
		            ->with('Branch')
		            ->orderBy('transfer_return_id','DESC')
		            ->paginate(10);
        }else{
            return TransferReturn::where('branch_id',Auth()->User()->branch_id)
                    ->where(function($que) use($request){
                        $que->whereHas('Branch', function($query) use($request){
                            $query->where('branch_name','like', "%$request->search%");
                        })
                        ->orWhere('transfer_return_no', 'like', '%'.$request->search.'%')
                        ->orWhere('transfer_return_date', 'like', '%'.$request->search.'%')
                        ->orWhere('sub_total', 'like', '%'.$request->search.'%')
                        ->orWhere('discount_amount', 'like', '%'.$request->search.'%')
                        ->orWhere('tax_amount', 'like', '%'.$request->search.'%')
                        ->orWhere('grand_total', 'like', '%'.$request->search.'%');
                    })->with('Branch')
                    ->orderBy('transfer_return_id','DESC')
                    ->paginate(10);
        }
        
    }

    public function validation(Request $request)
    {
        $this->validate($request, [
            'product_id' 		=> 'required|numeric',
            'product_code' 		=> 'required|max:50',
            'product_name' 		=> 'required|max:255',
            'price_id' 			=> 'required|numeric',
            'barcode' 			=> 'required',
            'sales_rate_exc' 	=> 'required|numeric',
            'sales_rate_inc' 	=> 'required|numeric',
            'discount' 			=> 'required|numeric',
            'discount_type' 	=> 'required|max:50',
            'discount_amount' 	=> 'required|numeric',
            'tax_id' 			=> 'required|numeric',
            'tax_amount' 		=> 'required|numeric',
            'quantity' 			=> 'required|numeric',
            'amount' 			=> 'required|numeric',
            'sub_total' 		=> 'required|numeric',
        ]);
        return $request;

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'transfer_return_no'	=> 'required|max:50',
            'transfer_return_date'	=> 'required|date',
            'branch_id'         	=> 'required|numeric',
            'sub_total'         	=> 'required|numeric',
            'discount_amount'   	=> 'required|numeric',
            'tax_amount'        	=> 'required|numeric',
            'round_off'         	=> 'required|numeric',
            'total_amount'      	=> 'required|numeric',
            'grand_total'       	=> 'required|numeric',
            'term_id'           	=> 'sometimes|nullable|numeric',
            'terms'             	=> 'max:2550',
            'note'              	=> 'max:2550',
        ]);

        $preference = Preference::first();
        $branch     = Branch::where('branch_id',$request->branch_id)->first();

        $transfer_return = TransferReturn::create([
            'fiscal_year'           => $preference->fiscal_year,
            'transfer_return_no'    => $request->transfer_return_no,
            'transfer_return_date'  => date("Y-m-d", strtotime($request->transfer_return_date)),
            'branch_id'             => $branch->branch_id,
            'sub_total'             => $request->sub_total,
            'discount_amount'       => $request->discount_amount,
            'tax_amount'            => $request->tax_amount,
            'total_amount'          => $request->total_amount,
            'round_off'             => $request->round_off,
            'grand_total'           => $request->grand_total,
            'term_id'               => $request->term_id,
            'terms'                 => $request->terms,
            'note'                  => $request->note,
            'transfer_return_status'       => 'Open',
            'created_by'            => Auth::User()->username,
        ]);

        foreach ($request->transfer_return_products as $transfer_return_product) 
        {
            $product = Product::where('product_id',$transfer_return_product['product_id'])->first();
            $price = Price::where('price_id',$transfer_return_product['price_id'])->first();
            
            $transfer_return_product_up = TransferReturnProduct::create([
                'transfer_return_id'    => $transfer_return->transfer_return_id,
                'product_id'            => $transfer_return_product['product_id'],
                'product_type'          => $product->product_type,
                'product_code'          => $transfer_return_product['product_code'],
                'hsn_code'              => $product->hsn_code,
                'category_id'           => $product->category_id,
                'product_name'          => $transfer_return_product['product_name'],
                'description'           => $product->description,
                'product_unit'          => $product->product_unit,
                'price_id'              => $transfer_return_product['price_id'],
                'barcode'               => $price->barcode,
                'purchase_rate_exc'     => $price->purchase_rate_exc,
                'sales_rate_exc'        => $transfer_return_product['sales_rate_exc'],
                'purchase_rate_inc'     => $price->purchase_rate_inc,
                'sales_rate_inc'        => $transfer_return_product['sales_rate_inc'],
                'quantity'              => $transfer_return_product['quantity'],
                'amount'                => $transfer_return_product['amount'],
                'discount'              => $transfer_return_product['discount'],
                'discount_type'         => $transfer_return_product['discount_type'],
                'discount_amount'       => $transfer_return_product['discount_amount'],
                'tax_id'                => $transfer_return_product['tax_id'],
                'tax_amount'            => $transfer_return_product['tax_amount'],
                'sub_total'             => $transfer_return_product['sub_total'],
                'created_by'            => Auth::User()->username,
            ]);
        }
    }

    public function view(TransferReturn $transfer_return)
    {
        $transfer_return = TransferReturn::where('transfer_return_id',$transfer_return->transfer_return_id)->with(['TransferReturnProducts','Branch'])->first();

        $transfer_return->TransferReturnProducts->each(function($trans_rp,$key){
            $price = Price::where('price_id',$trans_rp->price_id)->first();
            if ($price) {
                $trans_rp->current_stock = $price->CurrentStock($price->price_id);
            }
            
        }); 

        return $transfer_return;
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'transfer_return_no'    => 'required|max:50',
            'transfer_return_date'  => 'required|date',
            'branch_id'             => 'required|numeric',
            'sub_total'             => 'required|numeric',
            'discount_amount'       => 'required|numeric',
            'tax_amount'            => 'required|numeric',
            'round_off'             => 'required|numeric',
            'total_amount'          => 'required|numeric',
            'grand_total'           => 'required|numeric',
            'term_id'               => 'sometimes|nullable|numeric',
            'terms'                 => 'max:2550',
            'note'                  => 'max:2550',
        ]);

        $preference = Preference::first();
        
        $transfer_return = TransferReturn::where('transfer_return_id',$request->transfer_return_id)->first();

        TransferReturn::where('transfer_return_id',$request->transfer_return_id)->update([
            'fiscal_year'           => $transfer_return->fiscal_year,
            'transfer_return_no'    => $request->transfer_return_no,
            'transfer_return_date'  => date("Y-m-d", strtotime($request->transfer_return_date)),
            'branch_id'             => $request->branch_id,
            'sub_total'             => $request->sub_total,
            'discount_amount'       => $request->discount_amount,
            'tax_amount'            => $request->tax_amount,
            'total_amount'          => $request->total_amount,
            'round_off'             => $request->round_off,
            'grand_total'           => $request->grand_total,
            'term_id'               => $request->term_id,
            'terms'                 => $request->terms,
            'note'                  => $request->note,
            'updated_by'            => Auth::User()->username,
        ]);

        foreach ($request->deleted_transfer_return_products as $transfer_return_product) 
        {
            TransferReturnProduct::where('transfer_return_product_id',$transfer_return_product['transfer_return_product_id'])->delete();
        }

        foreach ($request->transfer_return_products as $transfer_return_product) 
        {
            $product = Product::where('product_id',$transfer_return_product['product_id'])->first();
            $price = Price::where('price_id',$transfer_return_product['price_id'])->first();

            if(empty($transfer_return_product['transfer_return_product_id'])) {
                TransferReturnProduct::create([
                    'transfer_return_id'    => $transfer_return->transfer_return_id,
                    'product_id'            => $transfer_return_product['product_id'],
                    'product_type'          => $product->product_type,
                    'product_code'          => $transfer_return_product['product_code'],
                    'hsn_code'              => $product->hsn_code,
                    'category_id'           => $product->category_id,
                    'product_name'          => $transfer_return_product['product_name'],
                    'description'           => $product->description,
                    'product_unit'          => $product->product_unit,
                    'price_id'              => $transfer_return_product['price_id'],
                    'barcode'               => $price->barcode,
                    'purchase_rate_exc'     => $price->purchase_rate_exc,
                    'sales_rate_exc'        => $transfer_return_product['sales_rate_exc'],
                    'purchase_rate_inc'     => $price->purchase_rate_inc,
                    'sales_rate_inc'        => $transfer_return_product['sales_rate_inc'],
                    'quantity'              => $transfer_return_product['quantity'],
                    'amount'                => $transfer_return_product['amount'],
                    'discount'              => $transfer_return_product['discount'],
                    'discount_type'         => $transfer_return_product['discount_type'],
                    'discount_amount'       => $transfer_return_product['discount_amount'],
                    'tax_id'                => $transfer_return_product['tax_id'],
                    'tax_amount'            => $transfer_return_product['tax_amount'],
                    'sub_total'             => $transfer_return_product['sub_total'],
                    'created_by'            => Auth::User()->username,
                ]);
            }
            else
            {
                TransferReturnProduct::where('transfer_return_product_id',$transfer_return_product['transfer_return_product_id'])->update([
                    'transfer_return_id'    => $transfer_return->transfer_return_id,
                    'product_id'            => $transfer_return_product['product_id'],
                    'product_type'          => $product->product_type,
                    'product_code'          => $transfer_return_product['product_code'],
                    'hsn_code'              => $product->hsn_code,
                    'category_id'           => $product->category_id,
                    'product_name'          => $transfer_return_product['product_name'],
                    'description'           => $product->description,
                    'product_unit'          => $product->product_unit,
                    'price_id'              => $transfer_return_product['price_id'],
                    'barcode'               => $price->barcode,
                    'purchase_rate_exc'     => $price->purchase_rate_exc,
                    'sales_rate_exc'        => $transfer_return_product['sales_rate_exc'],
                    'purchase_rate_inc'     => $price->purchase_rate_inc,
                    'sales_rate_inc'        => $transfer_return_product['sales_rate_inc'],
                    'quantity'              => $transfer_return_product['quantity'],
                    'amount'                => $transfer_return_product['amount'],
                    'discount'              => $transfer_return_product['discount'],
                    'discount_type'         => $transfer_return_product['discount_type'],
                    'discount_amount'       => $transfer_return_product['discount_amount'],
                    'tax_id'                => $transfer_return_product['tax_id'],
                    'tax_amount'            => $transfer_return_product['tax_amount'],
                    'sub_total'             => $transfer_return_product['sub_total'],
                    'updated_by'            => Auth::User()->username,
                ]);
            }
        }
    }

    public function destroy(TransferReturn $transfer_return)
    {
        TransferReturnProduct::where('transfer_return_id',$transfer_return->transfer_return_id)->forceDelete();
        TransferReturn::where('transfer_return_id',$transfer_return->transfer_return_id)->forceDelete();
        
    }

    public function pdf(TransferReturn $transfer_return)
    {
        $org = Organization::first();
        $preference = Preference::first();

        $transfer_return = TransferReturn::where('transfer_return_id',$transfer_return->transfer_return_id)->with('Branch','TransferProducts')->first();

        PDF::loadView('transfer_return.pdf', compact('transfer_return','org','preference'), [], [
            'margin_top' => 41.8,
        ])->stream($transfer_return->transfer_return_no.'.pdf');
    }

    public function report(Request $request)
    {
        $this->validate($request, [
            'from_date' => 'required|date',
            'to_date'   => 'required|date',
        ]);

        $from_date = Date('Y-m-d',strtotime($request->from_date));
        $to_date = Date('Y-m-d',strtotime($request->to_date));
        $org = Organization::first();
        $preference = Preference::first();
        if (Auth::User()->user_role == 'Super Admin') {
            $transfer_returns = TransferReturn::whereBetween('transfer_return_date',[$from_date,$to_date])->get();

            if ($request->branch_id > 0) {
                $transfer_returns = $transfer_returns->filter(function($value) use($request){
                    return ($value->branch_id == $request->branch_id);
                })->values();
            }
        }else{
            $transfer_returns = TransferReturn::where('branch_id',Auth::User()->branch_id)->whereBetween('transfer_return_date',[$from_date,$to_date])->get();
        }

        switch ($request->display_type) {
            case 'pdf':
                PDF::loadView('transfer_return.report', compact('org','preference','transfer_returns','request'), [], [
                'margin_top' => 10
                ])->stream('transfer-report.pdf');
                break;
            case 'excel':
                return view('transfer_return.report',compact('org','preference','transfer_returns','request'));
                break;
            
            default:
                return view('transfer_return.report',compact('org','preference','transfer_returns','request'));
                break;
        }
    }
}
